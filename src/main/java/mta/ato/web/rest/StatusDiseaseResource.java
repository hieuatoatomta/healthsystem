package mta.ato.web.rest;

import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import mta.ato.domain.Frequency;
import mta.ato.domain.StatusDisease;
import mta.ato.domain.StatusDisease_;
import mta.ato.service.*;
import mta.ato.service.dto.*;
import mta.ato.service.mapper.StatusDiseaseMapper;
import mta.ato.utils.DateUtil;
import mta.ato.utils.JsonUtils;
import mta.ato.utils.Utils;
import mta.ato.web.rest.errors.BadRequestAlertException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link mta.ato.domain.StatusDisease}.
 */
@RestController
@RequestMapping("/api")
public class StatusDiseaseResource {

    private final Logger log = LoggerFactory.getLogger(StatusDiseaseResource.class);

    private static final String ENTITY_NAME = "statusDisease";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StatusDiseaseService statusDiseaseService;
    private final TestDiseaseService testDiseaseService;

    private final StatusDiseaseQueryService statusDiseaseQueryService;

    private final SymptomService symptomService;

    private final StatusDiseaseMapper statusDiseaseMapper;

    private final FrequencyService frequencyService;


    public StatusDiseaseResource(StatusDiseaseService statusDiseaseService,
                                 StatusDiseaseQueryService statusDiseaseQueryService,
                                 TestDiseaseService testDiseaseService,
                                 SymptomService symptomService,
                                 FrequencyService frequencyService,
                                 StatusDiseaseMapper statusDiseaseMapper) {
        this.statusDiseaseService = statusDiseaseService;
        this.statusDiseaseQueryService = statusDiseaseQueryService;
        this.statusDiseaseMapper = statusDiseaseMapper;
        this.testDiseaseService = testDiseaseService;
        this.symptomService = symptomService;
        this.frequencyService = frequencyService;
    }

    /**
     * {@code POST  /status-diseases} : Create a new statusDisease.
     *
     * @param statusDiseaseDTO the statusDiseaseDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new statusDiseaseDTO, or with status {@code 400 (Bad Request)} if the statusDisease has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/status-diseases")
    public ResponseEntity<Object> createStatusDisease(@Valid @RequestBody StatusDiseaseDTO statusDiseaseDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to save StatusDisease : {}", statusDiseaseDTO);
        if (statusDiseaseDTO.getId() != null) {
            throw new BadRequestAlertException("A new statusDisease cannot already have an ID", ENTITY_NAME, "idexists");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError());
            } else {
                statusDiseaseDTO.setUpdateTime(DateUtil.getDateC());
                statusDiseaseService.save(statusDiseaseDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code PUT  /status-diseases} : Updates an existing statusDisease.
     *
     * @param statusDiseaseDTO the statusDiseaseDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated statusDiseaseDTO,
     * or with status {@code 400 (Bad Request)} if the statusDiseaseDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the statusDiseaseDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/status-diseases")
    public ResponseEntity<Object> updateStatusDisease(@Valid @RequestBody StatusDiseaseDTO statusDiseaseDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to update StatusDisease : {}", statusDiseaseDTO);
        if (statusDiseaseDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError());
            } else {
//                Optional<StatusDiseaseDTO> typeDiseaseServiceOne = statusDiseaseService.findOne(statusDiseaseDTO.getId());
                statusDiseaseDTO.setUpdateTime(DateUtil.getDateC());
                statusDiseaseService.save(statusDiseaseDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body(Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    @GetMapping(value = "/client/status-diseases", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getStatusDiseaseClient(StatusDiseaseDTO statusDiseaseDTO) {
        log.debug("REST request to get StatusDisease : {}", statusDiseaseDTO);
        try {
//            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
//            StatusDiseaseDTO statusDiseaseDTO = Utils.mapData(StatusDiseaseDTO.class, "yyyyMMdd", reqParamObj);
            if (statusDiseaseDTO.getType1() != null && statusDiseaseDTO.getType1() == -1 && statusDiseaseDTO.getLsGd() !=null ) {
                List<StatusDiseaseDTO> list = statusDiseaseService.getMax(statusDiseaseDTO.getTypediseaseId(), statusDiseaseDTO.getLsGd());
                JSONObject result = new JSONObject();
                result.put("list", Utils.convertEntityToJSONObject(list.get(0)));
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", result));
            } else {
                if (statusDiseaseDTO.getValue().equals(null)) {
                    return ResponseEntity.badRequest().body(
                        Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình xác thực triệu chứng, vui lòng thử lại sau"));
                } else {
                    Long value = statusDiseaseDTO.getValue();
                    List<StatusDiseaseDTO> statusDiseaseDTOList = statusDiseaseService.findByStatusAndDetermined(statusDiseaseDTO.getTypediseaseId(), statusDiseaseDTO.getType(), value);
                    if (statusDiseaseDTOList.size() == 1){
                        JSONObject result = new JSONObject();
                        result.put("list", Utils.convertEntityToJSONObject(statusDiseaseDTOList.get(0)));
                        return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", result));
                    } else {
                        return ResponseEntity.badRequest().body(
                            Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình chẩn đoán, vui lòng thử lại sau"));
                    }
                }
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    @GetMapping(value = "/client/status-diseases-subclinical", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getStatusDiseaseClientSubclinical(@RequestParam Map<String, String> paramSearch) {
        log.debug("REST request to get StatusDiseaseSubclinical : {}", paramSearch);
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            StatusDiseaseDTO statusDiseaseDTO = Utils.mapData(StatusDiseaseDTO.class, "yyyyMMdd", reqParamObj);
            if (statusDiseaseDTO.getTypediseaseId().equals(null)) {
                return ResponseEntity.badRequest().body(
                    Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình xác thực triệu chứng, vui lòng thử lại sau"));
            } else {
                List<TestDiseaseDTO> statusDiseaseDTOList = testDiseaseService.findByTypediseaseId(statusDiseaseDTO.getTypediseaseId());
                if (statusDiseaseDTOList.size() > 0) {
                    for (int i = 0; i < statusDiseaseDTOList.size(); i++) {
                        statusDiseaseDTOList.get(i).setSymptomDTOList(symptomService.findByTypediseaseId(statusDiseaseDTO.getTypediseaseId(), statusDiseaseDTOList.get(i).getId()));
                    }
                }
                JSONObject result = new JSONObject();
                result.put("list", statusDiseaseDTOList);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", result));
//                if (statusDiseaseDTOList.size() == 1){
//                    JSONObject result = new JSONObject();
//                    result.put("list", Utils.convertEntityToJSONObject(statusDiseaseDTOList.get(0)));
//                    return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", result));
//                } else {
//                    return ResponseEntity.badRequest().body(
//                        Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình chẩn đoán, vui lòng thử lại sau"));
//                }
            }

        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    /**
     * {@code GET  /status-diseases} : get all the statusDiseases.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of statusDiseases in body.
     */
    @GetMapping(value = "/status-diseases", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAllStatusDiseases(@RequestParam Map<String, String> paramSearch) {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            StatusDiseaseCriteria criteria = Utils.mappingCriteria(StatusDiseaseCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get getAllStatusDiseases by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, StatusDisease_.ID);
            Page<StatusDisease> page = statusDiseaseQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (StatusDisease item : page) {
                StatusDiseaseDTO statusDiseaseDTO = statusDiseaseMapper.toDto(item);
                if (item.getTypedisease() != null){
                    statusDiseaseDTO.setNameType(item.getTypedisease().getName());
                }
                JSONObject lstRoles = Utils.convertEntityToJSONObject(statusDiseaseDTO);
                arrayResult.put(lstRoles);
            }
            JSONObject result = new JSONObject();
            result.put("count", statusDiseaseQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    /**
     * {@code GET  /status-diseases/count} : count all the statusDiseases.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/status-diseases/count")
    public ResponseEntity<Long> countStatusDiseases(StatusDiseaseCriteria criteria) {
        log.debug("REST request to count StatusDiseases by criteria: {}", criteria);
        return ResponseEntity.ok().body(statusDiseaseQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /status-diseases/:id} : get the "id" statusDisease.
     *
     * @param id the id of the statusDiseaseDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the statusDiseaseDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/status-diseases/{id}")
    public ResponseEntity<StatusDiseaseDTO> getStatusDisease(@PathVariable Long id) {
        log.debug("REST request to get StatusDisease : {}", id);
        Optional<StatusDiseaseDTO> statusDiseaseDTO = statusDiseaseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(statusDiseaseDTO);
    }

    /**
     * {@code DELETE  /status-diseases/:id} : delete the "id" statusDisease.
     *
     * @param id the id of the statusDiseaseDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping(value = "/status-diseases/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteStatusDisease(@PathVariable Long id) {
        log.debug("REST request to delete StatusDisease : {}", id);
        try {
            Optional<StatusDiseaseDTO> usersDTO = statusDiseaseService.findOne(id);
            if (!usersDTO.isPresent()) {
                throw new IllegalArgumentException("Có lỗi xảy ra trong xoá");
            }
            statusDiseaseService.delete(id);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
        } catch (Exception e){
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong xoá: " + e.getMessage()));
        }
    }
}
