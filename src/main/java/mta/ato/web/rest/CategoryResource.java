package mta.ato.web.rest;

import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import mta.ato.domain.Category;
import mta.ato.domain.Category_;
import mta.ato.service.CategoryQueryService;
import mta.ato.service.CategoryService;
import mta.ato.service.dto.CategoryCriteria;
import mta.ato.service.dto.CategoryDTO;
import mta.ato.utils.DateUtil;
import mta.ato.utils.JsonUtils;
import mta.ato.utils.Utils;
import mta.ato.utils.VNCharacterUtils;
import mta.ato.web.rest.errors.BadRequestAlertException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link mta.ato.domain.Category}.
 */
@RestController
@RequestMapping("/api")
public class CategoryResource {

    private final Logger log = LoggerFactory.getLogger(CategoryResource.class);

    private static final String ENTITY_NAME = "category";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CategoryService categoryService;

    private final CategoryQueryService categoryQueryService;

    public CategoryResource(CategoryService categoryService, CategoryQueryService categoryQueryService) {
        this.categoryService = categoryService;
        this.categoryQueryService = categoryQueryService;
    }

    /**
     * {@code POST  /categories} : Create a new category.
     *
     * @param categoryDTO the categoryDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new categoryDTO, or with status {@code 400 (Bad Request)} if the category has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/categories")
    public ResponseEntity<Object> createCategory(@Valid @RequestBody CategoryDTO categoryDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to save category : {}", categoryDTO);
        if (categoryDTO.getId() != null) {
            throw new BadRequestAlertException("A new category cannot already have an ID", ENTITY_NAME, "idexists");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError());
            } else {
                categoryDTO.setName(categoryDTO.getName().trim().toLowerCase());
                categoryDTO.setCode(VNCharacterUtils.removeAccent1(categoryDTO.getName()));
                Category category = categoryService.findByCode(categoryDTO.getCode());
                categoryDTO.setName(Utils.toUpcase(categoryDTO.getName()));
                if (category != null){
                    throw new IllegalArgumentException("Mã chủ đề đã tồn tại");
                }
                categoryDTO.setUpdateTime(DateUtil.getDateC());
                categoryService.save(categoryDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code PUT  /categories} : Updates an existing category.
     *
     * @param categoryDTO the categoryDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated categoryDTO,
     * or with status {@code 400 (Bad Request)} if the categoryDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the categoryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/categories")
    public ResponseEntity<Object> updateCategory(@Valid @RequestBody CategoryDTO categoryDTO, BindingResult result) throws URISyntaxException {
        if (categoryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError());
            } else {
                Optional<CategoryDTO> users = categoryService.findOne(categoryDTO.getId());
                CategoryDTO rolesDTO1 = users.get();
                categoryDTO.setName(categoryDTO.getName().trim().toLowerCase());
                categoryDTO.setCode(VNCharacterUtils.removeAccent1(categoryDTO.getName()));
                categoryDTO.setName(Utils.toUpcase(categoryDTO.getName()));
                if (!rolesDTO1.getCode().equals(categoryDTO.getCode())){
                    Category roles = categoryService.findByCode(categoryDTO.getCode());
                    if (roles != null){
                        throw new IllegalArgumentException("Mã chủ đề đã tồn tại");
                    }
                }
                categoryDTO.setUpdateTime(DateUtil.getDateC());
                categoryService.save(categoryDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code GET  /categories} : get all the categories.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of categories in body.
     */
    @GetMapping(value = "/categories", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getCategory(@RequestParam Map<String, String> paramSearch) {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            CategoryCriteria criteria = Utils.mappingCriteria(CategoryCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get Category by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, Category_.ID);
            Page<Category> page = categoryQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (Category item : page) {
                JSONObject lstRoles = Utils.convertEntityToJSONObject(item);
                arrayResult.put(lstRoles);
            }
            JSONObject result = new JSONObject();
            result.put("count", categoryQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }


    @GetMapping(value = "/client/categories", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getCategoryByClient(@RequestParam Map<String, String> paramSearch) {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            CategoryCriteria criteria = Utils.mappingCriteria(CategoryCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get Category by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, Category_.ID);
            Page<Category> page = categoryQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (Category item : page) {
                if (item.getCode().equals("tin-tuc-noi-bat")) {
                } else if (item.getCode().equals("tin-tuc-gan-day")) {
                } else {
                    JSONObject lstRoles = Utils.convertEntityToJSONObject(item);
                    arrayResult.put(lstRoles);
                }
            }
            JSONObject result = new JSONObject();
            result.put("count", categoryQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }


    /**
     * {@code GET  /categories/count} : count all the categories.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/categories/count")
    public ResponseEntity<Long> countCategories(CategoryCriteria criteria) {
        log.debug("REST request to count Categories by criteria: {}", criteria);
        return ResponseEntity.ok().body(categoryQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /categories/:id} : get the "id" category.
     *
     * @param id the id of the categoryDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the categoryDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/categories/{id}")
    public ResponseEntity<CategoryDTO> getCategory(@PathVariable Long id) {
        log.debug("REST request to get Category : {}", id);
        Optional<CategoryDTO> categoryDTO = categoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(categoryDTO);
    }

    /**
     * {@code DELETE  /categories/:id} : delete the "id" category.
     *
     * @param id the id of the categoryDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/categories/{id}")
    public ResponseEntity<Object> deleteCategory(@PathVariable Long id) {
        log.debug("REST request to delete Category : {}", id);
        try {
            Optional<CategoryDTO> usersDTO = categoryService.findOne(id);
            if (!usersDTO.isPresent()) {
                throw new IllegalArgumentException("Chủ đề không tồn tại");
            }
            categoryService.delete(id);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
        } catch (Exception e){
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong xoá: " + e.getMessage()));
        }
    }
}
