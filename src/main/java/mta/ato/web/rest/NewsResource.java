package mta.ato.web.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import mta.ato.config.CustomUserDetails;
import mta.ato.domain.*;
import mta.ato.service.CategoryService;
import mta.ato.service.NewsQueryService;
import mta.ato.service.NewsService;
import mta.ato.service.dto.FormUploadDTO;
import mta.ato.service.dto.NewsCriteria;
import mta.ato.service.dto.NewsDTO;
import mta.ato.service.mapper.ImageLinkMapper;
import mta.ato.service.mapper.NewsMapper;
import mta.ato.utils.*;
import mta.ato.web.rest.errors.BadRequestAlertException;
import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.*;

/**
 * REST controller for managing {@link mta.ato.domain.News}.
 */
@RestController
@RequestMapping("/api")
public class NewsResource {

    private final Logger log = LoggerFactory.getLogger(NewsResource.class);

    private static final String ENTITY_NAME = "news";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NewsService newsService;

    private final ImageLinkMapper imageLinkMapper;

    private final NewsMapper newsMapper;

    private final NewsQueryService newsQueryService;

    private final CategoryService categoryService;

    public NewsResource(NewsService newsService, NewsQueryService newsQueryService,
                        ImageLinkMapper imageLinkMapper, CategoryService categoryService,
                        NewsMapper newsMapper) {
        this.newsService = newsService;
        this.categoryService = categoryService;
        this.newsQueryService = newsQueryService;
        this.newsMapper = newsMapper;
        this.imageLinkMapper = imageLinkMapper;
    }

    /**
     * {@code POST  /news} : Create a new news.
     *
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new newsDTO, or with status {@code 400 (Bad Request)} if the news has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping(value = "/news", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> createNews(@Valid @ModelAttribute FormUploadDTO formUploadDTO, BindingResult result) throws URISyntaxException, JsonProcessingException {
        NewsDTO newsDTO = new ObjectMapper().readValue(formUploadDTO.getModel().toString(), NewsDTO.class);
        log.debug("REST request to save News : {}", newsDTO);
        if (newsDTO.getId() != null) {
            throw new BadRequestAlertException("A new users cannot already have an ID", ENTITY_NAME, "idexists");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError());
            } else {
                String code = newsDTO.getTitle();
                if ( code.length() > 50){
                    code = code.substring(0, 50);
                }
                CustomUserDetails principal = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                newsDTO.setCode(VNCharacterUtils.removeAccent1(code) + "-" + DateUtil.getDateToString() + RandomStringUtils.randomNumeric(4));
                newsDTO.setUsersId(principal.getUser().getId());
                newsDTO.setUpdateTime(DateUtil.getDateC());
                newsDTO.setStatus(1L); // set trang thai dang cho phe duyet khai cong tac vien dang bai
                FileUtils fileUtils = new FileUtils();
                String path = fileUtils.uploadFile(formUploadDTO.getFile(), newsDTO.getCode());
                newsDTO.setImageLink(path);
                newsService.save(newsDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }


    @PostMapping(value = "/news-refuse", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> reasonForRefusalNews(@Valid @RequestBody NewsDTO newsDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to save News : {}", newsDTO);
        if (newsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError());
            } else {
                Optional<NewsDTO> newsDTO1 = newsService.findOne(newsDTO.getId());
                NewsDTO news = newsDTO1.get();
                if (!newsDTO1.isPresent()) {
                    throw new IllegalArgumentException("Tin tức không tồn tại");
                }
                news.setReasonForRefusal(newsDTO.getReasonForRefusal());
                news.setStatus(newsDTO.getStatus()); // set trang thai tu choi phe duyet khai cong tac vien dang bai
                newsService.save(news);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code PUT  /news} : Updates an existing news.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated newsDTO,
     * or with status {@code 400 (Bad Request)} if the newsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the newsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping(value = "/news-img",  consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<NewsDTO> updateNewsImg(@Valid @ModelAttribute FormUploadDTO formUploadDTO) throws URISyntaxException, JsonProcessingException {
        NewsDTO newsDTO = new ObjectMapper().readValue(formUploadDTO.getModel().toString(), NewsDTO.class);
        if (newsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        log.debug("REST request to update News : {}", newsDTO);

        CustomUserDetails principal = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        newsDTO.setUsersId(principal.getUser().getId());
        newsDTO.setUpdateTime(DateUtil.getDateC());

        FileUtils fileUtils = new FileUtils();
        String path = fileUtils.uploadFile(formUploadDTO.getFile(), newsDTO.getCode());
        newsDTO.setImageLink(path);
        NewsDTO result = newsService.save(newsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, newsDTO.getId().toString()))
            .body(result);
    }

    @PutMapping(value = "/news",  consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<NewsDTO> updateNews(@Valid @RequestBody NewsDTO newsDTO) throws URISyntaxException, JsonProcessingException {
        if (newsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        log.debug("REST request to update News : {}", newsDTO);

        CustomUserDetails principal = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        newsDTO.setUsersId(principal.getUser().getId());
        newsDTO.setUpdateTime(DateUtil.getDateC());

//        FileUtils fileUtils = new FileUtils();
//        String path = fileUtils.uploadFile(formUploadDTO.getFile(), newsDTO.getCode());
//        newsDTO.setImageLink(path);
        NewsDTO result = newsService.save(newsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, newsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /news} : get all the news.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of news in body.
     */
    @GetMapping( value = "/news", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAllNewsByUsers(@RequestParam Map<String, String> paramSearch) throws InstantiationException, IllegalAccessException {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            NewsCriteria criteria = Utils.mappingCriteria(NewsCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            CustomUserDetails principal = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            LongFilter longFilter = new LongFilter();
            longFilter.setEquals(principal.getUser().getId());
            criteria.setUsersId(longFilter);
            log.debug("REST request to get Users by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, Users_.ID);
            Page<News> page = newsQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (News item : page) {
                NewsDTO newsDTO = newsMapper.toDto(item);
                if (item.getCategory() != null){
                    newsDTO.setCategoryName(item.getCategory().getName());
                }
                JSONObject lstRoles = Utils.convertEntityToJSONObject(newsDTO);
                arrayResult.put(lstRoles);
            }
            JSONObject result = new JSONObject();
            result.put("count", newsQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        }catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }


    @GetMapping( value = "/news-all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAllNews(@RequestParam Map<String, String> paramSearch) throws InstantiationException, IllegalAccessException {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            NewsCriteria criteria = Utils.mappingCriteria(NewsCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get News by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, News_.ID);
            Page<News> page = newsQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (News item : page) {
                NewsDTO newsDTO = newsMapper.toDto(item);
                if (item.getCategory() != null){
                    newsDTO.setCategoryName(item.getCategory().getName());
                }
                if (item.getUsers() != null) {
                    newsDTO.setUsersName(item.getUsers().getFullName());
                }
                JSONObject lstRoles = Utils.convertEntityToJSONObject(newsDTO);
                arrayResult.put(lstRoles);
            }
            JSONObject result = new JSONObject();
            result.put("count", newsQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }


    @GetMapping(value = "/client/news-by-code", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> clientGetAllNewsByCode(@RequestParam Map<String, String> paramSearch) throws InstantiationException, IllegalAccessException {
        try {
            String code = paramSearch.get("code");
            String page = paramSearch.get("page");
            String pageSize = paramSearch.get("page_size");
            int page1 = Integer.parseInt(page);
            int pageSize1 = Integer.parseInt(pageSize);
            Category category = categoryService.findByCode(code);
            if (category == null) {
                throw new IllegalArgumentException("Chủ đề không tồn tại");
            }
            Pageable pageable;
            List<News> list;
            if (code.equals("tin-tuc-gan-day")) {
                pageable = PageRequest.of(page1, pageSize1, Sort.by(Sort.Direction.DESC, "updateTime"));
                list = newsService.findAllByStatus(pageable);
            } else if (code.equals("tin-tuc-noi-bat")){
                pageable = PageRequest.of(page1, pageSize1);
                list = newsService.findAllByCategoryAndHighlights(category, pageable);
            }else {
                pageable = PageRequest.of(page1, pageSize1);
                list = newsService.findAllByCategory(category, pageable);
            }
            JSONArray arrayResult = new JSONArray();
            for (News item : list) {
                NewsDTO newsDTO = newsMapper.toDto(item);
                if (item.getUsers() != null) {
                    newsDTO.setUsersName(item.getUsers().getFullName());
                }
                JSONObject lstRoles = Utils.convertEntityToJSONObject(newsDTO);
                arrayResult.put(lstRoles);
            }
            JSONObject result = new JSONObject();
//            result.put("count", newsQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    @GetMapping(value = "/client/news-by-id/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> clientGetAllNewsById(@PathVariable Long id) throws InstantiationException, IllegalAccessException {
        try {
            JSONArray arrayResult = new JSONArray();
            News news = newsService.findOne1(id).get();
            if ( news.getStatus() != 0) {
                throw new IllegalArgumentException("Tin tức đã bị khoá");
            }
            NewsDTO newsDTO = newsMapper.toDto(news);
            if (news.getUsers() != null) {
                newsDTO.setUsersName(news.getUsers().getFullName());
            }
            JSONObject lstRoles = Utils.convertEntityToJSONObject(newsDTO);
            arrayResult.put(lstRoles);
            JSONObject result = new JSONObject();
            result.put("list", arrayResult);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }


    @GetMapping(value = "/news-images/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getProducts(@PathVariable Long id) {
        log.debug("REST request to get Products : {}", id);
        try {
            Optional<News> products = newsService.findOneEntity(id);
            if (products.orElse(null) == null) {
                return ResponseEntity.badRequest().body(
                    Utils.getStatusBadRequest("Không tìm thấy đối tượng"));
            }
            JSONObject lstRoles = Utils.convertEntityToJSONObject(products.get());
            Set<ImageLink> dsImageLinks = products.get().getDsImageLinks();
            ArrayList<ImageLink> lists = Lists.newArrayList(dsImageLinks.iterator());
            List list = new ArrayList();
            for (ImageLink imageLink : lists) {
                list.add(imageLinkMapper.toDto(imageLink));
            }
            if (dsImageLinks.isEmpty()) {
                lstRoles.put("DS_Image", new JSONArray());
            } else {
                lstRoles.put("DS_Image",
                    Utils.convertListEntityToJSONArray(list));
            }
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", lstRoles));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    /**
     * {@code GET  /news/count} : count all the news.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/news/count")
    public ResponseEntity<Long> countNews(NewsCriteria criteria) {
        log.debug("REST request to count News by criteria: {}", criteria);
        return ResponseEntity.ok().body(newsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /news/:id} : get the "id" news.
     *
     * @param id the id of the newsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the newsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/news/{id}")
    public ResponseEntity<NewsDTO> getNews(@PathVariable Long id) {
        log.debug("REST request to get News : {}", id);
        Optional<NewsDTO> newsDTO = newsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(newsDTO);
    }

    @GetMapping(value = "/news-highlights/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> highlightsNews(@PathVariable Long id) {
        log.debug("REST request to highlights News : {}", id);
        try {
            Optional<NewsDTO> newsDTO = newsService.findOne(id);
            if (newsDTO.orElse(null) == null) {
                return ResponseEntity.badRequest().body(
                    Utils.getStatusBadRequest("Không tìm thấy đối tượng"));
            }
            NewsDTO news = newsDTO.get();
            news.setHighlights(0L);
            newsService.save(news);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }

    }

    @GetMapping(value = "/news-off-highlights/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> offHighlightsNews(@PathVariable Long id) {
        log.debug("REST request to highlights News : {}", id);
        try {
            Optional<NewsDTO> newsDTO = newsService.findOne(id);
            if (newsDTO.orElse(null) == null) {
                return ResponseEntity.badRequest().body(
                    Utils.getStatusBadRequest("Không tìm thấy đối tượng"));
            }
            NewsDTO news = newsDTO.get();
            news.setHighlights(null);
            newsService.save(news);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }

    }

    /**
     * {@code DELETE  /news/:id} : delete the "id" news.
     *
     * @param id the id of the newsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/news/{id}")
    public ResponseEntity<Object> deleteNews(@PathVariable Long id) {
        log.debug("REST request to delete News : {}", id);
        try {
            Optional<NewsDTO> newsDTO = newsService.findOne(id);
            if (!newsDTO.isPresent()) {
                throw new IllegalArgumentException("Tin tức không tồn tại");
            }
            newsService.delete(id);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
        } catch (Exception e){
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong xoá: " + e.getMessage()));
        }
    }
}
