package mta.ato.web.rest;

import mta.ato.domain.TestDisease;
import mta.ato.domain.TypeDisease;
import mta.ato.domain.TypeDisease_;
import mta.ato.service.TestDiseaseService;
import mta.ato.service.dto.TypeDiseaseCriteria;
import mta.ato.service.dto.TypeDiseaseDTO;
import mta.ato.utils.DateUtil;
import mta.ato.utils.JsonUtils;
import mta.ato.utils.Utils;
import mta.ato.web.rest.errors.BadRequestAlertException;
import mta.ato.service.dto.TestDiseaseDTO;
import mta.ato.service.dto.TestDiseaseCriteria;
import mta.ato.service.TestDiseaseQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link mta.ato.domain.TestDisease}.
 */
@RestController
@RequestMapping("/api")
public class TestDiseaseResource {

    private final Logger log = LoggerFactory.getLogger(TestDiseaseResource.class);

    private static final String ENTITY_NAME = "testDisease";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TestDiseaseService testDiseaseService;

    private final TestDiseaseQueryService testDiseaseQueryService;

    public TestDiseaseResource(TestDiseaseService testDiseaseService, TestDiseaseQueryService testDiseaseQueryService) {
        this.testDiseaseService = testDiseaseService;
        this.testDiseaseQueryService = testDiseaseQueryService;
    }

    /**
     * {@code POST  /test-diseases} : Create a new testDisease.
     *
     * @param testDiseaseDTO the testDiseaseDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new testDiseaseDTO, or with status {@code 400 (Bad Request)} if the testDisease has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/test-diseases")
    public ResponseEntity<Object> createTestDisease(@Valid @RequestBody TestDiseaseDTO testDiseaseDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to save TestDisease : {}", testDiseaseDTO);
        if (testDiseaseDTO.getId() != null) {
            throw new BadRequestAlertException("A new roles cannot already have an ID", ENTITY_NAME, "idexists");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError());
            } else {
                TestDisease testDisease = testDiseaseService.findByCode(testDiseaseDTO.getCode());
                if (testDisease != null){
                    throw new IllegalArgumentException("Mã xét nghiệm đã tồn tại");
                }
                testDiseaseDTO.setUpdateTime(DateUtil.getDateC());
                testDiseaseService.save(testDiseaseDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code PUT  /test-diseases} : Updates an existing testDisease.
     *
     * @param testDiseaseDTO the testDiseaseDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated testDiseaseDTO,
     * or with status {@code 400 (Bad Request)} if the testDiseaseDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the testDiseaseDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/test-diseases")
    public ResponseEntity<Object> updateTestDisease(@Valid @RequestBody TestDiseaseDTO testDiseaseDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to update TestDisease : {}", testDiseaseDTO);
        if (testDiseaseDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError());
            } else {
                Optional<TestDiseaseDTO> typeDiseaseServiceOne = testDiseaseService.findOne(testDiseaseDTO.getId());
                TestDiseaseDTO rolesDTO1 = typeDiseaseServiceOne.get();
                if (!rolesDTO1.getCode().equals(testDiseaseDTO.getCode())){
                    TestDisease roles = testDiseaseService.findByCode(testDiseaseDTO.getCode());
                    if (roles != null){
                        throw new IllegalArgumentException("Mã xét nghiệm đã tồn tại");
                    }
                }
                testDiseaseDTO.setUpdateTime(DateUtil.getDateC());
                testDiseaseService.save(testDiseaseDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code GET  /test-diseases} : get all the testDiseases.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of testDiseases in body.
     */
    @GetMapping(value = "/test-diseases", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAllTestDiseases(@RequestParam Map<String, String> paramSearch) {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            TestDiseaseCriteria criteria = Utils.mappingCriteria(TestDiseaseCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get getAllTypeDiseases by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, TypeDisease_.ID);
            Page<TestDisease> page = testDiseaseQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (TestDisease item : page) {
                JSONObject lstRoles = Utils.convertEntityToJSONObject(item);
                arrayResult.put(lstRoles);
            }
            JSONObject result = new JSONObject();
            result.put("count", testDiseaseQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    /**
     * {@code GET  /test-diseases/count} : count all the testDiseases.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/test-diseases/count")
    public ResponseEntity<Long> countTestDiseases(TestDiseaseCriteria criteria) {
        log.debug("REST request to count TestDiseases by criteria: {}", criteria);
        return ResponseEntity.ok().body(testDiseaseQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /test-diseases/:id} : get the "id" testDisease.
     *
     * @param id the id of the testDiseaseDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the testDiseaseDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/test-diseases/{id}")
    public ResponseEntity<TestDiseaseDTO> getTestDisease(@PathVariable Long id) {
        log.debug("REST request to get TestDisease : {}", id);
        Optional<TestDiseaseDTO> testDiseaseDTO = testDiseaseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(testDiseaseDTO);
    }

    /**
     * {@code DELETE  /test-diseases/:id} : delete the "id" testDisease.
     *
     * @param id the id of the testDiseaseDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping(value = "/test-diseases/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteTestDisease(@PathVariable Long id) {
        log.debug("REST request to delete TestDisease : {}", id);
        try {
            Optional<TestDiseaseDTO> usersDTO = testDiseaseService.findOne(id);
            if (!usersDTO.isPresent()) {
                throw new IllegalArgumentException("Mã xét nghiệm không tồn tại");
            }
            testDiseaseService.delete(id);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
        } catch (Exception e){
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong xoá: " + e.getMessage()));
        }
    }
}
