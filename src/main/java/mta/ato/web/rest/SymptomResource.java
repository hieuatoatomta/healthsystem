package mta.ato.web.rest;

import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import mta.ato.domain.Frequency;
import mta.ato.domain.Symptom;
import mta.ato.domain.TypeDisease_;
import mta.ato.service.SymptomQueryService;
import mta.ato.service.SymptomService;
import mta.ato.service.TypeDiseaseService;
import mta.ato.service.dto.FrequencyDTO;
import mta.ato.service.dto.StatusDiseaseDTO;
import mta.ato.service.dto.SymptomCriteria;
import mta.ato.service.dto.SymptomDTO;
import mta.ato.service.mapper.SymptomMapper;
import mta.ato.utils.DateUtil;
import mta.ato.utils.JsonUtils;
import mta.ato.utils.Utils;
import mta.ato.web.rest.errors.BadRequestAlertException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link mta.ato.domain.Symptom}.
 */
@RestController
@RequestMapping("/api")
public class SymptomResource {

    private final Logger log = LoggerFactory.getLogger(SymptomResource.class);

    private static final String ENTITY_NAME = "symptom";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SymptomService symptomService;

    private final SymptomQueryService symptomQueryService;

    private final TypeDiseaseService typeDiseaseService;

    private final SymptomMapper symptomMapper;

    public SymptomResource(SymptomService symptomService, SymptomMapper symptomMapper,
                           TypeDiseaseService typeDiseaseService,
                           SymptomQueryService symptomQueryService) {
        this.symptomService = symptomService;
        this.symptomQueryService = symptomQueryService;
        this.symptomMapper = symptomMapper;
        this.typeDiseaseService = typeDiseaseService;
    }

    /**
     * {@code POST  /symptoms} : Create a new symptom.
     *
     * @param symptomDTO the symptomDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new symptomDTO, or with status {@code 400 (Bad Request)} if the symptom has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/symptoms")
    public ResponseEntity<Object> createSymptom(@Valid @RequestBody SymptomDTO symptomDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to save Symptom : {}", symptomDTO);
        if (symptomDTO.getId() != null) {
            throw new BadRequestAlertException("A new roles cannot already have an ID", ENTITY_NAME, "idexists");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body(result.getFieldError());
            } else {
                symptomDTO.setUpdateTime(DateUtil.getDateC());
                symptomService.save(symptomDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body(Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code PUT  /symptoms} : Updates an existing symptom.
     *
     * @param symptomDTO the symptomDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated symptomDTO,
     * or with status {@code 400 (Bad Request)} if the symptomDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the symptomDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/symptoms")
    public ResponseEntity<Object> updateSymptom(@Valid @RequestBody SymptomDTO symptomDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to update Symptom : {}", symptomDTO);
        if (symptomDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body(result.getFieldError());
            } else {
                Optional<SymptomDTO> typeDiseaseServiceOne = symptomService.findOne(symptomDTO.getId());
                if (!typeDiseaseServiceOne.isPresent()) {
                    throw new IllegalArgumentException("Triệu chứng không tồn tại");
                }
                symptomDTO.setUpdateTime(DateUtil.getDateC());
                symptomService.save(symptomDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body(Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code GET  /symptoms} : get all the symptoms.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of symptoms in body.
     */
    @GetMapping(value = "/symptoms", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAllSymptoms(@RequestParam Map<String, String> paramSearch) {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            SymptomCriteria criteria = Utils.mappingCriteria(SymptomCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get getAllTypeDiseases by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, TypeDisease_.ID);
            Page<Symptom> page = symptomQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (Symptom item : page) {
                SymptomDTO newsDTO = symptomMapper.toDto(item);
//                TypeTestSymptom typeTestSymptom =typeTestSymptomService.findBySymptom(item);
//                if (typeTestSymptom != null) {
//                    newsDTO.setTypediseaseId(typeTestSymptom.getTypedisease().getId());
//                }
                JSONObject lstRoles = Utils.convertEntityToJSONObject(newsDTO);
                arrayResult.put(lstRoles);
            }
            JSONObject result = new JSONObject();
            result.put("count", symptomQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }


    @GetMapping(value = "/symptoms-group", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAllSymptomsGroup(SymptomDTO symptomDTO) {
        try {
            List<SymptomDTO> list = symptomService.findByGroup(symptomDTO.getTypediseaseId(), symptomDTO.getListIdXn());
            JSONObject result = new JSONObject();
            result.put("list", Utils.convertListEntityToJSONArray(list));
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    @GetMapping(value = "/client/symptoms", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAllSymptomsClient(@RequestParam Map<String, String> paramSearch) {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            SymptomCriteria criteria = Utils.mappingCriteria(SymptomCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get getAllTypeDiseases by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, TypeDisease_.ID);
            Page<Symptom> page = symptomQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
//            JSONArray arrayResult = new JSONArray();

            List<SymptomDTO> symptomDTOList = new ArrayList<>();
            for (Symptom item : page) {

                SymptomDTO newsDTO = symptomMapper.toDto(item);
                if( item.getDsFrequencies() != null && item.getDsFrequencies().size() > 0) {
                    List<Frequency> list = new ArrayList<>();
                    for(Frequency key: item.getDsFrequencies()){
                        list.add(key);
                    }
                    newsDTO.setFrequencyList(list);
                }
                symptomDTOList.add(newsDTO);
//
//                JSONObject lstRoles = Utils.convertEntityToJSONObject(newsDTO);
//                arrayResult.put(lstRoles);
            }
//            JSONObject result = new JSONObject();
//            result.put("count", symptomQueryService.countByCriteria(criteria));
//            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(symptomDTOList);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    /**
     * {@code GET  /symptoms/count} : count all the symptoms.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/symptoms/count")
    public ResponseEntity<Long> countSymptoms(SymptomCriteria criteria) {
        log.debug("REST request to count Symptoms by criteria: {}", criteria);
        return ResponseEntity.ok().body(symptomQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /symptoms/:id} : get the "id" symptom.
     *
     * @param id the id of the symptomDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the symptomDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/symptoms/{id}")
    public ResponseEntity<SymptomDTO> getSymptom(@PathVariable Long id) {
        log.debug("REST request to get Symptom : {}", id);
        Optional<SymptomDTO> symptomDTO = symptomService.findOne(id);
        return ResponseUtil.wrapOrNotFound(symptomDTO);
    }

    /**
     * {@code DELETE  /symptoms/:id} : delete the "id" symptom.
     *
     * @param id the id of the symptomDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping(value = "/symptoms/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteSymptom(@PathVariable Long id) {
        log.debug("REST request to delete Symptom : {}", id);
        try {
            Optional<SymptomDTO> symptomDTO = symptomService.findOne(id);
            if (!symptomDTO.isPresent()) {
                throw new IllegalArgumentException("Triệu chứng không tồn tại");
            }
            symptomService.delete(id);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong xoá: " + e.getMessage()));
        }
    }
}
