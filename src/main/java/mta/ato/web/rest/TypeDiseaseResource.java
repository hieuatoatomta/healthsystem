package mta.ato.web.rest;

import mta.ato.domain.Roles;
import mta.ato.domain.Roles_;
import mta.ato.domain.TypeDisease;
import mta.ato.domain.TypeDisease_;
import mta.ato.service.TypeDiseaseService;
import mta.ato.service.dto.RolesCriteria;
import mta.ato.service.dto.RolesDTO;
import mta.ato.utils.DateUtil;
import mta.ato.utils.JsonUtils;
import mta.ato.utils.Utils;
import mta.ato.web.rest.errors.BadRequestAlertException;
import mta.ato.service.dto.TypeDiseaseDTO;
import mta.ato.service.dto.TypeDiseaseCriteria;
import mta.ato.service.TypeDiseaseQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link mta.ato.domain.TypeDisease}.
 */
@RestController
@RequestMapping("/api")
public class TypeDiseaseResource {

    private final Logger log = LoggerFactory.getLogger(TypeDiseaseResource.class);

    private static final String ENTITY_NAME = "typeDisease";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TypeDiseaseService typeDiseaseService;

    private final TypeDiseaseQueryService typeDiseaseQueryService;

    public TypeDiseaseResource(TypeDiseaseService typeDiseaseService, TypeDiseaseQueryService typeDiseaseQueryService) {
        this.typeDiseaseService = typeDiseaseService;
        this.typeDiseaseQueryService = typeDiseaseQueryService;
    }

    /**
     * {@code POST  /type-diseases} : Create a new typeDisease.
     *
     * @param typeDiseaseDTO the typeDiseaseDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new typeDiseaseDTO, or with status {@code 400 (Bad Request)} if the typeDisease has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/type-diseases")
    public ResponseEntity<Object> createTypeDisease(@Valid @RequestBody TypeDiseaseDTO typeDiseaseDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to save Roles : {}", typeDiseaseDTO);
        if (typeDiseaseDTO.getId() != null) {
            throw new BadRequestAlertException("A new roles cannot already have an ID", ENTITY_NAME, "idexists");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError());
            } else {
                TypeDisease typeDisease = typeDiseaseService.findByCode(typeDiseaseDTO.getCode());
                if (typeDisease != null){
                    throw new IllegalArgumentException("Mã bệnh đã tồn tại");
                }
                typeDiseaseDTO.setUpdateTime(DateUtil.getDateC());
                typeDiseaseService.save(typeDiseaseDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code PUT  /type-diseases} : Updates an existing typeDisease.
     *
     * @param typeDiseaseDTO the typeDiseaseDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated typeDiseaseDTO,
     * or with status {@code 400 (Bad Request)} if the typeDiseaseDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the typeDiseaseDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/type-diseases")
    public ResponseEntity<Object> updateTypeDisease(@Valid @RequestBody TypeDiseaseDTO typeDiseaseDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to updateTypeDisease : {}", typeDiseaseDTO);
        if (typeDiseaseDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError());
            } else {
                Optional<TypeDiseaseDTO> typeDiseaseServiceOne = typeDiseaseService.findOne(typeDiseaseDTO.getId());
                TypeDiseaseDTO rolesDTO1 = typeDiseaseServiceOne.get();
                if (!rolesDTO1.getCode().equals(typeDiseaseDTO.getCode())){
                    TypeDisease roles = typeDiseaseService.findByCode(typeDiseaseDTO.getCode());
                    if (roles != null){
                        throw new IllegalArgumentException("Mã bệnh đã tồn tại");
                    }
                }
                typeDiseaseDTO.setUpdateTime(DateUtil.getDateC());
                typeDiseaseService.save(typeDiseaseDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code GET  /type-diseases} : get all the typeDiseases.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of typeDiseases in body.
     */
    @GetMapping(value = "/type-diseases", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAllTypeDiseases(@RequestParam Map<String, String> paramSearch) {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            TypeDiseaseCriteria criteria = Utils.mappingCriteria(TypeDiseaseCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get getAllTypeDiseases by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, TypeDisease_.ID);
            Page<TypeDisease> page = typeDiseaseQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (TypeDisease item : page) {
                JSONObject lstRoles = Utils.convertEntityToJSONObject(item);
                arrayResult.put(lstRoles);
            }
            JSONObject result = new JSONObject();
            result.put("count", typeDiseaseQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    /**
     * {@code GET  /type-diseases/count} : count all the typeDiseases.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/type-diseases/count")
    public ResponseEntity<Long> countTypeDiseases(TypeDiseaseCriteria criteria) {
        log.debug("REST request to count TypeDiseases by criteria: {}", criteria);
        return ResponseEntity.ok().body(typeDiseaseQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /type-diseases/:id} : get the "id" typeDisease.
     *
     * @param id the id of the typeDiseaseDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the typeDiseaseDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/type-diseases/{id}")
    public ResponseEntity<TypeDiseaseDTO> getTypeDisease(@PathVariable Long id) {
        log.debug("REST request to get TypeDisease : {}", id);
        Optional<TypeDiseaseDTO> typeDiseaseDTO = typeDiseaseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(typeDiseaseDTO);
    }

    /**
     * {@code DELETE  /type-diseases/:id} : delete the "id" typeDisease.
     *
     * @param id the id of the typeDiseaseDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping(value = "/type-diseases/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteTypeDisease(@PathVariable Long id) {
        log.debug("REST request to delete Roles : {}", id);
        try {
            Optional<TypeDiseaseDTO> usersDTO = typeDiseaseService.findOne(id);
            if (!usersDTO.isPresent()) {
                throw new IllegalArgumentException("Bệnh không tồn tại");
            }
            typeDiseaseService.delete(id);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
        } catch (Exception e){
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong xoá: " + e.getMessage()));
        }
    }
}
