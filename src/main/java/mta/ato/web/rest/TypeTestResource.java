package mta.ato.web.rest;

import mta.ato.domain.TypeTest;
import mta.ato.service.TypeTestService;
import mta.ato.service.dto.*;
import mta.ato.service.mapper.TypeTestMapper;
import mta.ato.utils.JsonUtils;
import mta.ato.utils.Utils;
import mta.ato.web.rest.errors.BadRequestAlertException;
import mta.ato.service.TypeTestQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link mta.ato.domain.TypeTest}.
 */
@RestController
@RequestMapping("/api")
public class TypeTestResource {

    private final Logger log = LoggerFactory.getLogger(TypeTestResource.class);

    private static final String ENTITY_NAME = "typeTest";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TypeTestService typeTestService;

    private final TypeTestQueryService typeTestQueryService;
    private final TypeTestMapper typeTestMapper;


    public TypeTestResource(TypeTestService typeTestService,
                            TypeTestMapper typeTestMapper,
                            TypeTestQueryService typeTestQueryService) {
        this.typeTestService = typeTestService;
        this.typeTestQueryService = typeTestQueryService;
        this.typeTestMapper = typeTestMapper;
    }

    /**
     * {@code POST  /type-tests} : Create a new typeTest.
     *
     * @param typeTestDTO the typeTestDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new typeTestDTO, or with status {@code 400 (Bad Request)} if the typeTest has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/type-tests")
    public ResponseEntity<TypeTestDTO> createTypeTest(@RequestBody TypeTestDTO typeTestDTO) throws URISyntaxException {
        log.debug("REST request to save TypeTest : {}", typeTestDTO);
        if (typeTestDTO.getId() != null) {
            throw new BadRequestAlertException("A new typeTest cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TypeTestDTO result = typeTestService.save(typeTestDTO);
        return ResponseEntity.created(new URI("/api/type-tests/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /type-tests} : Updates an existing typeTest.
     *
     * @param typeTestDTO the typeTestDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated typeTestDTO,
     * or with status {@code 400 (Bad Request)} if the typeTestDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the typeTestDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/type-tests")
    public ResponseEntity<TypeTestDTO> updateTypeTest(@RequestBody TypeTestDTO typeTestDTO) throws URISyntaxException {
        log.debug("REST request to update TypeTest : {}", typeTestDTO);
        if (typeTestDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TypeTestDTO result = typeTestService.save(typeTestDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, typeTestDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /type-tests} : get all the typeTests.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of typeTests in body.
     */
    @GetMapping(value = "/type-tests", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAllTypeTests(@RequestParam Map<String, String> paramSearch) {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            TypeTestCriteria criteria = Utils.mappingCriteria(TypeTestCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get Objects by criteria: {}", criteria);
            List<TypeTest> page = typeTestQueryService.findByCriteria(criteria);
            JSONArray arrayResult = new JSONArray();
            for (TypeTest item : page) {
                TypeTestDTO newsDTO = typeTestMapper.toDto(item);
                newsDTO.setNameTypeDisease(item.getTestdisease().getName());
                JSONObject usersJson = Utils.convertEntityToJSONObject(newsDTO);
                arrayResult.put(usersJson);
            }
            JSONObject result = new JSONObject();
            result.put("count", typeTestQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", result));
        }catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    @PostMapping("/type-tests/delete")
    public ResponseEntity<Object> delete(@RequestBody @Valid TypeTestDTO obj, HttpServletRequest request) {
        try {
            Long id = typeTestService.deleteByCode(obj);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong xoá: " + e.getMessage()));
        }
    }

    @PutMapping("/type-tests/insert")
    public ResponseEntity<Object> insert(@RequestBody @Valid TypeTestDTO obj, BindingResult result, HttpServletRequest request) {
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body(result.getFieldError());
            } else {
                Long id = typeTestService.insert(obj);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong them: " + e.getMessage()));
        }
    }

    /**
     * {@code GET  /type-tests/count} : count all the typeTests.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/type-tests/count")
    public ResponseEntity<Long> countTypeTests(TypeTestCriteria criteria) {
        log.debug("REST request to count TypeTests by criteria: {}", criteria);
        return ResponseEntity.ok().body(typeTestQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /type-tests/:id} : get the "id" typeTest.
     *
     * @param id the id of the typeTestDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the typeTestDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/type-tests/{id}")
    public ResponseEntity<TypeTestDTO> getTypeTest(@PathVariable Long id) {
        log.debug("REST request to get TypeTest : {}", id);
        Optional<TypeTestDTO> typeTestDTO = typeTestService.findOne(id);
        return ResponseUtil.wrapOrNotFound(typeTestDTO);
    }

    /**
     * {@code DELETE  /type-tests/:id} : delete the "id" typeTest.
     *
     * @param id the id of the typeTestDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/type-tests/{id}")
    public ResponseEntity<Void> deleteTypeTest(@PathVariable Long id) {
        log.debug("REST request to delete TypeTest : {}", id);
        typeTestService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
