package mta.ato.web.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mta.ato.config.CustomUserDetails;
import mta.ato.domain.Category;
import mta.ato.domain.Category_;
import mta.ato.domain.ExpertSystem;
import mta.ato.service.ExpertSystemService;
import mta.ato.service.dto.*;
import mta.ato.utils.*;
import mta.ato.web.rest.errors.BadRequestAlertException;
import mta.ato.service.ExpertSystemQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link mta.ato.domain.ExpertSystem}.
 */
@RestController
@RequestMapping("/api")
public class ExpertSystemResource {

    private final Logger log = LoggerFactory.getLogger(ExpertSystemResource.class);

    private static final String ENTITY_NAME = "expertSystem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExpertSystemService expertSystemService;

    private final ExpertSystemQueryService expertSystemQueryService;

    public ExpertSystemResource(ExpertSystemService expertSystemService, ExpertSystemQueryService expertSystemQueryService) {
        this.expertSystemService = expertSystemService;
        this.expertSystemQueryService = expertSystemQueryService;
    }

    /**
     * {@code POST  /expert-systems} : Create a new expertSystem.
     *
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new expertSystemDTO, or with status {@code 400 (Bad Request)} if the expertSystem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping(value = "/expert-systems", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> createNews(@Valid @ModelAttribute FormUploadDTO formUploadDTO, BindingResult result) throws URISyntaxException, JsonProcessingException {
        ExpertSystemDTO newsDTO = new ObjectMapper().readValue(formUploadDTO.getModel().toString(), ExpertSystemDTO.class);
        log.debug("REST request to save News : {}", newsDTO);
        if (newsDTO.getId() != null) {
            throw new BadRequestAlertException("A new users cannot already have an ID", ENTITY_NAME, "idexists");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError());
            } else {
                String code = newsDTO.getName().toLowerCase();
                if ( code.length() > 50){
                    code = code.substring(0, 50);
                }
                newsDTO.setCode(VNCharacterUtils.removeAccent(code) + "-" + DateUtil.getDateToString() + RandomStringUtils.randomNumeric(4));
                newsDTO.setUpdateTime(DateUtil.getDateC());
                FileUtils fileUtils = new FileUtils();
                String path = fileUtils.uploadFile(formUploadDTO.getFile(), newsDTO.getCode());
                newsDTO.setIsLink(path);
                expertSystemService.save(newsDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }
    /**
     * {@code PUT  /expert-systems} : Updates an existing expertSystem.
     *
     * @param expertSystemDTO the expertSystemDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated expertSystemDTO,
     * or with status {@code 400 (Bad Request)} if the expertSystemDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the expertSystemDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/expert-systems")
    public ResponseEntity<ExpertSystemDTO> updateExpertSystem(@RequestBody ExpertSystemDTO expertSystemDTO) throws URISyntaxException {
        log.debug("REST request to update ExpertSystem : {}", expertSystemDTO);
        if (expertSystemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        expertSystemDTO.setUpdateTime(DateUtil.getDateC());
        ExpertSystemDTO result = expertSystemService.save(expertSystemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, expertSystemDTO.getId().toString()))
            .body(result);
    }

    @PutMapping(value = "/expert-systems-img",  consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ExpertSystemDTO> updateNewsImg(@Valid @ModelAttribute FormUploadDTO formUploadDTO) throws URISyntaxException, JsonProcessingException {
        ExpertSystemDTO expertSystemDTO = new ObjectMapper().readValue(formUploadDTO.getModel().toString(), ExpertSystemDTO.class);
        if (expertSystemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        log.debug("REST request to update News : {}", expertSystemDTO);

//        CustomUserDetails principal = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        newsDTO.setUsersId(principal.getUser().getId());
        expertSystemDTO.setUpdateTime(DateUtil.getDateC());

        String code = expertSystemDTO.getName().toLowerCase();
        if ( code.length() > 50){
            code = code.substring(0, 50);
        }
        expertSystemDTO.setCode(VNCharacterUtils.removeAccent(code) + "-" + DateUtil.getDateToString() + RandomStringUtils.randomNumeric(4));

        FileUtils fileUtils = new FileUtils();
        String path = fileUtils.uploadFile(formUploadDTO.getFile(), expertSystemDTO.getCode());
        expertSystemDTO.setIsLink(path);
        ExpertSystemDTO result = expertSystemService.save(expertSystemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, expertSystemDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /expert-systems} : get all the expertSystems.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of expertSystems in body.
     */
    @GetMapping(value = "/expert-systems", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAllExpertSystems(@RequestParam Map<String, String> paramSearch) {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            ExpertSystemCriteria criteria = Utils.mappingCriteria(ExpertSystemCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get Category by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, Category_.ID);
            Page<ExpertSystem> page = expertSystemQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (ExpertSystem item : page) {
                JSONObject lstRoles = Utils.convertEntityToJSONObject(item);
                arrayResult.put(lstRoles);
            }
            JSONObject result = new JSONObject();
            result.put("count", expertSystemQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    @GetMapping(value = "/client/expert-systems", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAllExpertSystemsClient(@RequestParam Map<String, String> paramSearch) {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            ExpertSystemCriteria criteria = Utils.mappingCriteria(ExpertSystemCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get Category by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, Category_.ID);
            Page<ExpertSystem> page = expertSystemQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (ExpertSystem item : page) {
                JSONObject lstRoles = Utils.convertEntityToJSONObject(item);
                arrayResult.put(lstRoles);
            }
            JSONObject result = new JSONObject();
            result.put("count", expertSystemQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    /**
     * {@code GET  /expert-systems/count} : count all the expertSystems.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/expert-systems/count")
    public ResponseEntity<Long> countExpertSystems(ExpertSystemCriteria criteria) {
        log.debug("REST request to count ExpertSystems by criteria: {}", criteria);
        return ResponseEntity.ok().body(expertSystemQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /expert-systems/:id} : get the "id" expertSystem.
     *
     * @param id the id of the expertSystemDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the expertSystemDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/expert-systems/{id}")
    public ResponseEntity<ExpertSystemDTO> getExpertSystem(@PathVariable Long id) {
        log.debug("REST request to get ExpertSystem : {}", id);
        Optional<ExpertSystemDTO> expertSystemDTO = expertSystemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(expertSystemDTO);
    }

    /**
     * {@code DELETE  /expert-systems/:id} : delete the "id" expertSystem.
     *
     * @param id the id of the expertSystemDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/expert-systems/{id}")
    public ResponseEntity<Void> deleteExpertSystem(@PathVariable Long id) {
        log.debug("REST request to delete ExpertSystem : {}", id);
        expertSystemService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
