package mta.ato.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import mta.ato.service.StatusDiseaseService;
import mta.ato.service.SubclinicalDeterminationQueryService;
import mta.ato.service.SubclinicalDeterminationService;
import mta.ato.service.dto.StatusDiseaseDTO;
import mta.ato.service.dto.SubclinicalDeterminationCriteria;
import mta.ato.service.dto.SubclinicalDeterminationDTO;
import mta.ato.utils.DateUtil;
import mta.ato.utils.Utils;
import mta.ato.web.rest.errors.BadRequestAlertException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.Random;

/**
 * REST controller for managing {@link mta.ato.domain.SubclinicalDetermination}.
 */
@RestController
@RequestMapping("/api")
public class SubclinicalDeterminationResource {

    private final Logger log = LoggerFactory.getLogger(SubclinicalDeterminationResource.class);

    private static final String ENTITY_NAME = "subclinicalDetermination";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SubclinicalDeterminationService subclinicalDeterminationService;

    private final StatusDiseaseService statusDiseaseService;

    private final SubclinicalDeterminationQueryService subclinicalDeterminationQueryService;

    public SubclinicalDeterminationResource(SubclinicalDeterminationService subclinicalDeterminationService,
                                            StatusDiseaseService statusDiseaseService,
                                            SubclinicalDeterminationQueryService subclinicalDeterminationQueryService) {
        this.subclinicalDeterminationService = subclinicalDeterminationService;
        this.statusDiseaseService = statusDiseaseService;
        this.subclinicalDeterminationQueryService = subclinicalDeterminationQueryService;
    }

    /**
     * {@code POST  /subclinical-determinations} : Create a new subclinicalDetermination.
     *
     * @param subclinicalDeterminationDTO the subclinicalDeterminationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new subclinicalDeterminationDTO, or with status {@code 400 (Bad Request)} if the subclinicalDetermination has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/subclinical-determinations")
    public ResponseEntity<Object> createSubclinicalDetermination(@Valid  @RequestBody SubclinicalDeterminationDTO subclinicalDeterminationDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to save SubclinicalDetermination : {}", subclinicalDeterminationDTO);
        if (subclinicalDeterminationDTO.getId() != null) {
            throw new BadRequestAlertException("A new subclinicalDetermination cannot already have an ID", ENTITY_NAME, "idexists");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body(result.getFieldError());
            } else {
                if ((subclinicalDeterminationDTO.getListIdXn().size() + 1) != subclinicalDeterminationDTO.getAmount()) {
                    // kiểm tra xem đang thêm mới tuyệt đối hay tương đối
                    // tương đối
                    if (subclinicalDeterminationDTO.getListIdTt() == null ){
                            Long long1 = subclinicalDeterminationService.findAllByAmountAndIdXn(subclinicalDeterminationDTO.getTypediseaseId(), subclinicalDeterminationDTO.getAmount(), subclinicalDeterminationDTO.getListIdXn()).get(0).getTotalMax();
                            if (long1.equals(subclinicalDeterminationDTO.getAmount())) {
                                throw new IllegalArgumentException("Chẩn đoán bệnh đã tồn tại, vui lòng kiểm tra lại");
                            } else {
                                if (subclinicalDeterminationDTO.getListIdXn().size() > 0) {
                                    Random rand = new Random();
                                    subclinicalDeterminationDTO.setType(Long.valueOf(rand.nextInt(1000000000)));
                                    for (int i = 0; i < subclinicalDeterminationDTO.getListIdXn().size(); i++) {
                                        subclinicalDeterminationDTO.setIdXn(subclinicalDeterminationDTO.getListIdXn().get(i));
                                        subclinicalDeterminationDTO.setUpdateTime(DateUtil.getDateC());
                                        subclinicalDeterminationService.save(subclinicalDeterminationDTO);
                                    }
                                    return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
                                } else {
                                    return ResponseEntity.badRequest().body(Utils.getStatusBadRequest("ó lỗi xảy ra trong quá trình thêm mới, vui lòng thử lại"));
                                }
                            }

                    } else {
                        if (subclinicalDeterminationDTO.getListIdXn().size() > 0) {
                            Random rand = new Random();
                            subclinicalDeterminationDTO.setType(Long.valueOf(rand.nextInt(1000000000)));
                            for (int i = 0; i < subclinicalDeterminationDTO.getListIdXn().size(); i++) {
                                for (int j = 0; j < subclinicalDeterminationDTO.getListIdTt().size(); j++) {
                                    subclinicalDeterminationDTO.setIdCheck(1L);
                                    subclinicalDeterminationDTO.setIdXn(subclinicalDeterminationDTO.getListIdXn().get(i));
                                    subclinicalDeterminationDTO.setIdTT(subclinicalDeterminationDTO.getListIdTt().get(j));
                                    subclinicalDeterminationDTO.setUpdateTime(DateUtil.getDateC());
                                    subclinicalDeterminationService.save(subclinicalDeterminationDTO);
                                }

                            }
                            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
                        } else {
                            return ResponseEntity.badRequest().body(Utils.getStatusBadRequest("ó lỗi xảy ra trong quá trình thêm mới, vui lòng thử lại"));
                        }
                    }

                } else {
                    throw new IllegalArgumentException("Có lỗi xảy ra trong quá trình thêm mới, vui lòng thử lại");
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body(Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code PUT  /subclinical-determinations} : Updates an existing subclinicalDetermination.
     *
     * @param subclinicalDeterminationDTO the subclinicalDeterminationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated subclinicalDeterminationDTO,
     * or with status {@code 400 (Bad Request)} if the subclinicalDeterminationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the subclinicalDeterminationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/subclinical-determinations")
    public ResponseEntity<SubclinicalDeterminationDTO> updateSubclinicalDetermination(@RequestBody SubclinicalDeterminationDTO subclinicalDeterminationDTO) throws URISyntaxException {
        log.debug("REST request to update SubclinicalDetermination : {}", subclinicalDeterminationDTO);
        if (subclinicalDeterminationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SubclinicalDeterminationDTO result = subclinicalDeterminationService.save(subclinicalDeterminationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, subclinicalDeterminationDTO.getId().toString()))
            .body(result);
    }

    @GetMapping(value = "/client/subclinical-determinations")
    public ResponseEntity<String> getClinet(SubclinicalDeterminationDTO subclinicalDeterminationDTO) {
        try {
            List<StatusDiseaseDTO> list1 = statusDiseaseService.isCheck(subclinicalDeterminationDTO.getTypediseaseId(), subclinicalDeterminationDTO.getLsIdXn(), subclinicalDeterminationDTO.getLsIdTt());
            List<StatusDiseaseDTO> list = null;
            if (list1.size() == 0) {
                list = statusDiseaseService.findAllByClinetIsCheck(subclinicalDeterminationDTO.getTypediseaseId(), subclinicalDeterminationDTO.getAmount(), subclinicalDeterminationDTO.getLsIdXn());
            } else {
                list = statusDiseaseService.findAllByClinet(subclinicalDeterminationDTO.getTypediseaseId(), subclinicalDeterminationDTO.getAmount(), subclinicalDeterminationDTO.getLsIdXn(), subclinicalDeterminationDTO.getLsIdTt());

            }
            JSONObject result = new JSONObject();
            result.put("list", Utils.convertListEntityToJSONArray(list));
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }


    @GetMapping(value = "/subclinical-determinations/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAllSubclinicalDeterminations(@PathVariable Long id) {
        try {
            List<SubclinicalDeterminationDTO> list = subclinicalDeterminationService.getRoles(id);
            JSONObject result = new JSONObject();
            result.put("list", list);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    /**
     * {@code GET  /subclinical-determinations/count} : count all the subclinicalDeterminations.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/subclinical-determinations/count")
    public ResponseEntity<Long> countSubclinicalDeterminations(SubclinicalDeterminationCriteria criteria) {
        log.debug("REST request to count SubclinicalDeterminations by criteria: {}", criteria);
        return ResponseEntity.ok().body(subclinicalDeterminationQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /subclinical-determinations/:id} : get the "id" subclinicalDetermination.
     *
     * @param id the id of the subclinicalDeterminationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the subclinicalDeterminationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/subclinical-determinations/{id}")
    public ResponseEntity<SubclinicalDeterminationDTO> getSubclinicalDetermination(@PathVariable Long id) {
        log.debug("REST request to get SubclinicalDetermination : {}", id);
        Optional<SubclinicalDeterminationDTO> subclinicalDeterminationDTO = subclinicalDeterminationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(subclinicalDeterminationDTO);
    }

    /**
     * {@code DELETE  /subclinical-determinations/:id} : delete the "id" subclinicalDetermination.
     *
     * @param id the id of the subclinicalDeterminationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping(value = "/subclinical-determinations/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteSubclinicalDetermination(@PathVariable Long id) {
        log.debug("REST request to delete SubclinicalDetermination : {}", id);
        try {
            if (!subclinicalDeterminationService.findByType(id)) {
                throw new IllegalArgumentException("Chẩn đoán không tồn tại");
            }
            subclinicalDeterminationService.deleteByType(id);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong xoá: " + e.getMessage()));
        }
    }
}
