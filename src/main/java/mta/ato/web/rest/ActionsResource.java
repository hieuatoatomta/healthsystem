package mta.ato.web.rest;

import mta.ato.domain.Actions;
import mta.ato.domain.Actions_;
import mta.ato.service.ActionsService;
import mta.ato.utils.JsonUtils;
import mta.ato.utils.Utils;
import mta.ato.web.rest.errors.BadRequestAlertException;
import mta.ato.service.dto.ActionsDTO;
import mta.ato.service.dto.ActionsCriteria;
import mta.ato.service.ActionsQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link mta.ato.domain.Actions}.
 */
@RestController
@RequestMapping("/api")
public class ActionsResource {

    private final Logger log = LoggerFactory.getLogger(ActionsResource.class);

    private static final String ENTITY_NAME = "actions";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ActionsService actionsService;

    private final ActionsQueryService actionsQueryService;

    public ActionsResource(ActionsService actionsService, ActionsQueryService actionsQueryService) {
        this.actionsService = actionsService;
        this.actionsQueryService = actionsQueryService;
    }

    /**
     * {@code POST  /actions} : Create a new actions.
     *
     * @param actionsDTO the actionsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new actionsDTO, or with status {@code 400 (Bad Request)} if the actions has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/actions")
    public ResponseEntity<ActionsDTO> createActions(@Valid @RequestBody ActionsDTO actionsDTO) throws URISyntaxException {
        log.debug("REST request to save Actions : {}", actionsDTO);
        if (actionsDTO.getId() != null) {
            throw new BadRequestAlertException("A new actions cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ActionsDTO result = actionsService.save(actionsDTO);
        return ResponseEntity.created(new URI("/api/actions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /actions} : Updates an existing actions.
     *
     * @param actionsDTO the actionsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated actionsDTO,
     * or with status {@code 400 (Bad Request)} if the actionsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the actionsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/actions")
    public ResponseEntity<ActionsDTO> updateActions(@Valid @RequestBody ActionsDTO actionsDTO) throws URISyntaxException {
        log.debug("REST request to update Actions : {}", actionsDTO);
        if (actionsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ActionsDTO result = actionsService.save(actionsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, actionsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /actions} : get all the actions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of actions in body.
     */
    @GetMapping(value = "/actions", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAllActions(@RequestParam Map<String, String> paramSearch) throws InstantiationException, IllegalAccessException {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            ActionsCriteria criteria = Utils.mappingCriteria(ActionsCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get Users by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, Actions_.ID);
            Page<Actions> page = actionsQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (Actions item : page) {
                JSONObject usersJson = Utils.convertEntityToJSONObject(item);
                arrayResult.put(usersJson);
            }
            JSONObject result = new JSONObject();
            result.put("count", actionsQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        }catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    /**
     * {@code GET  /actions/count} : count all the actions.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/actions/count")
    public ResponseEntity<Long> countActions(ActionsCriteria criteria) {
        log.debug("REST request to count Actions by criteria: {}", criteria);
        return ResponseEntity.ok().body(actionsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /actions/:id} : get the "id" actions.
     *
     * @param id the id of the actionsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the actionsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/actions/{id}")
    public ResponseEntity<ActionsDTO> getActions(@PathVariable Long id) {
        log.debug("REST request to get Actions : {}", id);
        Optional<ActionsDTO> actionsDTO = actionsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(actionsDTO);
    }

    /**
     * {@code DELETE  /actions/:id} : delete the "id" actions.
     *
     * @param id the id of the actionsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/actions/{id}")
    public ResponseEntity<Void> deleteActions(@PathVariable Long id) {
        log.debug("REST request to delete Actions : {}", id);
        actionsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
