package mta.ato.web.rest;

import mta.ato.config.Constants;
import mta.ato.domain.ImageLink;
import mta.ato.domain.News;
import mta.ato.service.ImageLinkService;
import mta.ato.service.NewsService;
import mta.ato.service.dto.NewsDTO;
import mta.ato.utils.DateUtil;
import mta.ato.utils.FileUtils;
import mta.ato.utils.Utils;
import mta.ato.utils.VNCharacterUtils;
import mta.ato.web.rest.errors.BadRequestAlertException;
import mta.ato.service.dto.ImageLinkDTO;
import mta.ato.service.dto.ImageLinkCriteria;
import mta.ato.service.ImageLinkQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link mta.ato.domain.ImageLink}.
 */
@RestController
@RequestMapping("/api")
public class ImageLinkResource {

    private final Logger log = LoggerFactory.getLogger(ImageLinkResource.class);

    private static final String ENTITY_NAME = "imageLink";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ImageLinkService imageLinkService;

    private final ImageLinkQueryService imageLinkQueryService;
    private final NewsService newsService;

    public ImageLinkResource(ImageLinkService imageLinkService,NewsService newsService, ImageLinkQueryService imageLinkQueryService) {
        this.imageLinkService = imageLinkService;
        this.imageLinkQueryService = imageLinkQueryService;
        this.newsService = newsService;
    }

    /**
     * {@code GET  /image-links} : get all the imageLinks.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of imageLinks in body.
     */
    @GetMapping("/image-links")
    public ResponseEntity<List<ImageLinkDTO>> getAllImageLinks(ImageLinkCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ImageLinks by criteria: {}", criteria);
        Page<ImageLinkDTO> page = imageLinkQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /image-links/count} : count all the imageLinks.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/image-links/count")
    public ResponseEntity<Long> countImageLinks(ImageLinkCriteria criteria) {
        log.debug("REST request to count ImageLinks by criteria: {}", criteria);
        return ResponseEntity.ok().body(imageLinkQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /image-links/:id} : get the "id" imageLink.
     *
     * @param id the id of the imageLinkDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the imageLinkDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/image-links/{id}")
    public ResponseEntity<ImageLinkDTO> getImageLink(@PathVariable Long id) {
        log.debug("REST request to get ImageLink : {}", id);
        Optional<ImageLinkDTO> imageLinkDTO = imageLinkService.findOne(id);
        return ResponseUtil.wrapOrNotFound(imageLinkDTO);
    }

    /**
     * {@code DELETE  /image-links/:id} : delete the "id" imageLink.
     *
     * @param id the id of the imageLinkDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping(value = "/image-links/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteImageLink(@PathVariable Long id) {
        try {
            log.debug("REST request to delete ImageLink : {}", id);
            Optional<ImageLinkDTO> imageLinkDTO =imageLinkService.findOne(id);
            if (!imageLinkDTO.isPresent()) {
                throw new IllegalArgumentException("Hinh anh khong tồn tại");
            }
            imageLinkService.delete(id);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong xoá: " + e.getMessage()));
        }
    }

    @PostMapping("/image-links/lock")
    public ResponseEntity<Object> insert(@RequestBody @Valid ImageLinkDTO obj, BindingResult result) {
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError().getDefaultMessage());
            } else {
                return ResponseEntity.status(200).body(Collections.singletonMap(Constants.DATA, imageLinkService.lock(obj)));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.status(400).body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    @PostMapping("/upload")
    @Transactional
    public ResponseEntity<Object> singleFileUpload(@RequestParam("file") MultipartFile file,
                                                   RedirectAttributes redirectAttributes, HttpServletRequest request) {
        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return ResponseEntity.status(400).body( "that bai");
        }
        try {
            Long id = Long.parseLong(request.getParameter("id"));
            Optional<News> news =newsService.findOneEntity(id);
            News news1 = news.get();
            if (news1 == null) {
                return ResponseEntity.status(400).body( "Upload hinh anh that bai");
            }
            FileUtils fileUtils = new FileUtils();
            String path = fileUtils.uploadFile(file, news1.getCode());
            ImageLink imageLink = new ImageLink();
            imageLink.setNews(news1);
            imageLink.setName(VNCharacterUtils.removeAccent1(file.getOriginalFilename()));
            imageLink.setImageLink(path);
            imageLink.setStatus(1L);
            imageLink.setUpdateTime(DateUtil.getDateC());
            imageLinkService.save(imageLink);
            return ResponseEntity.ok().body(Collections.singletonMap(Constants.DATA, "thanh cong"));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body( e.getMessage());
        }
    }
}
