package mta.ato.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A TypeDisease.
 */
@Entity
@Table(name = "type_disease")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TypeDisease implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @Column(name = "code", length = 100)
    private String code;

    @Size(max = 100)
    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "status")
    private Long status;

    @Column(name = "update_time")
    private ZonedDateTime updateTime;

    @Size(max = 1000)
    @Column(name = "description", length = 1000)
    private String description;

    @OneToMany(mappedBy = "typedisease")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<TypeTest> dsTypeTests = new HashSet<>();

    @OneToMany(mappedBy = "typedisease", fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Symptom> dsSymptoms = new HashSet<>();

    @OneToMany(mappedBy = "typedisease")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<StatusDisease> dsStatusDiseases = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public TypeDisease code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public TypeDisease name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStatus() {
        return status;
    }

    public TypeDisease status(Long status) {
        this.status = status;
        return this;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public TypeDisease updateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getDescription() {
        return description;
    }

    public TypeDisease description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<TypeTest> getDsTypeTests() {
        return dsTypeTests;
    }

    public TypeDisease dsTypeTests(Set<TypeTest> typeTests) {
        this.dsTypeTests = typeTests;
        return this;
    }

    public TypeDisease addDsTypeTest(TypeTest typeTest) {
        this.dsTypeTests.add(typeTest);
        typeTest.setTypedisease(this);
        return this;
    }

    public TypeDisease removeDsTypeTest(TypeTest typeTest) {
        this.dsTypeTests.remove(typeTest);
        typeTest.setTypedisease(null);
        return this;
    }

    public void setDsTypeTests(Set<TypeTest> typeTests) {
        this.dsTypeTests = typeTests;
    }

    public Set<Symptom> getDsSymptoms() {
        return dsSymptoms;
    }

    public TypeDisease dsSymptoms(Set<Symptom> symptoms) {
        this.dsSymptoms = symptoms;
        return this;
    }

    public TypeDisease addDsSymptom(Symptom symptom) {
        this.dsSymptoms.add(symptom);
        symptom.setTypedisease(this);
        return this;
    }

    public TypeDisease removeDsSymptom(Symptom symptom) {
        this.dsSymptoms.remove(symptom);
        symptom.setTypedisease(null);
        return this;
    }

    public void setDsSymptoms(Set<Symptom> symptoms) {
        this.dsSymptoms = symptoms;
    }

    public Set<StatusDisease> getDsStatusDiseases() {
        return dsStatusDiseases;
    }

    public TypeDisease dsStatusDiseases(Set<StatusDisease> statusDiseases) {
        this.dsStatusDiseases = statusDiseases;
        return this;
    }

    public TypeDisease addDsStatusDisease(StatusDisease statusDisease) {
        this.dsStatusDiseases.add(statusDisease);
        statusDisease.setTypedisease(this);
        return this;
    }

    public TypeDisease removeDsStatusDisease(StatusDisease statusDisease) {
        this.dsStatusDiseases.remove(statusDisease);
        statusDisease.setTypedisease(null);
        return this;
    }

    public void setDsStatusDiseases(Set<StatusDisease> statusDiseases) {
        this.dsStatusDiseases = statusDiseases;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TypeDisease)) {
            return false;
        }
        return id != null && id.equals(((TypeDisease) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TypeDisease{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", name='" + getName() + "'" +
            ", status=" + getStatus() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
