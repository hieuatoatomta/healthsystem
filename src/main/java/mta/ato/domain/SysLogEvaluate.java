package mta.ato.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A SysLogEvaluate.
 */
@Entity
@Table(name = "sys_log_evaluate")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SysLogEvaluate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "full_name")
    private String fullName;

    @Size(max = 100)
    @Column(name = "mail", length = 100)
    private String mail;

    @Size(max = 10)
    @Column(name = "phone", length = 10)
    private String phone;

    @Column(name = "name_type")
    private String nameType;

    @Column(name = "name_evaluate")
    private String nameEvaluate;

    @Column(name = "content")
    private String content;

    @Column(name = "update_time")
    private ZonedDateTime updateTime;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public SysLogEvaluate fullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMail() {
        return mail;
    }

    public SysLogEvaluate mail(String mail) {
        this.mail = mail;
        return this;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public SysLogEvaluate phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNameType() {
        return nameType;
    }

    public SysLogEvaluate nameType(String nameType) {
        this.nameType = nameType;
        return this;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    public String getNameEvaluate() {
        return nameEvaluate;
    }

    public SysLogEvaluate nameEvaluate(String nameEvaluate) {
        this.nameEvaluate = nameEvaluate;
        return this;
    }

    public void setNameEvaluate(String nameEvaluate) {
        this.nameEvaluate = nameEvaluate;
    }

    public String getContent() {
        return content;
    }

    public SysLogEvaluate content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public SysLogEvaluate updateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SysLogEvaluate)) {
            return false;
        }
        return id != null && id.equals(((SysLogEvaluate) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SysLogEvaluate{" +
            "id=" + getId() +
            ", fullName='" + getFullName() + "'" +
            ", mail='" + getMail() + "'" +
            ", phone='" + getPhone() + "'" +
            ", nameType='" + getNameType() + "'" +
            ", nameEvaluate='" + getNameEvaluate() + "'" +
            ", content='" + getContent() + "'" +
            ", updateTime='" + getUpdateTime() + "'" +
            "}";
    }
}
