package mta.ato.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A SubclinicalDetermination.
 */
@Entity
@Table(name = "subclinical_determination")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SubclinicalDetermination implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "amount")
    private Long amount;

    @Column(name = "id_xn")
    private Long idXn;

    @Column(name = "id_check")
    private Long idCheck;

    @Column(name = "id_tt")
    private Long idTT;

    @Column(name = "type")
    private Long type;

    @Column(name = "update_time")
    private ZonedDateTime updateTime;

    @ManyToOne
    @JsonIgnoreProperties(value = "dsSubclinicalDeterminations", allowSetters = true)
    private StatusDisease statusdisease;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public SubclinicalDetermination amount(Long amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getIdXn() {
        return idXn;
    }

    public SubclinicalDetermination idXn(Long idXn) {
        this.idXn = idXn;
        return this;
    }

    public void setIdXn(Long idXn) {
        this.idXn = idXn;
    }

    public Long getIdCheck() {
        return idCheck;
    }

    public SubclinicalDetermination idCheck(Long idCheck) {
        this.idCheck = idCheck;
        return this;
    }

    public void setIdCheck(Long idCheck) {
        this.idCheck = idCheck;
    }

    public Long getIdTT() {
        return idTT;
    }

    public SubclinicalDetermination idTT(Long idTT) {
        this.idTT = idTT;
        return this;
    }

    public void setIdTT(Long idTT) {
        this.idTT = idTT;
    }

    public Long getType() {
        return type;
    }

    public SubclinicalDetermination type(Long type) {
        this.type = type;
        return this;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public SubclinicalDetermination updateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public StatusDisease getStatusdisease() {
        return statusdisease;
    }

    public SubclinicalDetermination statusdisease(StatusDisease statusDisease) {
        this.statusdisease = statusDisease;
        return this;
    }

    public void setStatusdisease(StatusDisease statusDisease) {
        this.statusdisease = statusDisease;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SubclinicalDetermination)) {
            return false;
        }
        return id != null && id.equals(((SubclinicalDetermination) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SubclinicalDetermination{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", idXn=" + getIdXn() +
            ", idCheck=" + getIdCheck() +
            ", idTT=" + getIdTT() +
            ", type=" + getType() +
            ", updateTime='" + getUpdateTime() + "'" +
            "}";
    }
}
