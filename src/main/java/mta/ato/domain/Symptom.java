package mta.ato.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A Symptom.
 */
@Entity
@Table(name = "symptom")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Symptom implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "status")
    private Long status;

    @Column(name = "type")
    private Long type;

    @Column(name = "update_time")
    private ZonedDateTime updateTime;

    @OneToMany(mappedBy = "symptom", fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Frequency> dsFrequencies = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "typedisease_id", referencedColumnName = "id")
    @JsonIgnoreProperties(value = "dsSymptoms", allowSetters = true)
    private TypeDisease typedisease;

    @ManyToOne
    @JoinColumn(name = "testdisease_id", referencedColumnName = "id")
    @JsonIgnoreProperties(value = "dsSymptoms", allowSetters = true)
    private TestDisease testdisease;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Symptom name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStatus() {
        return status;
    }

    public Symptom status(Long status) {
        this.status = status;
        return this;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getType() {
        return type;
    }

    public Symptom type(Long type) {
        this.type = type;
        return this;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public Symptom updateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Set<Frequency> getDsFrequencies() {
        return dsFrequencies;
    }

    public Symptom dsFrequencies(Set<Frequency> frequencies) {
        this.dsFrequencies = frequencies;
        return this;
    }

    public Symptom addDsFrequency(Frequency frequency) {
        this.dsFrequencies.add(frequency);
        frequency.setSymptom(this);
        return this;
    }

    public Symptom removeDsFrequency(Frequency frequency) {
        this.dsFrequencies.remove(frequency);
        frequency.setSymptom(null);
        return this;
    }

    public void setDsFrequencies(Set<Frequency> frequencies) {
        this.dsFrequencies = frequencies;
    }

    public TypeDisease getTypedisease() {
        return typedisease;
    }

    public Symptom typedisease(TypeDisease typeDisease) {
        this.typedisease = typeDisease;
        return this;
    }

    public void setTypedisease(TypeDisease typeDisease) {
        this.typedisease = typeDisease;
    }

    public TestDisease getTestdisease() {
        return testdisease;
    }

    public Symptom testdisease(TestDisease testDisease) {
        this.testdisease = testDisease;
        return this;
    }

    public void setTestdisease(TestDisease testDisease) {
        this.testdisease = testDisease;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Symptom)) {
            return false;
        }
        return id != null && id.equals(((Symptom) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Symptom{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", status=" + getStatus() +
            ", type=" + getType() +
            ", updateTime='" + getUpdateTime() + "'" +
            "}";
    }
}
