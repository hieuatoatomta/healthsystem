package mta.ato.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A StatusDisease.
 */
@Entity
@Table(name = "status_disease")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class StatusDisease implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "determined")
    private String determined;

    @Column(name = "status")
    private Long status;

    @Column(name = "type")
    private Long type;

    @Column(name = "update_time")
    private ZonedDateTime updateTime;

    @Column(name = "is_ex")
    private Long isEx;

    @Column(name = "lik_status")
    private Long likStatus;

    @Size(max = 1000)
    @Column(name = "description", length = 1000)
    private String description;

    @OneToMany(mappedBy = "statusdisease")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<SubclinicalDetermination> dsSubclinicalDeterminations = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "dsStatusDiseases", allowSetters = true)
    private TypeDisease typedisease;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public StatusDisease name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetermined() {
        return determined;
    }

    public StatusDisease determined(String determined) {
        this.determined = determined;
        return this;
    }

    public void setDetermined(String determined) {
        this.determined = determined;
    }

    public Long getStatus() {
        return status;
    }

    public StatusDisease status(Long status) {
        this.status = status;
        return this;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getType() {
        return type;
    }

    public StatusDisease type(Long type) {
        this.type = type;
        return this;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public StatusDisease updateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Long getIsEx() {
        return isEx;
    }

    public StatusDisease isEx(Long isEx) {
        this.isEx = isEx;
        return this;
    }

    public void setIsEx(Long isEx) {
        this.isEx = isEx;
    }

    public Long getLikStatus() {
        return likStatus;
    }

    public StatusDisease likStatus(Long likStatus) {
        this.likStatus = likStatus;
        return this;
    }

    public void setLikStatus(Long likStatus) {
        this.likStatus = likStatus;
    }

    public String getDescription() {
        return description;
    }

    public StatusDisease description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<SubclinicalDetermination> getDsSubclinicalDeterminations() {
        return dsSubclinicalDeterminations;
    }

    public StatusDisease dsSubclinicalDeterminations(Set<SubclinicalDetermination> subclinicalDeterminations) {
        this.dsSubclinicalDeterminations = subclinicalDeterminations;
        return this;
    }

    public StatusDisease addDsSubclinicalDetermination(SubclinicalDetermination subclinicalDetermination) {
        this.dsSubclinicalDeterminations.add(subclinicalDetermination);
        subclinicalDetermination.setStatusdisease(this);
        return this;
    }

    public StatusDisease removeDsSubclinicalDetermination(SubclinicalDetermination subclinicalDetermination) {
        this.dsSubclinicalDeterminations.remove(subclinicalDetermination);
        subclinicalDetermination.setStatusdisease(null);
        return this;
    }

    public void setDsSubclinicalDeterminations(Set<SubclinicalDetermination> subclinicalDeterminations) {
        this.dsSubclinicalDeterminations = subclinicalDeterminations;
    }

    public TypeDisease getTypedisease() {
        return typedisease;
    }

    public StatusDisease typedisease(TypeDisease typeDisease) {
        this.typedisease = typeDisease;
        return this;
    }

    public void setTypedisease(TypeDisease typeDisease) {
        this.typedisease = typeDisease;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StatusDisease)) {
            return false;
        }
        return id != null && id.equals(((StatusDisease) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "StatusDisease{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", determined='" + getDetermined() + "'" +
            ", status=" + getStatus() +
            ", type=" + getType() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", isEx=" + getIsEx() +
            ", likStatus=" + getLikStatus() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
