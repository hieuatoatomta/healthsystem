package mta.ato.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A TestDisease.
 */
@Entity
@Table(name = "test_disease")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TestDisease implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @Column(name = "code", length = 100)
    private String code;

    @Size(max = 100)
    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "status")
    private Long status;

    @Column(name = "update_time")
    private ZonedDateTime updateTime;

    @Size(max = 1000)
    @Column(name = "description", length = 1000)
    private String description;

    @OneToMany(mappedBy = "testdisease")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<TypeTest> dsTypeTests = new HashSet<>();

    @OneToMany(mappedBy = "testdisease")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Symptom> dsSymptoms = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public TestDisease code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public TestDisease name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStatus() {
        return status;
    }

    public TestDisease status(Long status) {
        this.status = status;
        return this;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public TestDisease updateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getDescription() {
        return description;
    }

    public TestDisease description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<TypeTest> getDsTypeTests() {
        return dsTypeTests;
    }

    public TestDisease dsTypeTests(Set<TypeTest> typeTests) {
        this.dsTypeTests = typeTests;
        return this;
    }

    public TestDisease addDsTypeTest(TypeTest typeTest) {
        this.dsTypeTests.add(typeTest);
        typeTest.setTestdisease(this);
        return this;
    }

    public TestDisease removeDsTypeTest(TypeTest typeTest) {
        this.dsTypeTests.remove(typeTest);
        typeTest.setTestdisease(null);
        return this;
    }

    public void setDsTypeTests(Set<TypeTest> typeTests) {
        this.dsTypeTests = typeTests;
    }

    public Set<Symptom> getDsSymptoms() {
        return dsSymptoms;
    }

    public TestDisease dsSymptoms(Set<Symptom> symptoms) {
        this.dsSymptoms = symptoms;
        return this;
    }

    public TestDisease addDsSymptom(Symptom symptom) {
        this.dsSymptoms.add(symptom);
        symptom.setTestdisease(this);
        return this;
    }

    public TestDisease removeDsSymptom(Symptom symptom) {
        this.dsSymptoms.remove(symptom);
        symptom.setTestdisease(null);
        return this;
    }

    public void setDsSymptoms(Set<Symptom> symptoms) {
        this.dsSymptoms = symptoms;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TestDisease)) {
            return false;
        }
        return id != null && id.equals(((TestDisease) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TestDisease{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", name='" + getName() + "'" +
            ", status=" + getStatus() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
