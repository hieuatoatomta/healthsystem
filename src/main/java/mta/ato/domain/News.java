package mta.ato.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A News.
 */
@Entity
@Table(name = "news")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class News implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 200)
    @Column(name = "code", length = 200)
    private String code;

    @Size(max = 300)
    @Column(name = "title", length = 300)
    private String title;

    @Size(max = 1000)
    @Column(name = "body_child", length = 1000)
    private String bodyChild;

    @Size(max = 10000)
    @Column(name = "body_news", length = 10000)
    private String bodyNews;

    @Size(max = 1000)
    @Column(name = "description", length = 1000)
    private String description;

    @Size(max = 1000)
    @Column(name = "reason_for_refusal", length = 1000)
    private String reasonForRefusal;

    @Column(name = "status")
    private Long status;

    @Column(name = "number_of_viewer")
    private Long numberOfViewer;

    @Column(name = "update_time")
    private ZonedDateTime updateTime;

    @Column(name = "highlights")
    private Long highlights;

    @Size(max = 1000)
    @Column(name = "image_link", length = 1000)
    private String imageLink;

    @OneToMany(mappedBy = "news", fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ImageLink> dsImageLinks = new HashSet<>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnoreProperties(value = "dsNews", allowSetters = true)
    private Category category;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnoreProperties(value = "dsNews", allowSetters = true)
    private Users users;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public News code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public News title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBodyChild() {
        return bodyChild;
    }

    public News bodyChild(String bodyChild) {
        this.bodyChild = bodyChild;
        return this;
    }

    public void setBodyChild(String bodyChild) {
        this.bodyChild = bodyChild;
    }

    public String getBodyNews() {
        return bodyNews;
    }

    public News bodyNews(String bodyNews) {
        this.bodyNews = bodyNews;
        return this;
    }

    public void setBodyNews(String bodyNews) {
        this.bodyNews = bodyNews;
    }

    public String getDescription() {
        return description;
    }

    public News description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReasonForRefusal() {
        return reasonForRefusal;
    }

    public News reasonForRefusal(String reasonForRefusal) {
        this.reasonForRefusal = reasonForRefusal;
        return this;
    }

    public void setReasonForRefusal(String reasonForRefusal) {
        this.reasonForRefusal = reasonForRefusal;
    }

    public Long getStatus() {
        return status;
    }

    public News status(Long status) {
        this.status = status;
        return this;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getNumberOfViewer() {
        return numberOfViewer;
    }

    public News numberOfViewer(Long numberOfViewer) {
        this.numberOfViewer = numberOfViewer;
        return this;
    }

    public void setNumberOfViewer(Long numberOfViewer) {
        this.numberOfViewer = numberOfViewer;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public News updateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Long getHighlights() {
        return highlights;
    }

    public News highlights(Long highlights) {
        this.highlights = highlights;
        return this;
    }

    public void setHighlights(Long highlights) {
        this.highlights = highlights;
    }

    public String getImageLink() {
        return imageLink;
    }

    public News imageLink(String imageLink) {
        this.imageLink = imageLink;
        return this;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public Set<ImageLink> getDsImageLinks() {
        return dsImageLinks;
    }

    public News dsImageLinks(Set<ImageLink> imageLinks) {
        this.dsImageLinks = imageLinks;
        return this;
    }

    public News addDsImageLink(ImageLink imageLink) {
        this.dsImageLinks.add(imageLink);
        imageLink.setNews(this);
        return this;
    }

    public News removeDsImageLink(ImageLink imageLink) {
        this.dsImageLinks.remove(imageLink);
        imageLink.setNews(null);
        return this;
    }

    public void setDsImageLinks(Set<ImageLink> imageLinks) {
        this.dsImageLinks = imageLinks;
    }

    public Category getCategory() {
        return category;
    }

    public News category(Category category) {
        this.category = category;
        return this;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Users getUsers() {
        return users;
    }

    public News users(Users users) {
        this.users = users;
        return this;
    }

    public void setUsers(Users users) {
        this.users = users;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof News)) {
            return false;
        }
        return id != null && id.equals(((News) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "News{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", title='" + getTitle() + "'" +
            ", bodyChild='" + getBodyChild() + "'" +
            ", bodyNews='" + getBodyNews() + "'" +
            ", description='" + getDescription() + "'" +
            ", reasonForRefusal='" + getReasonForRefusal() + "'" +
            ", status=" + getStatus() +
            ", numberOfViewer=" + getNumberOfViewer() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", highlights=" + getHighlights() +
            ", imageLink='" + getImageLink() + "'" +
            "}";
    }
}
