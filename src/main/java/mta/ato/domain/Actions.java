package mta.ato.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A Actions.
 */
@Entity
@Table(name = "actions")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Actions implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 60)
    @Column(name = "name", length = 60)
    private String name;

    @Size(max = 60)
    @Column(name = "code", length = 60)
    private String code;

    @Size(max = 1000)
    @Column(name = "description", length = 1000)
    private String description;

    @Column(name = "status")
    private Long status;

    @Column(name = "update_time")
    private ZonedDateTime updateTime;

    @OneToMany(mappedBy = "actions")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<RoleObject> dsRoleObjects = new HashSet<>();

    @OneToMany(mappedBy = "actions")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ObjectAction> dsObjectActions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Actions name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public Actions code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public Actions description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStatus() {
        return status;
    }

    public Actions status(Long status) {
        this.status = status;
        return this;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public Actions updateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Set<RoleObject> getDsRoleObjects() {
        return dsRoleObjects;
    }

    public Actions dsRoleObjects(Set<RoleObject> roleObjects) {
        this.dsRoleObjects = roleObjects;
        return this;
    }

    public Actions addDsRoleObject(RoleObject roleObject) {
        this.dsRoleObjects.add(roleObject);
        roleObject.setActions(this);
        return this;
    }

    public Actions removeDsRoleObject(RoleObject roleObject) {
        this.dsRoleObjects.remove(roleObject);
        roleObject.setActions(null);
        return this;
    }

    public void setDsRoleObjects(Set<RoleObject> roleObjects) {
        this.dsRoleObjects = roleObjects;
    }

    public Set<ObjectAction> getDsObjectActions() {
        return dsObjectActions;
    }

    public Actions dsObjectActions(Set<ObjectAction> objectActions) {
        this.dsObjectActions = objectActions;
        return this;
    }

    public Actions addDsObjectAction(ObjectAction objectAction) {
        this.dsObjectActions.add(objectAction);
        objectAction.setActions(this);
        return this;
    }

    public Actions removeDsObjectAction(ObjectAction objectAction) {
        this.dsObjectActions.remove(objectAction);
        objectAction.setActions(null);
        return this;
    }

    public void setDsObjectActions(Set<ObjectAction> objectActions) {
        this.dsObjectActions = objectActions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Actions)) {
            return false;
        }
        return id != null && id.equals(((Actions) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Actions{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            ", description='" + getDescription() + "'" +
            ", status=" + getStatus() +
            ", updateTime='" + getUpdateTime() + "'" +
            "}";
    }
}
