package mta.ato.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A TypeTest.
 */
@Entity
@Table(name = "type_test")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TypeTest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "update_time")
    private ZonedDateTime updateTime;

    @ManyToOne
    @JsonIgnoreProperties(value = "dsTypeTests", allowSetters = true)
    private TypeDisease typedisease;

    @ManyToOne
    @JsonIgnoreProperties(value = "dsTypeTests", allowSetters = true)
    private TestDisease testdisease;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public TypeTest updateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public TypeDisease getTypedisease() {
        return typedisease;
    }

    public TypeTest typedisease(TypeDisease typeDisease) {
        this.typedisease = typeDisease;
        return this;
    }

    public void setTypedisease(TypeDisease typeDisease) {
        this.typedisease = typeDisease;
    }

    public TestDisease getTestdisease() {
        return testdisease;
    }

    public TypeTest testdisease(TestDisease testDisease) {
        this.testdisease = testDisease;
        return this;
    }

    public void setTestdisease(TestDisease testDisease) {
        this.testdisease = testDisease;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TypeTest)) {
            return false;
        }
        return id != null && id.equals(((TypeTest) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TypeTest{" +
            "id=" + getId() +
            ", updateTime='" + getUpdateTime() + "'" +
            "}";
    }
}
