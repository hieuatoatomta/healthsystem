package mta.ato.repository;

import mta.ato.domain.TestDisease;

import mta.ato.domain.TypeDisease;
import mta.ato.domain.TypeTest;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the TestDisease entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TestDiseaseRepository extends JpaRepository<TestDisease, Long>, JpaSpecificationExecutor<TestDisease> {
    TestDisease findByCode(String code);

    @Query(value = "select td from  TestDisease td inner join TypeTest tt on td.id = tt.testdisease.id where td.status = 1 and tt.typedisease.id = :typediseaseId ")
    List<TestDisease> findByDetermined(@Param("typediseaseId") Long typediseaseId);
}
