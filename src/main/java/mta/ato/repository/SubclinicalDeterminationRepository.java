package mta.ato.repository;

import mta.ato.domain.SubclinicalDetermination;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the SubclinicalDetermination entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubclinicalDeterminationRepository extends JpaRepository<SubclinicalDetermination, Long>, JpaSpecificationExecutor<SubclinicalDetermination> {
    @Query(value = " select t.amount, t.statusdiseaseId,t.type, GROUP_CONCAT(t.nameXn) nameXn,  GROUP_CONCAT(t.id_tt) nameTT from (\n" +
        "SELECT DISTINCT\n" +
        "    sd.amount AS amount,\n" +
        "    sd.statusdisease_id AS statusdiseaseId,\n" +
        "    sd.type,\n" +
        "   td.name as nameXn,\n" +
        " GROUP_CONCAT(s.name) as id_tt\n" +
        "FROM\n" +
        "    healthSystem.subclinical_determination sd\n" +
        "    inner join test_disease td on td.id = sd.id_xn\n" +
        "    left join symptom as s on s.id = sd.id_tt\n" +
        "    where sd.statusdisease_id = :statusdiseaseId \n" +
        "    group by sd.type, sd.amount,  sd.statusdisease_id, td.name\n" +
        "    ) as t\n" +
        "GROUP BY t.type, t.amount, t.statusdiseaseId\n" +
        "order by t.amount ", nativeQuery = true)
    List<Object[]> getRoleAction(@Param("statusdiseaseId") Long statusdiseaseId);

    @Query(value = "\n" +
        "select max(t.ty) totalMax from (\n" +
        "SELECT count(sd.type) ty FROM healthSystem.subclinical_determination sd\n" +
        "where sd.statusdisease_id in (SELECT id FROM healthSystem.status_disease where type = 3 and typedisease_id = :typediseaseId) and sd.amount = :amount and sd.id_xn in :idXn \n" +
        "group by sd.type ) as t", nativeQuery = true )
    List<Object[]> findAllByAmountInIdXn(@Param("typediseaseId") Long typediseaseId, @Param("amount")Long amount, @Param("idXn") List<Long> idXn);

    List<SubclinicalDetermination> findAllByType(Long type);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM SubclinicalDetermination a WHERE a.type = :type")
    void deleteByType(@Param("type") Long type);
}
