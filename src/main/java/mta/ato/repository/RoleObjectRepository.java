package mta.ato.repository;

import mta.ato.domain.RoleObject;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data  repository for the RoleObject entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RoleObjectRepository extends JpaRepository<RoleObject, Long>, JpaSpecificationExecutor<RoleObject> {
    @Modifying
    @Transactional
    @Query(value = "DELETE FROM role_object a WHERE a.roles_id = :roles_id",nativeQuery = true)
    void deleteByRoleId(@Param("roles_id") Long roles_id);
}
