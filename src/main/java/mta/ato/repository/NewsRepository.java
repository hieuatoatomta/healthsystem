package mta.ato.repository;

import mta.ato.domain.Category;
import mta.ato.domain.News;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the News entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsRepository extends JpaRepository<News, Long>, JpaSpecificationExecutor<News> {
    List<News> findAllByCategoryAndStatus(Category category, Long Status, Pageable pageable);
    List<News> findAllByStatus(Long Status, Pageable pageable);
    List<News> findAllByStatusAndHighlights(Long Status, Long Highlights, Pageable pageable);
}
