package mta.ato.repository;

import mta.ato.domain.ObjectAction;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data  repository for the ObjectAction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ObjectActionRepository extends JpaRepository<ObjectAction, Long>, JpaSpecificationExecutor<ObjectAction> {
    @Modifying
    @Transactional
    @Query(value = "DELETE FROM `healthSystem`.`object_action` WHERE actions_id = :actions_id and objects_id = :objects_id ",nativeQuery = true)
    void deleteByObjectsAndAction(@Param("actions_id") Long actions_id, @Param("objects_id") Long objects_id);
}
