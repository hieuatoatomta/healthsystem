package mta.ato.repository;

import mta.ato.domain.TypeTest;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data  repository for the TypeTest entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TypeTestRepository extends JpaRepository<TypeTest, Long>, JpaSpecificationExecutor<TypeTest> {
    @Modifying
    @Transactional
    @Query(value = "DELETE FROM `healthSystem`.`type_test` WHERE testdisease_id = :testdisease_id and typedisease_id = :typedisease_id ",nativeQuery = true)
    void deleteByObjectsAndAction(@Param("testdisease_id") Long testdisease_id, @Param("typedisease_id") Long typedisease_id);
}
