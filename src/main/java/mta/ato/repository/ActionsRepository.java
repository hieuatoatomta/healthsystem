package mta.ato.repository;

import mta.ato.domain.Actions;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Actions entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ActionsRepository extends JpaRepository<Actions, Long>, JpaSpecificationExecutor<Actions> {
}
