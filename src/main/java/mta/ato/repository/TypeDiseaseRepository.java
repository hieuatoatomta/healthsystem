package mta.ato.repository;

import mta.ato.domain.TypeDisease;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TypeDisease entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TypeDiseaseRepository extends JpaRepository<TypeDisease, Long>, JpaSpecificationExecutor<TypeDisease> {
    TypeDisease findByCode(String code);
}
