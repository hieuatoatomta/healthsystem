package mta.ato.repository;

import mta.ato.domain.Symptom;

import mta.ato.domain.TestDisease;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Symptom entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SymptomRepository extends JpaRepository<Symptom, Long>, JpaSpecificationExecutor<Symptom> {
    @Query(value = "select st from  Symptom st where st.status = 1 and  st.typedisease.id = :typediseaseId and st.testdisease.id = :testdiseaseId")
    List<Symptom> findByDetermined(@Param("typediseaseId") Long typediseaseId, @Param("testdiseaseId") Long testdiseaseId);

    @Query(value = "select st from  Symptom st where st.status = 1 and  st.typedisease.id = :typediseaseId and st.testdisease.id in :listIdXn")
    List<Symptom> findByGroup(@Param("typediseaseId") Long typediseaseId, @Param("listIdXn") List<Long> listIdXn);
}
