package mta.ato.repository;

import mta.ato.domain.ExpertSystem;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ExpertSystem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExpertSystemRepository extends JpaRepository<ExpertSystem, Long>, JpaSpecificationExecutor<ExpertSystem> {
}
