package mta.ato.repository;

import mta.ato.domain.StatusDisease;

import mta.ato.domain.SubclinicalDetermination;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the StatusDisease entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StatusDiseaseRepository extends JpaRepository<StatusDisease, Long>, JpaSpecificationExecutor<StatusDisease> {
    @Query(value = "select std.id, std.description, std.name, std.status, std.update_time, std.typedisease_id, std.determined, std.type, std.lik_status, std.is_ex" +
        " from status_disease std where 1=1 and std.status = 1" +
        " and (:typediseaseId is null or std.typedisease_id = :typediseaseId )  and std.type = :type" +
        " and :determined between substring_index(std.determined,'-',1) and substring_index(std.determined,'-',-1) ", nativeQuery = true)
    List<StatusDisease> findByStatusAndDetermined(@Param("typediseaseId") Long typediseaseId, @Param("type") Long type, @Param("determined") Long determined);

    @Query(value = "select td.id,\n" +
        "    td.description,\n" +
        "    td.name,\n" +
        "    td.status,\n" +
        "    td.update_time,\n" +
        "    td.typedisease_id,\n" +
        "    td.determined,\n" +
        "    td.type,\n" +
        "    td.lik_status, td.is_ex from (\n" +
        "select GROUP_CONCAT(t.nameTT) nameTT,GROUP_CONCAT(t.id_xn ORDER BY t.id_xn ASC) nameXm, t.type, t.statusdisease_id from (\n" +
        "SELECT DISTINCT\n" +
        "           id_xn, sd.type, sd.statusdisease_id,  GROUP_CONCAT(sd.id_tt) AS nameTT\n" +
        "    FROM \n" +
        "        healthSystem.subclinical_determination sd\n" +
        "    WHERE\n" +
        "        sd.amount = :amount\n" +
        "     GROUP BY id_xn, sd.type, sd.statusdisease_id\n" +
        "     \n" +
        "     ) as t group by t.type, t.statusdisease_id having nameXm = :idXn and nameTT = :idTt ) as z    INNER JOIN\n" +
        "    status_disease AS td ON td.id = z.statusdisease_id where td.typedisease_id = :typediseaseId ", nativeQuery = true )
    List<StatusDisease> findAllByClinet(@Param("typediseaseId") Long typediseaseId, @Param("amount")Long amount, @Param("idXn") String idXn, @Param("idTt") String idTt);

    @Query(value = "select td.id,\n" +
        "    td.description,\n" +
        "    td.name,\n" +
        "    td.status,\n" +
        "    td.update_time,\n" +
        "    td.typedisease_id,\n" +
        "    td.determined,\n" +
        "    td.type,\n" +
        "    td.lik_status, td.is_ex from (\n" +
        "select GROUP_CONCAT(t.nameTT) nameTT,GROUP_CONCAT(t.id_xn ORDER BY t.id_xn ASC) nameXm, t.type, t.statusdisease_id from (\n" +
        "SELECT DISTINCT\n" +
        "           id_xn, sd.type, sd.statusdisease_id,  GROUP_CONCAT(sd.id_tt) AS nameTT\n" +
        "    FROM \n" +
        "        healthSystem.subclinical_determination sd\n" +
        "    WHERE\n" +
        "        sd.amount = :amount\n" +
        "     GROUP BY id_xn, sd.type, sd.statusdisease_id\n" +
        "     \n" +
        "     ) as t group by t.type, t.statusdisease_id having nameXm = :idXn and nameTT is null) as z    INNER JOIN\n" +
        "    status_disease AS td ON td.id = z.statusdisease_id where td.typedisease_id = :typediseaseId ", nativeQuery = true )
    List<StatusDisease> findAllByClinetIsCheck(@Param("typediseaseId") Long typediseaseId, @Param("amount")Long amount, @Param("idXn") String idXn);

    @Query(value = "SELECT *  FROM healthSystem.status_disease \n" +
        "where type = 4 and typedisease_id = :typediseaseId and \n" +
        "CONVERT(SUBSTRING_INDEX(determined,'-',-1),UNSIGNED INTEGER)  \n" +
        "= (SELECT max(type) FROM healthSystem.frequency where id in :lsId) ", nativeQuery = true )
    List<StatusDisease> getMax(@Param("typediseaseId") Long typediseaseId ,@Param("lsId") List<Long> lsId);


    @Query(value = "SELECT \n" +
        "    GROUP_CONCAT(subclinical_determination.id_xn order by subclinical_determination.id_xn) nameXn,  GROUP_CONCAT(subclinical_determination.id_tt order by subclinical_determination.id_tt) nameTt\n" +
        "FROM\n" +
        "    healthSystem.subclinical_determination\n" +
        "        INNER JOIN\n" +
        "    status_disease td ON subclinical_determination.statusdisease_id = td.id\n" +
        "WHERE\n" +
        "    td.typedisease_id = :typediseaseId \n" +
        "        AND subclinical_determination.id_check = 1\n" +
        "        group by subclinical_determination.type\n" +
        "        having nameXn = :idXn and nameTt = :idTt ", nativeQuery = true )
    List<Object[]> isCheck(@Param("typediseaseId") Long typediseaseId, @Param("idXn") String idXn, @Param("idTt") String idTt);
}
