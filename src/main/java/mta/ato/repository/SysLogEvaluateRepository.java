package mta.ato.repository;

import mta.ato.domain.SysLogEvaluate;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the SysLogEvaluate entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SysLogEvaluateRepository extends JpaRepository<SysLogEvaluate, Long>, JpaSpecificationExecutor<SysLogEvaluate> {
}
