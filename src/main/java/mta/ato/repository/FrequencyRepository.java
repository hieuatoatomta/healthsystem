package mta.ato.repository;

import mta.ato.domain.Frequency;

import mta.ato.domain.Symptom;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Frequency entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FrequencyRepository extends JpaRepository<Frequency, Long>, JpaSpecificationExecutor<Frequency> {
    List<Frequency> findAllBySymptom(Symptom symptom);

    @Query(value = "SELECT max(type) as maxCd FROM healthSystem.frequency where id in :lsId ", nativeQuery = true)
    List<Object[]> getMax(@Param("lsId") List<Long> lsId);

}
