package mta.ato.repository;

import mta.ato.domain.UserRole;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Spring Data  repository for the UserRole entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long>, JpaSpecificationExecutor<UserRole> {
    List<UserRole> findAllById(Long id);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM user_role a WHERE a.users_id = :users_id",nativeQuery = true)
    void deleteByUsersId(@Param("users_id") Long users_id);
}
