package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.TypeDiseaseDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TypeDisease} and its DTO {@link TypeDiseaseDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TypeDiseaseMapper extends EntityMapper<TypeDiseaseDTO, TypeDisease> {


    @Mapping(target = "dsTypeTests", ignore = true)
    @Mapping(target = "removeDsTypeTest", ignore = true)
    @Mapping(target = "dsSymptoms", ignore = true)
    @Mapping(target = "removeDsSymptom", ignore = true)
    @Mapping(target = "dsStatusDiseases", ignore = true)
    @Mapping(target = "removeDsStatusDisease", ignore = true)
    TypeDisease toEntity(TypeDiseaseDTO typeDiseaseDTO);

    default TypeDisease fromId(Long id) {
        if (id == null) {
            return null;
        }
        TypeDisease typeDisease = new TypeDisease();
        typeDisease.setId(id);
        return typeDisease;
    }
}
