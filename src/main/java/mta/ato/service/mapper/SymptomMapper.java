package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.SymptomDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Symptom} and its DTO {@link SymptomDTO}.
 */
@Mapper(componentModel = "spring", uses = {TypeDiseaseMapper.class, TestDiseaseMapper.class})
public interface SymptomMapper extends EntityMapper<SymptomDTO, Symptom> {

    @Mapping(source = "typedisease.id", target = "typediseaseId")
    @Mapping(source = "testdisease.id", target = "testdiseaseId")
    SymptomDTO toDto(Symptom symptom);

    @Mapping(target = "dsFrequencies", ignore = true)
    @Mapping(target = "removeDsFrequency", ignore = true)
    @Mapping(source = "typediseaseId", target = "typedisease")
    @Mapping(source = "testdiseaseId", target = "testdisease")
    Symptom toEntity(SymptomDTO symptomDTO);

    default Symptom fromId(Long id) {
        if (id == null) {
            return null;
        }
        Symptom symptom = new Symptom();
        symptom.setId(id);
        return symptom;
    }
}
