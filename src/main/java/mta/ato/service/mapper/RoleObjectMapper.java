package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.RoleObjectDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link RoleObject} and its DTO {@link RoleObjectDTO}.
 */
@Mapper(componentModel = "spring", uses = {RolesMapper.class, ActionsMapper.class, ObjectsMapper.class})
public interface RoleObjectMapper extends EntityMapper<RoleObjectDTO, RoleObject> {

    @Mapping(source = "roles.id", target = "rolesId")
    @Mapping(source = "actions.id", target = "actionsId")
    @Mapping(source = "objects.id", target = "objectsId")
    RoleObjectDTO toDto(RoleObject roleObject);

    @Mapping(source = "rolesId", target = "roles")
    @Mapping(source = "actionsId", target = "actions")
    @Mapping(source = "objectsId", target = "objects")
    RoleObject toEntity(RoleObjectDTO roleObjectDTO);

    default RoleObject fromId(Long id) {
        if (id == null) {
            return null;
        }
        RoleObject roleObject = new RoleObject();
        roleObject.setId(id);
        return roleObject;
    }
}
