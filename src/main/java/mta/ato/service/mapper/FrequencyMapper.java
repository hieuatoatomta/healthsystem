package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.FrequencyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Frequency} and its DTO {@link FrequencyDTO}.
 */
@Mapper(componentModel = "spring", uses = {SymptomMapper.class})
public interface FrequencyMapper extends EntityMapper<FrequencyDTO, Frequency> {

    @Mapping(source = "symptom.id", target = "symptomId")
    FrequencyDTO toDto(Frequency frequency);

    @Mapping(source = "symptomId", target = "symptom")
    Frequency toEntity(FrequencyDTO frequencyDTO);

    default Frequency fromId(Long id) {
        if (id == null) {
            return null;
        }
        Frequency frequency = new Frequency();
        frequency.setId(id);
        return frequency;
    }
}
