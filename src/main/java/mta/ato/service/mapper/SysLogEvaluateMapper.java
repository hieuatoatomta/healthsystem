package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.SysLogEvaluateDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link SysLogEvaluate} and its DTO {@link SysLogEvaluateDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SysLogEvaluateMapper extends EntityMapper<SysLogEvaluateDTO, SysLogEvaluate> {



    default SysLogEvaluate fromId(Long id) {
        if (id == null) {
            return null;
        }
        SysLogEvaluate sysLogEvaluate = new SysLogEvaluate();
        sysLogEvaluate.setId(id);
        return sysLogEvaluate;
    }
}
