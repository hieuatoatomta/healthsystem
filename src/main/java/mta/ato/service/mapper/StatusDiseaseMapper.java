package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.StatusDiseaseDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link StatusDisease} and its DTO {@link StatusDiseaseDTO}.
 */
@Mapper(componentModel = "spring", uses = {TypeDiseaseMapper.class})
public interface StatusDiseaseMapper extends EntityMapper<StatusDiseaseDTO, StatusDisease> {

    @Mapping(source = "typedisease.id", target = "typediseaseId")
    StatusDiseaseDTO toDto(StatusDisease statusDisease);

    @Mapping(target = "dsSubclinicalDeterminations", ignore = true)
    @Mapping(target = "removeDsSubclinicalDetermination", ignore = true)
    @Mapping(source = "typediseaseId", target = "typedisease")
    StatusDisease toEntity(StatusDiseaseDTO statusDiseaseDTO);

    default StatusDisease fromId(Long id) {
        if (id == null) {
            return null;
        }
        StatusDisease statusDisease = new StatusDisease();
        statusDisease.setId(id);
        return statusDisease;
    }
}
