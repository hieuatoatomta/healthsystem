package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.UsersDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Users} and its DTO {@link UsersDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UsersMapper extends EntityMapper<UsersDTO, Users> {


    @Mapping(target = "dsUserRoles", ignore = true)
    @Mapping(target = "removeDsUserRole", ignore = true)
    Users toEntity(UsersDTO usersDTO);

    default Users fromId(Long id) {
        if (id == null) {
            return null;
        }
        Users users = new Users();
        users.setId(id);
        return users;
    }
}
