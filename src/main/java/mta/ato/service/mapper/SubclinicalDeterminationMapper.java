package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.SubclinicalDeterminationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link SubclinicalDetermination} and its DTO {@link SubclinicalDeterminationDTO}.
 */
@Mapper(componentModel = "spring", uses = {StatusDiseaseMapper.class})
public interface SubclinicalDeterminationMapper extends EntityMapper<SubclinicalDeterminationDTO, SubclinicalDetermination> {

    @Mapping(source = "statusdisease.id", target = "statusdiseaseId")
    SubclinicalDeterminationDTO toDto(SubclinicalDetermination subclinicalDetermination);

    @Mapping(source = "statusdiseaseId", target = "statusdisease")
    SubclinicalDetermination toEntity(SubclinicalDeterminationDTO subclinicalDeterminationDTO);

    default SubclinicalDetermination fromId(Long id) {
        if (id == null) {
            return null;
        }
        SubclinicalDetermination subclinicalDetermination = new SubclinicalDetermination();
        subclinicalDetermination.setId(id);
        return subclinicalDetermination;
    }
}
