package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.ImageLinkDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ImageLink} and its DTO {@link ImageLinkDTO}.
 */
@Mapper(componentModel = "spring", uses = {NewsMapper.class})
public interface ImageLinkMapper extends EntityMapper<ImageLinkDTO, ImageLink> {

    @Mapping(source = "news.id", target = "newsId")
    ImageLinkDTO toDto(ImageLink imageLink);

    @Mapping(source = "newsId", target = "news")
    ImageLink toEntity(ImageLinkDTO imageLinkDTO);

    default ImageLink fromId(Long id) {
        if (id == null) {
            return null;
        }
        ImageLink imageLink = new ImageLink();
        imageLink.setId(id);
        return imageLink;
    }
}
