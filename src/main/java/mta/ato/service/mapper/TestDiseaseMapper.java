package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.TestDiseaseDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TestDisease} and its DTO {@link TestDiseaseDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TestDiseaseMapper extends EntityMapper<TestDiseaseDTO, TestDisease> {


    @Mapping(target = "dsTypeTests", ignore = true)
    @Mapping(target = "removeDsTypeTest", ignore = true)
    @Mapping(target = "dsSymptoms", ignore = true)
    @Mapping(target = "removeDsSymptom", ignore = true)
    TestDisease toEntity(TestDiseaseDTO testDiseaseDTO);

    default TestDisease fromId(Long id) {
        if (id == null) {
            return null;
        }
        TestDisease testDisease = new TestDisease();
        testDisease.setId(id);
        return testDisease;
    }
}
