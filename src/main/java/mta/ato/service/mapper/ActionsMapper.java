package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.ActionsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Actions} and its DTO {@link ActionsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ActionsMapper extends EntityMapper<ActionsDTO, Actions> {


    @Mapping(target = "dsRoleObjects", ignore = true)
    @Mapping(target = "removeDsRoleObject", ignore = true)
    @Mapping(target = "dsObjectActions", ignore = true)
    @Mapping(target = "removeDsObjectAction", ignore = true)
    Actions toEntity(ActionsDTO actionsDTO);

    default Actions fromId(Long id) {
        if (id == null) {
            return null;
        }
        Actions actions = new Actions();
        actions.setId(id);
        return actions;
    }
}
