package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.TypeTestDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TypeTest} and its DTO {@link TypeTestDTO}.
 */
@Mapper(componentModel = "spring", uses = {TypeDiseaseMapper.class, TestDiseaseMapper.class})
public interface TypeTestMapper extends EntityMapper<TypeTestDTO, TypeTest> {

    @Mapping(source = "typedisease.id", target = "typediseaseId")
    @Mapping(source = "testdisease.id", target = "testdiseaseId")
    TypeTestDTO toDto(TypeTest typeTest);

    @Mapping(source = "typediseaseId", target = "typedisease")
    @Mapping(source = "testdiseaseId", target = "testdisease")
    TypeTest toEntity(TypeTestDTO typeTestDTO);

    default TypeTest fromId(Long id) {
        if (id == null) {
            return null;
        }
        TypeTest typeTest = new TypeTest();
        typeTest.setId(id);
        return typeTest;
    }
}
