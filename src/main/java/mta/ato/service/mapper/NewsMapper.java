package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.NewsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link News} and its DTO {@link NewsDTO}.
 */
@Mapper(componentModel = "spring", uses = {CategoryMapper.class, UsersMapper.class})
public interface NewsMapper extends EntityMapper<NewsDTO, News> {

    @Mapping(source = "category.id", target = "categoryId")
    @Mapping(source = "users.id", target = "usersId")
    NewsDTO toDto(News news);

    @Mapping(target = "dsImageLinks", ignore = true)
    @Mapping(target = "removeDsImageLink", ignore = true)
    @Mapping(source = "categoryId", target = "category")
    @Mapping(source = "usersId", target = "users")
    News toEntity(NewsDTO newsDTO);

    default News fromId(Long id) {
        if (id == null) {
            return null;
        }
        News news = new News();
        news.setId(id);
        return news;
    }
}
