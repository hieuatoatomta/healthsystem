package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.ExpertSystemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ExpertSystem} and its DTO {@link ExpertSystemDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ExpertSystemMapper extends EntityMapper<ExpertSystemDTO, ExpertSystem> {



    default ExpertSystem fromId(Long id) {
        if (id == null) {
            return null;
        }
        ExpertSystem expertSystem = new ExpertSystem();
        expertSystem.setId(id);
        return expertSystem;
    }
}
