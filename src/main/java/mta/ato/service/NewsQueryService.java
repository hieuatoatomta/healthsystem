package mta.ato.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mta.ato.domain.News;
import mta.ato.domain.*; // for static metamodels
import mta.ato.repository.NewsRepository;
import mta.ato.service.dto.NewsCriteria;
import mta.ato.service.dto.NewsDTO;
import mta.ato.service.mapper.NewsMapper;

/**
 * Service for executing complex queries for {@link News} entities in the database.
 * The main input is a {@link NewsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link NewsDTO} or a {@link Page} of {@link NewsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class NewsQueryService extends QueryService<News> {

    private final Logger log = LoggerFactory.getLogger(NewsQueryService.class);

    private final NewsRepository newsRepository;

    private final NewsMapper newsMapper;

    public NewsQueryService(NewsRepository newsRepository, NewsMapper newsMapper) {
        this.newsRepository = newsRepository;
        this.newsMapper = newsMapper;
    }

    /**
     * Return a {@link List} of {@link NewsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsDTO> findByCriteria(NewsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<News> specification = createSpecification(criteria);
        return newsMapper.toDto(newsRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link NewsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<News> findByCriteria(NewsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<News> specification = createSpecification(criteria);
        return newsRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(NewsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<News> specification = createSpecification(criteria);
        return newsRepository.count(specification);
    }

    /**
     * Function to convert {@link NewsCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<News> createSpecification(NewsCriteria criteria) {
        Specification<News> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), News_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), News_.code));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), News_.title));
            }
            if (criteria.getBodyChild() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBodyChild(), News_.bodyChild));
            }
            if (criteria.getBodyNews() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBodyNews(), News_.bodyNews));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), News_.description));
            }
            if (criteria.getReasonForRefusal() != null) {
                specification = specification.and(buildStringSpecification(criteria.getReasonForRefusal(), News_.reasonForRefusal));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatus(), News_.status));
            }
            if (criteria.getNumberOfViewer() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumberOfViewer(), News_.numberOfViewer));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), News_.updateTime));
            }
            if (criteria.getHighlights() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getHighlights(), News_.highlights));
            }
            if (criteria.getImageLink() != null) {
                specification = specification.and(buildStringSpecification(criteria.getImageLink(), News_.imageLink));
            }
            if (criteria.getDsImageLinkId() != null) {
                specification = specification.and(buildSpecification(criteria.getDsImageLinkId(),
                    root -> root.join(News_.dsImageLinks, JoinType.LEFT).get(ImageLink_.id)));
            }
            if (criteria.getCategoryId() != null) {
                specification = specification.and(buildSpecification(criteria.getCategoryId(),
                    root -> root.join(News_.category, JoinType.LEFT).get(Category_.id)));
            }
            if (criteria.getUsersId() != null) {
                specification = specification.and(buildSpecification(criteria.getUsersId(),
                    root -> root.join(News_.users, JoinType.LEFT).get(Users_.id)));
            }
        }
        return specification;
    }
}
