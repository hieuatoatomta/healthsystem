package mta.ato.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mta.ato.domain.UserRole;
import mta.ato.domain.*; // for static metamodels
import mta.ato.repository.UserRoleRepository;
import mta.ato.service.dto.UserRoleCriteria;
import mta.ato.service.dto.UserRoleDTO;
import mta.ato.service.mapper.UserRoleMapper;

/**
 * Service for executing complex queries for {@link UserRole} entities in the database.
 * The main input is a {@link UserRoleCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UserRoleDTO} or a {@link Page} of {@link UserRoleDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UserRoleQueryService extends QueryService<UserRole> {

    private final Logger log = LoggerFactory.getLogger(UserRoleQueryService.class);

    private final UserRoleRepository userRoleRepository;

    private final UserRoleMapper userRoleMapper;

    public UserRoleQueryService(UserRoleRepository userRoleRepository, UserRoleMapper userRoleMapper) {
        this.userRoleRepository = userRoleRepository;
        this.userRoleMapper = userRoleMapper;
    }

    /**
     * Return a {@link List} of {@link UserRoleDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UserRoleDTO> findByCriteria(UserRoleCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<UserRole> specification = createSpecification(criteria);
        return userRoleMapper.toDto(userRoleRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link UserRoleDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UserRoleDTO> findByCriteria(UserRoleCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<UserRole> specification = createSpecification(criteria);
        return userRoleRepository.findAll(specification, page)
            .map(userRoleMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UserRoleCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UserRole> specification = createSpecification(criteria);
        return userRoleRepository.count(specification);
    }

    /**
     * Function to convert {@link UserRoleCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<UserRole> createSpecification(UserRoleCriteria criteria) {
        Specification<UserRole> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), UserRole_.id));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), UserRole_.updateTime));
            }
            if (criteria.getUsersId() != null) {
                specification = specification.and(buildSpecification(criteria.getUsersId(),
                    root -> root.join(UserRole_.users, JoinType.LEFT).get(Users_.id)));
            }
            if (criteria.getRolesId() != null) {
                specification = specification.and(buildSpecification(criteria.getRolesId(),
                    root -> root.join(UserRole_.roles, JoinType.LEFT).get(Roles_.id)));
            }
        }
        return specification;
    }
}
