package mta.ato.service;

import mta.ato.domain.ExpertSystem;
import mta.ato.repository.ExpertSystemRepository;
import mta.ato.service.dto.ExpertSystemDTO;
import mta.ato.service.mapper.ExpertSystemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ExpertSystem}.
 */
@Service
@Transactional
public class ExpertSystemService {

    private final Logger log = LoggerFactory.getLogger(ExpertSystemService.class);

    private final ExpertSystemRepository expertSystemRepository;

    private final ExpertSystemMapper expertSystemMapper;

    public ExpertSystemService(ExpertSystemRepository expertSystemRepository, ExpertSystemMapper expertSystemMapper) {
        this.expertSystemRepository = expertSystemRepository;
        this.expertSystemMapper = expertSystemMapper;
    }

    /**
     * Save a expertSystem.
     *
     * @param expertSystemDTO the entity to save.
     * @return the persisted entity.
     */
    public ExpertSystemDTO save(ExpertSystemDTO expertSystemDTO) {
        log.debug("Request to save ExpertSystem : {}", expertSystemDTO);
        ExpertSystem expertSystem = expertSystemMapper.toEntity(expertSystemDTO);
        expertSystem = expertSystemRepository.save(expertSystem);
        return expertSystemMapper.toDto(expertSystem);
    }

    /**
     * Get all the expertSystems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ExpertSystemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ExpertSystems");
        return expertSystemRepository.findAll(pageable)
            .map(expertSystemMapper::toDto);
    }


    /**
     * Get one expertSystem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ExpertSystemDTO> findOne(Long id) {
        log.debug("Request to get ExpertSystem : {}", id);
        return expertSystemRepository.findById(id)
            .map(expertSystemMapper::toDto);
    }

    /**
     * Delete the expertSystem by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ExpertSystem : {}", id);
        expertSystemRepository.deleteById(id);
    }
}
