package mta.ato.service;

import mta.ato.domain.ExpertSystem;
import mta.ato.domain.StatusDisease;
import mta.ato.domain.SubclinicalDetermination;
import mta.ato.repository.StatusDiseaseRepository;
import mta.ato.service.dto.ExpertSystemDTO;
import mta.ato.service.dto.StatusDiseaseDTO;
import mta.ato.service.dto.SubclinicalDeterminationDTO;
import mta.ato.service.mapper.StatusDiseaseMapper;
import mta.ato.utils.DataUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link StatusDisease}.
 */
@Service
@Transactional
public class StatusDiseaseService {

    private final Logger log = LoggerFactory.getLogger(StatusDiseaseService.class);

    private final StatusDiseaseRepository statusDiseaseRepository;

    private final StatusDiseaseMapper statusDiseaseMapper;

    private final ExpertSystemService expertSystemService;

    public StatusDiseaseService(StatusDiseaseRepository statusDiseaseRepository,
                                ExpertSystemService expertSystemService,
                                StatusDiseaseMapper statusDiseaseMapper) {
        this.statusDiseaseRepository = statusDiseaseRepository;
        this.statusDiseaseMapper = statusDiseaseMapper;
        this.expertSystemService = expertSystemService;
    }

    /**
     * Save a statusDisease.
     *
     * @param statusDiseaseDTO the entity to save.
     * @return the persisted entity.
     */
    public StatusDiseaseDTO save(StatusDiseaseDTO statusDiseaseDTO) {
        log.debug("Request to save StatusDisease : {}", statusDiseaseDTO);
        StatusDisease statusDisease = statusDiseaseMapper.toEntity(statusDiseaseDTO);
        statusDisease = statusDiseaseRepository.save(statusDisease);
        return statusDiseaseMapper.toDto(statusDisease);
    }

    /**
     * Get all the statusDiseases.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<StatusDiseaseDTO> findAll(Pageable pageable) {
        log.debug("Request to get all StatusDiseases");
        return statusDiseaseRepository.findAll(pageable)
            .map(statusDiseaseMapper::toDto);
    }


    /**
     * Get one statusDisease by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<StatusDiseaseDTO> findOne(Long id) {
        log.debug("Request to get StatusDisease : {}", id);
        return statusDiseaseRepository.findById(id)
            .map(statusDiseaseMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<StatusDiseaseDTO> findByStatusAndDetermined(Long typediseaseId, Long type, Long value) {
        log.debug("Request to get StatusDisease : {}", value);
        List<StatusDisease> statusDiseaseList = statusDiseaseRepository.findByStatusAndDetermined(typediseaseId, type, value);
        List<StatusDiseaseDTO> statusDiseaseDTOList = new ArrayList<>();
        for (int i = 0; i < statusDiseaseList.size(); i++) {
            StatusDiseaseDTO statusDiseaseDTO = statusDiseaseMapper.toDto(statusDiseaseList.get(i));
            if (statusDiseaseList.get(i).getIsEx() != null){
                Optional<ExpertSystemDTO> expertSystem = expertSystemService.findOne(statusDiseaseList.get(i).getIsEx());
                if(expertSystem.isPresent()){
                    statusDiseaseDTO.setIsExLink(expertSystem.get().getCode());
                }
            }
            statusDiseaseDTOList.add(statusDiseaseDTO);
        }
        return statusDiseaseDTOList;
    }

    /**
     * Delete the statusDisease by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete StatusDisease : {}", id);
        statusDiseaseRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public List<StatusDiseaseDTO> findAllByClinet(Long typediseaseId, Long id, String idXn, String idTt) {
        log.debug("Request to get all SubclinicalDeterminations");
        List<StatusDisease> statusDiseaseList = statusDiseaseRepository.findAllByClinet(typediseaseId, id, idXn, idTt);
        List<StatusDiseaseDTO> statusDiseaseDTOList = new ArrayList<>();
        for (int i = 0; i < statusDiseaseList.size(); i++) {
            StatusDiseaseDTO statusDiseaseDTO = statusDiseaseMapper.toDto(statusDiseaseList.get(i));
            if (statusDiseaseList.get(i).getIsEx() != null){
                Optional<ExpertSystemDTO> expertSystem = expertSystemService.findOne(statusDiseaseList.get(i).getIsEx());
                if(expertSystem.isPresent()){
                    statusDiseaseDTO.setIsExLink(expertSystem.get().getCode());
                }
            }
            statusDiseaseDTOList.add(statusDiseaseDTO);
        }
        return statusDiseaseDTOList;
    }
    @Transactional(readOnly = true)
    public List<StatusDiseaseDTO> findAllByClinetIsCheck(Long typediseaseId, Long id, String idXn) {
        log.debug("Request to get all SubclinicalDeterminations");
        List<StatusDisease> statusDiseaseList = statusDiseaseRepository.findAllByClinetIsCheck(typediseaseId, id, idXn);
        List<StatusDiseaseDTO> statusDiseaseDTOList = new ArrayList<>();
        for (int i = 0; i < statusDiseaseList.size(); i++) {
            StatusDiseaseDTO statusDiseaseDTO = statusDiseaseMapper.toDto(statusDiseaseList.get(i));
            if (statusDiseaseList.get(i).getIsEx() != null){
                Optional<ExpertSystemDTO> expertSystem = expertSystemService.findOne(statusDiseaseList.get(i).getIsEx());
                if(expertSystem.isPresent()){
                    statusDiseaseDTO.setIsExLink(expertSystem.get().getCode());
                }
            }
            statusDiseaseDTOList.add(statusDiseaseDTO);
        }
        return statusDiseaseDTOList;
    }

    @Transactional(readOnly = true)
    public List<StatusDiseaseDTO> getMax(Long typediseaseId, List<Long> idXn) {
        log.debug("Request to get all SubclinicalDeterminations");
        List<StatusDisease> statusDiseaseList = statusDiseaseRepository.getMax(typediseaseId, idXn);
        List<StatusDiseaseDTO> statusDiseaseDTOList = new ArrayList<>();
        for (int i = 0; i < statusDiseaseList.size(); i++) {
            StatusDiseaseDTO statusDiseaseDTO = statusDiseaseMapper.toDto(statusDiseaseList.get(i));
            if (statusDiseaseList.get(i).getIsEx() != null){
                Optional<ExpertSystemDTO> expertSystem = expertSystemService.findOne(statusDiseaseList.get(i).getIsEx());
                if(expertSystem.isPresent()){
                    statusDiseaseDTO.setIsExLink(expertSystem.get().getCode());
                }
            }
            statusDiseaseDTOList.add(statusDiseaseDTO);
        }
        return statusDiseaseDTOList;
    }

    @Transactional(readOnly = true)
    public List<StatusDiseaseDTO> isCheck(Long typediseaseId ,String idXn, String idTt) {
        log.debug("Request to get all SubclinicalDeterminations");
        List<StatusDiseaseDTO> lstObject = statusDiseaseRepository.isCheck(typediseaseId, idXn, idTt)
            .stream().map( e -> {
                if(null == e){
                    StatusDiseaseDTO dto = new StatusDiseaseDTO();
                    dto.setTotalMax(0L);
                    return dto;
                } else {
                    StatusDiseaseDTO dto = new StatusDiseaseDTO();
                    dto.setNameXn( DataUtil.safeToString( e[0] ) );
                    dto.setNameTT( DataUtil.safeToString( e[1] ) );
                    return dto;
                }
            } ).collect( Collectors.toList() );
        return lstObject;
    }
}
