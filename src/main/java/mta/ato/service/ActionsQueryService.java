package mta.ato.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mta.ato.domain.Actions;
import mta.ato.domain.*; // for static metamodels
import mta.ato.repository.ActionsRepository;
import mta.ato.service.dto.ActionsCriteria;
import mta.ato.service.dto.ActionsDTO;
import mta.ato.service.mapper.ActionsMapper;

/**
 * Service for executing complex queries for {@link Actions} entities in the database.
 * The main input is a {@link ActionsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ActionsDTO} or a {@link Page} of {@link ActionsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ActionsQueryService extends QueryService<Actions> {

    private final Logger log = LoggerFactory.getLogger(ActionsQueryService.class);

    private final ActionsRepository actionsRepository;

    private final ActionsMapper actionsMapper;

    public ActionsQueryService(ActionsRepository actionsRepository, ActionsMapper actionsMapper) {
        this.actionsRepository = actionsRepository;
        this.actionsMapper = actionsMapper;
    }

    /**
     * Return a {@link List} of {@link ActionsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ActionsDTO> findByCriteria(ActionsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Actions> specification = createSpecification(criteria);
        return actionsMapper.toDto(actionsRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ActionsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Actions> findByCriteria(ActionsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Actions> specification = createSpecification(criteria);
        return actionsRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ActionsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Actions> specification = createSpecification(criteria);
        return actionsRepository.count(specification);
    }

    /**
     * Function to convert {@link ActionsCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Actions> createSpecification(ActionsCriteria criteria) {
        Specification<Actions> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Actions_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Actions_.name));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), Actions_.code));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Actions_.description));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatus(), Actions_.status));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), Actions_.updateTime));
            }
            if (criteria.getDsRoleObjectId() != null) {
                specification = specification.and(buildSpecification(criteria.getDsRoleObjectId(),
                    root -> root.join(Actions_.dsRoleObjects, JoinType.LEFT).get(RoleObject_.id)));
            }
            if (criteria.getDsObjectActionId() != null) {
                specification = specification.and(buildSpecification(criteria.getDsObjectActionId(),
                    root -> root.join(Actions_.dsObjectActions, JoinType.LEFT).get(ObjectAction_.id)));
            }
        }
        return specification;
    }
}
