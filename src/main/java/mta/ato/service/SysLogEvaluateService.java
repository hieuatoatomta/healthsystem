package mta.ato.service;

import mta.ato.domain.SysLogEvaluate;
import mta.ato.repository.SysLogEvaluateRepository;
import mta.ato.service.dto.SysLogEvaluateDTO;
import mta.ato.service.mapper.SysLogEvaluateMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link SysLogEvaluate}.
 */
@Service
@Transactional
public class SysLogEvaluateService {

    private final Logger log = LoggerFactory.getLogger(SysLogEvaluateService.class);

    private final SysLogEvaluateRepository sysLogEvaluateRepository;

    private final SysLogEvaluateMapper sysLogEvaluateMapper;

    public SysLogEvaluateService(SysLogEvaluateRepository sysLogEvaluateRepository, SysLogEvaluateMapper sysLogEvaluateMapper) {
        this.sysLogEvaluateRepository = sysLogEvaluateRepository;
        this.sysLogEvaluateMapper = sysLogEvaluateMapper;
    }

    /**
     * Save a sysLogEvaluate.
     *
     * @param sysLogEvaluateDTO the entity to save.
     * @return the persisted entity.
     */
    public SysLogEvaluateDTO save(SysLogEvaluateDTO sysLogEvaluateDTO) {
        log.debug("Request to save SysLogEvaluate : {}", sysLogEvaluateDTO);
        SysLogEvaluate sysLogEvaluate = sysLogEvaluateMapper.toEntity(sysLogEvaluateDTO);
        sysLogEvaluate = sysLogEvaluateRepository.save(sysLogEvaluate);
        return sysLogEvaluateMapper.toDto(sysLogEvaluate);
    }

    /**
     * Get all the sysLogEvaluates.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SysLogEvaluateDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SysLogEvaluates");
        return sysLogEvaluateRepository.findAll(pageable)
            .map(sysLogEvaluateMapper::toDto);
    }


    /**
     * Get one sysLogEvaluate by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SysLogEvaluateDTO> findOne(Long id) {
        log.debug("Request to get SysLogEvaluate : {}", id);
        return sysLogEvaluateRepository.findById(id)
            .map(sysLogEvaluateMapper::toDto);
    }

    /**
     * Delete the sysLogEvaluate by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete SysLogEvaluate : {}", id);
        sysLogEvaluateRepository.deleteById(id);
    }
}
