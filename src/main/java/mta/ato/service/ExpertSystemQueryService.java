package mta.ato.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mta.ato.domain.ExpertSystem;
import mta.ato.domain.*; // for static metamodels
import mta.ato.repository.ExpertSystemRepository;
import mta.ato.service.dto.ExpertSystemCriteria;
import mta.ato.service.dto.ExpertSystemDTO;
import mta.ato.service.mapper.ExpertSystemMapper;

/**
 * Service for executing complex queries for {@link ExpertSystem} entities in the database.
 * The main input is a {@link ExpertSystemCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ExpertSystemDTO} or a {@link Page} of {@link ExpertSystemDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ExpertSystemQueryService extends QueryService<ExpertSystem> {

    private final Logger log = LoggerFactory.getLogger(ExpertSystemQueryService.class);

    private final ExpertSystemRepository expertSystemRepository;

    private final ExpertSystemMapper expertSystemMapper;

    public ExpertSystemQueryService(ExpertSystemRepository expertSystemRepository, ExpertSystemMapper expertSystemMapper) {
        this.expertSystemRepository = expertSystemRepository;
        this.expertSystemMapper = expertSystemMapper;
    }

    /**
     * Return a {@link List} of {@link ExpertSystemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ExpertSystemDTO> findByCriteria(ExpertSystemCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ExpertSystem> specification = createSpecification(criteria);
        return expertSystemMapper.toDto(expertSystemRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ExpertSystemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ExpertSystem> findByCriteria(ExpertSystemCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ExpertSystem> specification = createSpecification(criteria);
        return expertSystemRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ExpertSystemCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ExpertSystem> specification = createSpecification(criteria);
        return expertSystemRepository.count(specification);
    }

    /**
     * Function to convert {@link ExpertSystemCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ExpertSystem> createSpecification(ExpertSystemCriteria criteria) {
        Specification<ExpertSystem> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ExpertSystem_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), ExpertSystem_.code));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), ExpertSystem_.name));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatus(), ExpertSystem_.status));
            }
            if (criteria.getParentId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getParentId(), ExpertSystem_.parentId));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getType(), ExpertSystem_.type));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), ExpertSystem_.updateTime));
            }
            if (criteria.getIsLink() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsLink(), ExpertSystem_.isLink));
            }
        }
        return specification;
    }
}
