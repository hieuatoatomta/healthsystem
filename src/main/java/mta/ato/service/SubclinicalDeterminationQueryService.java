package mta.ato.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mta.ato.domain.SubclinicalDetermination;
import mta.ato.domain.*; // for static metamodels
import mta.ato.repository.SubclinicalDeterminationRepository;
import mta.ato.service.dto.SubclinicalDeterminationCriteria;
import mta.ato.service.dto.SubclinicalDeterminationDTO;
import mta.ato.service.mapper.SubclinicalDeterminationMapper;

/**
 * Service for executing complex queries for {@link SubclinicalDetermination} entities in the database.
 * The main input is a {@link SubclinicalDeterminationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SubclinicalDeterminationDTO} or a {@link Page} of {@link SubclinicalDeterminationDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SubclinicalDeterminationQueryService extends QueryService<SubclinicalDetermination> {

    private final Logger log = LoggerFactory.getLogger(SubclinicalDeterminationQueryService.class);

    private final SubclinicalDeterminationRepository subclinicalDeterminationRepository;

    private final SubclinicalDeterminationMapper subclinicalDeterminationMapper;

    public SubclinicalDeterminationQueryService(SubclinicalDeterminationRepository subclinicalDeterminationRepository, SubclinicalDeterminationMapper subclinicalDeterminationMapper) {
        this.subclinicalDeterminationRepository = subclinicalDeterminationRepository;
        this.subclinicalDeterminationMapper = subclinicalDeterminationMapper;
    }

    /**
     * Return a {@link List} of {@link SubclinicalDeterminationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SubclinicalDeterminationDTO> findByCriteria(SubclinicalDeterminationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<SubclinicalDetermination> specification = createSpecification(criteria);
        return subclinicalDeterminationMapper.toDto(subclinicalDeterminationRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SubclinicalDeterminationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SubclinicalDeterminationDTO> findByCriteria(SubclinicalDeterminationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<SubclinicalDetermination> specification = createSpecification(criteria);
        return subclinicalDeterminationRepository.findAll(specification, page)
            .map(subclinicalDeterminationMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SubclinicalDeterminationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<SubclinicalDetermination> specification = createSpecification(criteria);
        return subclinicalDeterminationRepository.count(specification);
    }

    /**
     * Function to convert {@link SubclinicalDeterminationCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<SubclinicalDetermination> createSpecification(SubclinicalDeterminationCriteria criteria) {
        Specification<SubclinicalDetermination> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), SubclinicalDetermination_.id));
            }
            if (criteria.getAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAmount(), SubclinicalDetermination_.amount));
            }
            if (criteria.getIdXn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdXn(), SubclinicalDetermination_.idXn));
            }
            if (criteria.getIdCheck() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdCheck(), SubclinicalDetermination_.idCheck));
            }
            if (criteria.getIdTT() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdTT(), SubclinicalDetermination_.idTT));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getType(), SubclinicalDetermination_.type));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), SubclinicalDetermination_.updateTime));
            }
            if (criteria.getStatusdiseaseId() != null) {
                specification = specification.and(buildSpecification(criteria.getStatusdiseaseId(),
                    root -> root.join(SubclinicalDetermination_.statusdisease, JoinType.LEFT).get(StatusDisease_.id)));
            }
        }
        return specification;
    }
}
