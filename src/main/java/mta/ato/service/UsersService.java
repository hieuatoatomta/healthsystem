package mta.ato.service;

import mta.ato.config.CustomUserDetails;
import mta.ato.domain.Users;
import mta.ato.repository.UsersRepository;
import mta.ato.service.dto.UserRoleDTO;
import mta.ato.service.dto.UsersClientDTO;
import mta.ato.service.dto.UsersDTO;
import mta.ato.service.mapper.UsersMapper;
import mta.ato.utils.DateUtil;
import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Users}.
 */
@Service
@Transactional
public class UsersService {

    private final Logger log = LoggerFactory.getLogger(UsersService.class);

    private final UsersRepository usersRepository;

    private final UsersMapper usersMapper;

    private final UserRoleService userRoleService;

    private final RolesService rolesService;

    public UsersService(UsersRepository usersRepository,
                        UsersMapper usersMapper,
                        UserRoleService userRoleService,
                        RolesService rolesService) {
        this.usersRepository = usersRepository;
        this.usersMapper = usersMapper;
        this.userRoleService = userRoleService;
        this.rolesService = rolesService;
    }

    /**
     * Save a users.
     *
     * @param usersDTO the entity to save.
     * @return the persisted entity.
     */
    public UsersDTO save(UsersDTO usersDTO) {
        log.debug("Request to save Users : {}", usersDTO);
        Users users = usersMapper.toEntity(usersDTO);
        users = usersRepository.save(users);
        return usersMapper.toDto(users);
    }

    public UsersDTO saveClient(UsersClientDTO usersDTO) {
        log.debug("Request to save Users : {}", usersDTO);
        Users users = usersDTO.users(usersDTO);
//        Users users = usersMapper.toEntity(usersDTO);
        users = usersRepository.save(users);
        return usersMapper.toDto(users);
    }

    /**
     * Get all the users.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<UsersDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Users");
        return usersRepository.findAll(pageable)
            .map(usersMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<Users> findByPhone(String phone) {
        log.debug("Request to get all Roles");
        return usersRepository.findAllByPhone(phone);
    }


    /**
     * Get one users by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<UsersDTO> findOne(Long id) {
        log.debug("Request to get Users : {}", id);
        return usersRepository.findById(id)
            .map(usersMapper::toDto);
    }

    /**
     * Delete the users by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Users : {}", id);
        usersRepository.deleteById(id);
    }

    public Users update(UsersDTO obj) {
        try {
            int length = obj.getListRole().size();
            CustomUserDetails principal = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (obj.getPass() == null) {
                obj.setPass(principal.getUser().getPass());
                obj.setCreator(principal.getUser().getCreator());
                obj.setCreationTime(principal.getUser().getCreationTime());
                obj.setResetDate(principal.getUser().getResetDate());
                obj.setResetKey(principal.getUser().getResetKey());
            } else {
                if (obj.getPass().equals(obj.getRePassword())) {
                    obj.setPass(new BCryptPasswordEncoder().encode(obj.getPass()));
                    obj.setCreator(principal.getUser().getCreator());
                    obj.setCreationTime(principal.getUser().getCreationTime());
                    obj.setResetDate(principal.getUser().getResetDate());
                    obj.setResetKey(principal.getUser().getResetKey());
                } else {
                    throw new IllegalArgumentException("Mat khau nhap lai khong dung");
                }
            }
            Users users = usersMapper.toEntity(obj);
            usersRepository.save(users);
            userRoleService.deleteAllByUsersId(users.getId());
            if (length > 0) {
                for (int i = 0; i < length; i++) {
                    UserRoleDTO userRoleDTO = new UserRoleDTO();
                    userRoleDTO.setUsersId(users.getId());
                    // kiem tra xem ds role co ton tai trong db hay chua ?
                    if (!rolesService.findOne(obj.getListRole().get(i)).isPresent()){
                        throw new IllegalArgumentException("Vui lòng kiểm tra lại danh sách quyền");
                    }
                    userRoleDTO.setRolesId(obj.getListRole().get(i));
                    userRoleDTO.setUpdateTime(DateUtil.getDateC());
                    userRoleService.save(userRoleDTO);
                }
            }
            users.setPass(null);
            return users;
//            List<UserRole> userRoleList = userRoleService.findAllById(obj.getId());
//            List<Long> list = new ArrayList<>();
//            for (UserRole role : userRoleList) {
//                list.add(role.getRoles().getId());
//            }
//            List<Long> base;
//            // ds them
//            base = new ArrayList<>( obj.getListRole());
//            base.removeAll(list);
//            for (Long aLong : base) {
//                UserRole userRole = new UserRole();
//                userRole.setIdUser(obj.getId());
//                userRole.setIdRole(aLong);
//                userRole.setUpdateTime(new Timestamp(System.currentTimeMillis()));
//                userRoleService.insert(userRole);
//            }
//            // ds xoa
//            base = new ArrayList<>(list);
//            base.removeAll(obj.getListRole());
//            for (Long aLong : base) {
//                String[] cars = {"idUser", "idRole"};
//                Object obj1[] = {obj.getId(), aLong};
//                List<UserRole> userRoleLis1 = userRoleService.find(cars, obj1);
//                for (UserRole userRole : userRoleLis1) {
//                    userRoleService.delete(userRole);
//                }
//            }
//            return obj.getId();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ServiceException(e.getMessage());
        }
    }

}
