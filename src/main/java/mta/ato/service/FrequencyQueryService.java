package mta.ato.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.JoinType;

import mta.ato.service.dto.ObjectsDTO;
import mta.ato.utils.DataUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mta.ato.domain.Frequency;
import mta.ato.domain.*; // for static metamodels
import mta.ato.repository.FrequencyRepository;
import mta.ato.service.dto.FrequencyCriteria;
import mta.ato.service.dto.FrequencyDTO;
import mta.ato.service.mapper.FrequencyMapper;

/**
 * Service for executing complex queries for {@link Frequency} entities in the database.
 * The main input is a {@link FrequencyCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link FrequencyDTO} or a {@link Page} of {@link FrequencyDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FrequencyQueryService extends QueryService<Frequency> {

    private final Logger log = LoggerFactory.getLogger(FrequencyQueryService.class);

    private final FrequencyRepository frequencyRepository;

    private final FrequencyMapper frequencyMapper;

    public FrequencyQueryService(FrequencyRepository frequencyRepository, FrequencyMapper frequencyMapper) {
        this.frequencyRepository = frequencyRepository;
        this.frequencyMapper = frequencyMapper;
    }

    /**
     * Return a {@link List} of {@link FrequencyDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<FrequencyDTO> findByCriteria(FrequencyCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Frequency> specification = createSpecification(criteria);
        return frequencyMapper.toDto(frequencyRepository.findAll(specification));
    }

    @Transactional(readOnly = true)
    public List<FrequencyDTO> findByAllSymptom(Symptom symptom) {
        log.debug("find by criteria : {}", symptom);
        return frequencyMapper.toDto(frequencyRepository.findAllBySymptom(symptom));
    }




    /**
     * Return a {@link Page} of {@link FrequencyDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<FrequencyDTO> findByCriteria(FrequencyCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Frequency> specification = createSpecification(criteria);
        return frequencyRepository.findAll(specification, page)
            .map(frequencyMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FrequencyCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Frequency> specification = createSpecification(criteria);
        return frequencyRepository.count(specification);
    }

    /**
     * Function to convert {@link FrequencyCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Frequency> createSpecification(FrequencyCriteria criteria) {
        Specification<Frequency> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Frequency_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Frequency_.name));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatus(), Frequency_.status));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getType(), Frequency_.type));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), Frequency_.updateTime));
            }
            if (criteria.getSymptomId() != null) {
                specification = specification.and(buildSpecification(criteria.getSymptomId(),
                    root -> root.join(Frequency_.symptom, JoinType.LEFT).get(Symptom_.id)));
            }
        }
        return specification;
    }
}
