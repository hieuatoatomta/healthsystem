package mta.ato.service;

import mta.ato.domain.TypeDisease;
import mta.ato.repository.TypeDiseaseRepository;
import mta.ato.service.dto.TypeDiseaseDTO;
import mta.ato.service.mapper.TypeDiseaseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TypeDisease}.
 */
@Service
@Transactional
public class TypeDiseaseService {

    private final Logger log = LoggerFactory.getLogger(TypeDiseaseService.class);

    private final TypeDiseaseRepository typeDiseaseRepository;

    private final TypeDiseaseMapper typeDiseaseMapper;

    public TypeDiseaseService(TypeDiseaseRepository typeDiseaseRepository, TypeDiseaseMapper typeDiseaseMapper) {
        this.typeDiseaseRepository = typeDiseaseRepository;
        this.typeDiseaseMapper = typeDiseaseMapper;
    }

    /**
     * Save a typeDisease.
     *
     * @param typeDiseaseDTO the entity to save.
     * @return the persisted entity.
     */
    public TypeDiseaseDTO save(TypeDiseaseDTO typeDiseaseDTO) {
        log.debug("Request to save TypeDisease : {}", typeDiseaseDTO);
        TypeDisease typeDisease = typeDiseaseMapper.toEntity(typeDiseaseDTO);
        typeDisease = typeDiseaseRepository.save(typeDisease);
        return typeDiseaseMapper.toDto(typeDisease);
    }

    /**
     * Get all the typeDiseases.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TypeDiseaseDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TypeDiseases");
        return typeDiseaseRepository.findAll(pageable)
            .map(typeDiseaseMapper::toDto);
    }


    /**
     * Get one typeDisease by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TypeDiseaseDTO> findOne(Long id) {
        log.debug("Request to get TypeDisease : {}", id);
        return typeDiseaseRepository.findById(id)
            .map(typeDiseaseMapper::toDto);
    }

    /**
     * Delete the typeDisease by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TypeDisease : {}", id);
        typeDiseaseRepository.deleteById(id);
    }


    @Transactional(readOnly = true)
    public TypeDisease findByCode(String code) {
        log.debug("Request to get all Roles");
        return typeDiseaseRepository.findByCode(code);
    }
}
