package mta.ato.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mta.ato.domain.Symptom;
import mta.ato.domain.*; // for static metamodels
import mta.ato.repository.SymptomRepository;
import mta.ato.service.dto.SymptomCriteria;
import mta.ato.service.dto.SymptomDTO;
import mta.ato.service.mapper.SymptomMapper;

/**
 * Service for executing complex queries for {@link Symptom} entities in the database.
 * The main input is a {@link SymptomCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SymptomDTO} or a {@link Page} of {@link SymptomDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SymptomQueryService extends QueryService<Symptom> {

    private final Logger log = LoggerFactory.getLogger(SymptomQueryService.class);

    private final SymptomRepository symptomRepository;

    private final SymptomMapper symptomMapper;

    public SymptomQueryService(SymptomRepository symptomRepository, SymptomMapper symptomMapper) {
        this.symptomRepository = symptomRepository;
        this.symptomMapper = symptomMapper;
    }

    /**
     * Return a {@link List} of {@link SymptomDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SymptomDTO> findByCriteria(SymptomCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Symptom> specification = createSpecification(criteria);
        return symptomMapper.toDto(symptomRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SymptomDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Symptom> findByCriteria(SymptomCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Symptom> specification = createSpecification(criteria);
        return symptomRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SymptomCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Symptom> specification = createSpecification(criteria);
        return symptomRepository.count(specification);
    }

    /**
     * Function to convert {@link SymptomCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Symptom> createSpecification(SymptomCriteria criteria) {
        Specification<Symptom> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Symptom_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Symptom_.name));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatus(), Symptom_.status));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getType(), Symptom_.type));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), Symptom_.updateTime));
            }
            if (criteria.getDsFrequencyId() != null) {
                specification = specification.and(buildSpecification(criteria.getDsFrequencyId(),
                    root -> root.join(Symptom_.dsFrequencies, JoinType.LEFT).get(Frequency_.id)));
            }
            if (criteria.getTypediseaseId() != null) {
                specification = specification.and(buildSpecification(criteria.getTypediseaseId(),
                    root -> root.join(Symptom_.typedisease, JoinType.LEFT).get(TypeDisease_.id)));
            }
            if (criteria.getTestdiseaseId() != null) {
                specification = specification.and(buildSpecification(criteria.getTestdiseaseId(),
                    root -> root.join(Symptom_.testdisease, JoinType.LEFT).get(TestDisease_.id)));
            }
        }
        return specification;
    }
}
