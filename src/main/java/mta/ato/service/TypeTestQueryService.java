package mta.ato.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mta.ato.domain.TypeTest;
import mta.ato.domain.*; // for static metamodels
import mta.ato.repository.TypeTestRepository;
import mta.ato.service.dto.TypeTestCriteria;
import mta.ato.service.dto.TypeTestDTO;
import mta.ato.service.mapper.TypeTestMapper;

/**
 * Service for executing complex queries for {@link TypeTest} entities in the database.
 * The main input is a {@link TypeTestCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TypeTestDTO} or a {@link Page} of {@link TypeTestDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TypeTestQueryService extends QueryService<TypeTest> {

    private final Logger log = LoggerFactory.getLogger(TypeTestQueryService.class);

    private final TypeTestRepository typeTestRepository;

    private final TypeTestMapper typeTestMapper;

    public TypeTestQueryService(TypeTestRepository typeTestRepository, TypeTestMapper typeTestMapper) {
        this.typeTestRepository = typeTestRepository;
        this.typeTestMapper = typeTestMapper;
    }

    /**
     * Return a {@link List} of {@link TypeTestDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TypeTest> findByCriteria(TypeTestCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TypeTest> specification = createSpecification(criteria);
        return typeTestRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link TypeTestDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TypeTestDTO> findByCriteria(TypeTestCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TypeTest> specification = createSpecification(criteria);
        return typeTestRepository.findAll(specification, page)
            .map(typeTestMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TypeTestCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TypeTest> specification = createSpecification(criteria);
        return typeTestRepository.count(specification);
    }

    /**
     * Function to convert {@link TypeTestCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TypeTest> createSpecification(TypeTestCriteria criteria) {
        Specification<TypeTest> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TypeTest_.id));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), TypeTest_.updateTime));
            }
            if (criteria.getTypediseaseId() != null) {
                specification = specification.and(buildSpecification(criteria.getTypediseaseId(),
                    root -> root.join(TypeTest_.typedisease, JoinType.LEFT).get(TypeDisease_.id)));
            }
            if (criteria.getTestdiseaseId() != null) {
                specification = specification.and(buildSpecification(criteria.getTestdiseaseId(),
                    root -> root.join(TypeTest_.testdisease, JoinType.LEFT).get(TestDisease_.id)));
            }
        }
        return specification;
    }
}
