package mta.ato.service;

import mta.ato.domain.Objects;
import mta.ato.repository.ObjectsRepository;
import mta.ato.service.dto.ObjectsDTO;
import mta.ato.service.mapper.ObjectsMapper;
import mta.ato.utils.DataUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Objects}.
 */
@Service
@Transactional
public class ObjectsService {

    private final Logger log = LoggerFactory.getLogger(ObjectsService.class);

    private final ObjectsRepository objectsRepository;

    private final ObjectsMapper objectsMapper;

    public ObjectsService(ObjectsRepository objectsRepository, ObjectsMapper objectsMapper) {
        this.objectsRepository = objectsRepository;
        this.objectsMapper = objectsMapper;
    }

    /**
     * Save a objects.
     *
     * @param objectsDTO the entity to save.
     * @return the persisted entity.
     */
    public ObjectsDTO save(ObjectsDTO objectsDTO) {
        log.debug("Request to save Objects : {}", objectsDTO);
        Objects objects = objectsMapper.toEntity(objectsDTO);
        objects = objectsRepository.save(objects);
        return objectsMapper.toDto(objects);
    }

    /**
     * Get all the objects.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ObjectsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Objects");
        return objectsRepository.findAll(pageable)
            .map(objectsMapper::toDto);
    }


    /**
     * Get one objects by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ObjectsDTO> findOne(Long id) {
        log.debug("Request to get Objects : {}", id);
        return objectsRepository.findById(id)
            .map(objectsMapper::toDto);
    }

    /**
     * Delete the objects by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Objects : {}", id);
        objectsRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Objects findByCode(String code) {
        log.debug("Request to get all Roles");
        return objectsRepository.findByCode(code);
    }

    public List<ObjectsDTO> getAllObjRoleAction(Long id, Long type) {
        List<ObjectsDTO> lstObject = objectsRepository.getAllObjRoleAction(id, type)
            .stream().map(e -> {
                ObjectsDTO dto = new ObjectsDTO();
                dto.setId(DataUtil.safeToLong(e[0]));
                dto.setParentId(DataUtil.safeToLong(e[1]));
                dto.setName(DataUtil.safeToString(e[2]));
                dto.setCode(DataUtil.safeToString(e[3]));
                dto.setChecked(DataUtil.safeToLong(e[4]));
                return dto;
            }).collect(Collectors.toList());
        return lstObject;
    }
}
