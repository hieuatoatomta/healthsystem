package mta.ato.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mta.ato.domain.TypeDisease;
import mta.ato.domain.*; // for static metamodels
import mta.ato.repository.TypeDiseaseRepository;
import mta.ato.service.dto.TypeDiseaseCriteria;
import mta.ato.service.dto.TypeDiseaseDTO;
import mta.ato.service.mapper.TypeDiseaseMapper;

/**
 * Service for executing complex queries for {@link TypeDisease} entities in the database.
 * The main input is a {@link TypeDiseaseCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TypeDiseaseDTO} or a {@link Page} of {@link TypeDiseaseDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TypeDiseaseQueryService extends QueryService<TypeDisease> {

    private final Logger log = LoggerFactory.getLogger(TypeDiseaseQueryService.class);

    private final TypeDiseaseRepository typeDiseaseRepository;

    private final TypeDiseaseMapper typeDiseaseMapper;

    public TypeDiseaseQueryService(TypeDiseaseRepository typeDiseaseRepository, TypeDiseaseMapper typeDiseaseMapper) {
        this.typeDiseaseRepository = typeDiseaseRepository;
        this.typeDiseaseMapper = typeDiseaseMapper;
    }

    /**
     * Return a {@link List} of {@link TypeDiseaseDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TypeDiseaseDTO> findByCriteria(TypeDiseaseCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TypeDisease> specification = createSpecification(criteria);
        return typeDiseaseMapper.toDto(typeDiseaseRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TypeDiseaseDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TypeDisease> findByCriteria(TypeDiseaseCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TypeDisease> specification = createSpecification(criteria);
        return typeDiseaseRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TypeDiseaseCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TypeDisease> specification = createSpecification(criteria);
        return typeDiseaseRepository.count(specification);
    }

    /**
     * Function to convert {@link TypeDiseaseCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TypeDisease> createSpecification(TypeDiseaseCriteria criteria) {
        Specification<TypeDisease> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TypeDisease_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), TypeDisease_.code));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), TypeDisease_.name));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatus(), TypeDisease_.status));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), TypeDisease_.updateTime));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), TypeDisease_.description));
            }
            if (criteria.getDsTypeTestId() != null) {
                specification = specification.and(buildSpecification(criteria.getDsTypeTestId(),
                    root -> root.join(TypeDisease_.dsTypeTests, JoinType.LEFT).get(TypeTest_.id)));
            }
            if (criteria.getDsSymptomId() != null) {
                specification = specification.and(buildSpecification(criteria.getDsSymptomId(),
                    root -> root.join(TypeDisease_.dsSymptoms, JoinType.LEFT).get(Symptom_.id)));
            }
            if (criteria.getDsStatusDiseaseId() != null) {
                specification = specification.and(buildSpecification(criteria.getDsStatusDiseaseId(),
                    root -> root.join(TypeDisease_.dsStatusDiseases, JoinType.LEFT).get(StatusDisease_.id)));
            }
        }
        return specification;
    }
}
