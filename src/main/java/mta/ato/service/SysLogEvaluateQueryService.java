package mta.ato.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mta.ato.domain.SysLogEvaluate;
import mta.ato.domain.*; // for static metamodels
import mta.ato.repository.SysLogEvaluateRepository;
import mta.ato.service.dto.SysLogEvaluateCriteria;
import mta.ato.service.dto.SysLogEvaluateDTO;
import mta.ato.service.mapper.SysLogEvaluateMapper;

/**
 * Service for executing complex queries for {@link SysLogEvaluate} entities in the database.
 * The main input is a {@link SysLogEvaluateCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SysLogEvaluateDTO} or a {@link Page} of {@link SysLogEvaluateDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SysLogEvaluateQueryService extends QueryService<SysLogEvaluate> {

    private final Logger log = LoggerFactory.getLogger(SysLogEvaluateQueryService.class);

    private final SysLogEvaluateRepository sysLogEvaluateRepository;

    private final SysLogEvaluateMapper sysLogEvaluateMapper;

    public SysLogEvaluateQueryService(SysLogEvaluateRepository sysLogEvaluateRepository, SysLogEvaluateMapper sysLogEvaluateMapper) {
        this.sysLogEvaluateRepository = sysLogEvaluateRepository;
        this.sysLogEvaluateMapper = sysLogEvaluateMapper;
    }

    /**
     * Return a {@link List} of {@link SysLogEvaluateDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SysLogEvaluateDTO> findByCriteria(SysLogEvaluateCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<SysLogEvaluate> specification = createSpecification(criteria);
        return sysLogEvaluateMapper.toDto(sysLogEvaluateRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SysLogEvaluateDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SysLogEvaluateDTO> findByCriteria(SysLogEvaluateCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<SysLogEvaluate> specification = createSpecification(criteria);
        return sysLogEvaluateRepository.findAll(specification, page)
            .map(sysLogEvaluateMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SysLogEvaluateCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<SysLogEvaluate> specification = createSpecification(criteria);
        return sysLogEvaluateRepository.count(specification);
    }

    /**
     * Function to convert {@link SysLogEvaluateCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<SysLogEvaluate> createSpecification(SysLogEvaluateCriteria criteria) {
        Specification<SysLogEvaluate> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), SysLogEvaluate_.id));
            }
            if (criteria.getFullName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFullName(), SysLogEvaluate_.fullName));
            }
            if (criteria.getMail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMail(), SysLogEvaluate_.mail));
            }
            if (criteria.getPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhone(), SysLogEvaluate_.phone));
            }
            if (criteria.getNameType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNameType(), SysLogEvaluate_.nameType));
            }
            if (criteria.getNameEvaluate() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNameEvaluate(), SysLogEvaluate_.nameEvaluate));
            }
            if (criteria.getContent() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContent(), SysLogEvaluate_.content));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), SysLogEvaluate_.updateTime));
            }
        }
        return specification;
    }
}
