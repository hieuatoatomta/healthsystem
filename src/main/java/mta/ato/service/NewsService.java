package mta.ato.service;

import mta.ato.domain.Category;
import mta.ato.domain.News;
import mta.ato.repository.NewsRepository;
import mta.ato.service.dto.NewsDTO;
import mta.ato.service.mapper.NewsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link News}.
 */
@Service
@Transactional
public class NewsService {

    private final Logger log = LoggerFactory.getLogger(NewsService.class);

    private final NewsRepository newsRepository;

    private final NewsMapper newsMapper;

    public NewsService(NewsRepository newsRepository, NewsMapper newsMapper) {
        this.newsRepository = newsRepository;
        this.newsMapper = newsMapper;
    }

    /**
     * Save a news.
     *
     * @param newsDTO the entity to save.
     * @return the persisted entity.
     */
    public NewsDTO save(NewsDTO newsDTO) {
        log.debug("Request to save News : {}", newsDTO);
        News news = newsMapper.toEntity(newsDTO);
        news = newsRepository.save(news);
        return newsMapper.toDto(news);
    }

    /**
     * Get all the news.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all News");
        return newsRepository.findAll(pageable)
            .map(newsMapper::toDto);
    }


    /**
     * Get one news by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<NewsDTO> findOne(Long id) {
        log.debug("Request to get News : {}", id);
        return newsRepository.findById(id)
            .map(newsMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Optional<News> findOne1(Long id) {
        log.debug("Request to get News : {}", id);
        return newsRepository.findById(id);
    }


    @Transactional(readOnly = true)
    public Optional<News> findOneEntity(Long id) {
        log.debug("Request to get News : {}", id);
        return newsRepository.findById(id);
    }
    @Transactional(readOnly = true)
    public List<News> findAllByCategory(Category id, Pageable pageable) {
        log.debug("Request to get News : {}", id);
        return newsRepository.findAllByCategoryAndStatus(id, 0L, pageable);
    }
    @Transactional(readOnly = true)
    public List<News> findAllByCategoryAndHighlights(Category id, Pageable pageable) {
        log.debug("Request to get News : {}", id);
        return newsRepository.findAllByStatusAndHighlights(0L, 0L, pageable);
    }

    @Transactional(readOnly = true)
    public List<News> findAllByStatus(Pageable pageable) {
        return newsRepository.findAllByStatus(0L, pageable);
    }

    /**
     * Delete the news by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete News : {}", id);
        newsRepository.deleteById(id);
    }
}
