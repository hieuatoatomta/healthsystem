package mta.ato.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the {@link mta.ato.domain.TypeTest} entity.
 */
public class TypeTestDTO implements Serializable {

    private Long id;

    private ZonedDateTime updateTime;

    private List<Long> listUncheck;
    private List<Long> listAdd;

    private Long typediseaseId;

    private String nameTypeDisease;

    private Long testdiseaseId;

    public List<Long> getListUncheck() {
        return listUncheck;
    }

    public String getNameTypeDisease() {
        return nameTypeDisease;
    }

    public void setNameTypeDisease(String nameTypeDisease) {
        this.nameTypeDisease = nameTypeDisease;
    }

    public void setListUncheck(List<Long> listUncheck) {
        this.listUncheck = listUncheck;
    }

    public List<Long> getListAdd() {
        return listAdd;
    }

    public void setListAdd(List<Long> listAdd) {
        this.listAdd = listAdd;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Long getTypediseaseId() {
        return typediseaseId;
    }

    public void setTypediseaseId(Long typeDiseaseId) {
        this.typediseaseId = typeDiseaseId;
    }

    public Long getTestdiseaseId() {
        return testdiseaseId;
    }

    public void setTestdiseaseId(Long testDiseaseId) {
        this.testdiseaseId = testDiseaseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TypeTestDTO)) {
            return false;
        }

        return id != null && id.equals(((TypeTestDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TypeTestDTO{" +
            "id=" + getId() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", typediseaseId=" + getTypediseaseId() +
            ", testdiseaseId=" + getTestdiseaseId() +
            "}";
    }
}
