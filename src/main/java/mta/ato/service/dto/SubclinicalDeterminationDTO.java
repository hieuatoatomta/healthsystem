package mta.ato.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the {@link mta.ato.domain.SubclinicalDetermination} entity.
 */
public class SubclinicalDeterminationDTO implements Serializable {

    private Long id;

    private Long amount;

    private Long idXn;

    private List<Long> listIdXn;
    private List<Long> listIdTt;

    private String lsIdXn;
    private String lsIdTt;

    private Long type;
    private Long idCheck;

    private Long idTT;
    private ZonedDateTime updateTime;

    private Long statusdiseaseId;

    private Long typediseaseId;

    private String nameXn;
    private String nameTT;

    private Long totalMax;

    public List<Long> getListIdTt() {
        return listIdTt;
    }

    public void setListIdTt(List<Long> listIdTt) {
        this.listIdTt = listIdTt;
    }

    public Long getIdCheck() {
        return idCheck;
    }

    public void setIdCheck(Long idCheck) {
        this.idCheck = idCheck;
    }

    public Long getIdTT() {
        return idTT;
    }

    public void setIdTT(Long idTT) {
        this.idTT = idTT;
    }

    public String getLsIdXn() {
        return lsIdXn;
    }

    public String getLsIdTt() {
        return lsIdTt;
    }

    public void setLsIdTt(String lsIdTt) {
        this.lsIdTt = lsIdTt;
    }

    public void setLsIdXn(String lsIdXn) {
        this.lsIdXn = lsIdXn;
    }

    public String getNameTT() {
        return nameTT;
    }

    public void setNameTT(String nameTT) {
        this.nameTT = nameTT;
    }

    public Long getTypediseaseId() {
        return typediseaseId;
    }

    public void setTypediseaseId(Long typediseaseId) {
        this.typediseaseId = typediseaseId;
    }

    public Long getTotalMax() {
        return totalMax;
    }

    public void setTotalMax(Long totalMax) {
        this.totalMax = totalMax;
    }

    public List<Long> getListIdXn() {
        return listIdXn;
    }

    public void setListIdXn(List<Long> listIdXn) {
        this.listIdXn = listIdXn;
    }

    public String getNameXn() {
        return nameXn;
    }

    public void setNameXn(String nameXn) {
        this.nameXn = nameXn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getIdXn() {
        return idXn;
    }

    public void setIdXn(Long idXn) {
        this.idXn = idXn;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Long getStatusdiseaseId() {
        return statusdiseaseId;
    }

    public void setStatusdiseaseId(Long statusDiseaseId) {
        this.statusdiseaseId = statusDiseaseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SubclinicalDeterminationDTO)) {
            return false;
        }

        return id != null && id.equals(((SubclinicalDeterminationDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SubclinicalDeterminationDTO{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", idXn=" + getIdXn() +
            ", type=" + getType() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", statusdiseaseId=" + getStatusdiseaseId() +
            "}";
    }
}
