package mta.ato.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

/**
 * Criteria class for the {@link mta.ato.domain.ExpertSystem} entity. This class is used
 * in {@link mta.ato.web.rest.ExpertSystemResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /expert-systems?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ExpertSystemCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter code;

    private StringFilter name;

    private LongFilter status;

    private LongFilter parentId;

    private LongFilter type;

    private ZonedDateTimeFilter updateTime;

    private StringFilter isLink;

    public ExpertSystemCriteria() {
    }

    public ExpertSystemCriteria(ExpertSystemCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.parentId = other.parentId == null ? null : other.parentId.copy();
        this.type = other.type == null ? null : other.type.copy();
        this.updateTime = other.updateTime == null ? null : other.updateTime.copy();
        this.isLink = other.isLink == null ? null : other.isLink.copy();
    }

    @Override
    public ExpertSystemCriteria copy() {
        return new ExpertSystemCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getStatus() {
        return status;
    }

    public void setStatus(LongFilter status) {
        this.status = status;
    }

    public LongFilter getParentId() {
        return parentId;
    }

    public void setParentId(LongFilter parentId) {
        this.parentId = parentId;
    }

    public LongFilter getType() {
        return type;
    }

    public void setType(LongFilter type) {
        this.type = type;
    }

    public ZonedDateTimeFilter getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTimeFilter updateTime) {
        this.updateTime = updateTime;
    }

    public StringFilter getIsLink() {
        return isLink;
    }

    public void setIsLink(StringFilter isLink) {
        this.isLink = isLink;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ExpertSystemCriteria that = (ExpertSystemCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(code, that.code) &&
            Objects.equals(name, that.name) &&
            Objects.equals(status, that.status) &&
            Objects.equals(parentId, that.parentId) &&
            Objects.equals(type, that.type) &&
            Objects.equals(updateTime, that.updateTime) &&
            Objects.equals(isLink, that.isLink);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        code,
        name,
        status,
        parentId,
        type,
        updateTime,
        isLink
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExpertSystemCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (parentId != null ? "parentId=" + parentId + ", " : "") +
                (type != null ? "type=" + type + ", " : "") +
                (updateTime != null ? "updateTime=" + updateTime + ", " : "") +
                (isLink != null ? "isLink=" + isLink + ", " : "") +
            "}";
    }

}
