package mta.ato.service.dto;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the {@link mta.ato.domain.TestDisease} entity.
 */
public class TestDiseaseDTO implements Serializable {

    private Long id;

    @Size(max = 100)
    private String code;

    @Size(max = 100)
    private String name;

    private Long status;

    private ZonedDateTime updateTime;

    @Size(max = 1000)
    private String description;

    private List<SymptomDTO> symptomDTOList;


    public List<SymptomDTO> getSymptomDTOList() {
        return symptomDTOList;
    }

    public void setSymptomDTOList(List<SymptomDTO> symptomDTOList) {
        this.symptomDTOList = symptomDTOList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TestDiseaseDTO)) {
            return false;
        }

        return id != null && id.equals(((TestDiseaseDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TestDiseaseDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", name='" + getName() + "'" +
            ", status=" + getStatus() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
