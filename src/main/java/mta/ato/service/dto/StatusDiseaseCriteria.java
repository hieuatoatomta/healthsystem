package mta.ato.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

/**
 * Criteria class for the {@link mta.ato.domain.StatusDisease} entity. This class is used
 * in {@link mta.ato.web.rest.StatusDiseaseResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /status-diseases?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class StatusDiseaseCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter determined;

    private LongFilter status;

    private LongFilter type;

    private ZonedDateTimeFilter updateTime;

    private LongFilter isEx;

    private LongFilter likStatus;

    private StringFilter description;

    private LongFilter dsSubclinicalDeterminationId;

    private LongFilter typediseaseId;

    public StatusDiseaseCriteria() {
    }

    public StatusDiseaseCriteria(StatusDiseaseCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.determined = other.determined == null ? null : other.determined.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.type = other.type == null ? null : other.type.copy();
        this.updateTime = other.updateTime == null ? null : other.updateTime.copy();
        this.isEx = other.isEx == null ? null : other.isEx.copy();
        this.likStatus = other.likStatus == null ? null : other.likStatus.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.dsSubclinicalDeterminationId = other.dsSubclinicalDeterminationId == null ? null : other.dsSubclinicalDeterminationId.copy();
        this.typediseaseId = other.typediseaseId == null ? null : other.typediseaseId.copy();
    }

    @Override
    public StatusDiseaseCriteria copy() {
        return new StatusDiseaseCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getDetermined() {
        return determined;
    }

    public void setDetermined(StringFilter determined) {
        this.determined = determined;
    }

    public LongFilter getStatus() {
        return status;
    }

    public void setStatus(LongFilter status) {
        this.status = status;
    }

    public LongFilter getType() {
        return type;
    }

    public void setType(LongFilter type) {
        this.type = type;
    }

    public ZonedDateTimeFilter getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTimeFilter updateTime) {
        this.updateTime = updateTime;
    }

    public LongFilter getIsEx() {
        return isEx;
    }

    public void setIsEx(LongFilter isEx) {
        this.isEx = isEx;
    }

    public LongFilter getLikStatus() {
        return likStatus;
    }

    public void setLikStatus(LongFilter likStatus) {
        this.likStatus = likStatus;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public LongFilter getDsSubclinicalDeterminationId() {
        return dsSubclinicalDeterminationId;
    }

    public void setDsSubclinicalDeterminationId(LongFilter dsSubclinicalDeterminationId) {
        this.dsSubclinicalDeterminationId = dsSubclinicalDeterminationId;
    }

    public LongFilter getTypediseaseId() {
        return typediseaseId;
    }

    public void setTypediseaseId(LongFilter typediseaseId) {
        this.typediseaseId = typediseaseId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final StatusDiseaseCriteria that = (StatusDiseaseCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(determined, that.determined) &&
            Objects.equals(status, that.status) &&
            Objects.equals(type, that.type) &&
            Objects.equals(updateTime, that.updateTime) &&
            Objects.equals(isEx, that.isEx) &&
            Objects.equals(likStatus, that.likStatus) &&
            Objects.equals(description, that.description) &&
            Objects.equals(dsSubclinicalDeterminationId, that.dsSubclinicalDeterminationId) &&
            Objects.equals(typediseaseId, that.typediseaseId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        determined,
        status,
        type,
        updateTime,
        isEx,
        likStatus,
        description,
        dsSubclinicalDeterminationId,
        typediseaseId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "StatusDiseaseCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (determined != null ? "determined=" + determined + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (type != null ? "type=" + type + ", " : "") +
                (updateTime != null ? "updateTime=" + updateTime + ", " : "") +
                (isEx != null ? "isEx=" + isEx + ", " : "") +
                (likStatus != null ? "likStatus=" + likStatus + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (dsSubclinicalDeterminationId != null ? "dsSubclinicalDeterminationId=" + dsSubclinicalDeterminationId + ", " : "") +
                (typediseaseId != null ? "typediseaseId=" + typediseaseId + ", " : "") +
            "}";
    }

}
