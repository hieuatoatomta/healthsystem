package mta.ato.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;

/**
 * A DTO for the {@link mta.ato.domain.Frequency} entity.
 */
public class FrequencyDTO implements Serializable {

    private Long id;

    private String name;

    private Long status;

    private Long type;

    private ZonedDateTime updateTime;

    private Long maxCd;

    public Long getMaxCd() {
        return maxCd;
    }

    public void setMaxCd(Long maxCd) {
        this.maxCd = maxCd;
    }

    private Long symptomId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Long getSymptomId() {
        return symptomId;
    }

    public void setSymptomId(Long symptomId) {
        this.symptomId = symptomId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FrequencyDTO)) {
            return false;
        }

        return id != null && id.equals(((FrequencyDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FrequencyDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", status=" + getStatus() +
            ", type=" + getType() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", symptomId=" + getSymptomId() +
            "}";
    }
}
