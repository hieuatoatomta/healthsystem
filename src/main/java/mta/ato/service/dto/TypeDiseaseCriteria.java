package mta.ato.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

/**
 * Criteria class for the {@link mta.ato.domain.TypeDisease} entity. This class is used
 * in {@link mta.ato.web.rest.TypeDiseaseResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /type-diseases?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TypeDiseaseCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter code;

    private StringFilter name;

    private LongFilter status;

    private ZonedDateTimeFilter updateTime;

    private StringFilter description;

    private LongFilter dsTypeTestId;

    private LongFilter dsSymptomId;

    private LongFilter dsStatusDiseaseId;

    public TypeDiseaseCriteria() {
    }

    public TypeDiseaseCriteria(TypeDiseaseCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.updateTime = other.updateTime == null ? null : other.updateTime.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.dsTypeTestId = other.dsTypeTestId == null ? null : other.dsTypeTestId.copy();
        this.dsSymptomId = other.dsSymptomId == null ? null : other.dsSymptomId.copy();
        this.dsStatusDiseaseId = other.dsStatusDiseaseId == null ? null : other.dsStatusDiseaseId.copy();
    }

    @Override
    public TypeDiseaseCriteria copy() {
        return new TypeDiseaseCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getStatus() {
        return status;
    }

    public void setStatus(LongFilter status) {
        this.status = status;
    }

    public ZonedDateTimeFilter getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTimeFilter updateTime) {
        this.updateTime = updateTime;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public LongFilter getDsTypeTestId() {
        return dsTypeTestId;
    }

    public void setDsTypeTestId(LongFilter dsTypeTestId) {
        this.dsTypeTestId = dsTypeTestId;
    }

    public LongFilter getDsSymptomId() {
        return dsSymptomId;
    }

    public void setDsSymptomId(LongFilter dsSymptomId) {
        this.dsSymptomId = dsSymptomId;
    }

    public LongFilter getDsStatusDiseaseId() {
        return dsStatusDiseaseId;
    }

    public void setDsStatusDiseaseId(LongFilter dsStatusDiseaseId) {
        this.dsStatusDiseaseId = dsStatusDiseaseId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TypeDiseaseCriteria that = (TypeDiseaseCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(code, that.code) &&
            Objects.equals(name, that.name) &&
            Objects.equals(status, that.status) &&
            Objects.equals(updateTime, that.updateTime) &&
            Objects.equals(description, that.description) &&
            Objects.equals(dsTypeTestId, that.dsTypeTestId) &&
            Objects.equals(dsSymptomId, that.dsSymptomId) &&
            Objects.equals(dsStatusDiseaseId, that.dsStatusDiseaseId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        code,
        name,
        status,
        updateTime,
        description,
        dsTypeTestId,
        dsSymptomId,
        dsStatusDiseaseId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TypeDiseaseCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (updateTime != null ? "updateTime=" + updateTime + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (dsTypeTestId != null ? "dsTypeTestId=" + dsTypeTestId + ", " : "") +
                (dsSymptomId != null ? "dsSymptomId=" + dsSymptomId + ", " : "") +
                (dsStatusDiseaseId != null ? "dsStatusDiseaseId=" + dsStatusDiseaseId + ", " : "") +
            "}";
    }

}
