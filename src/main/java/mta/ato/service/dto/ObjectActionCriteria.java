package mta.ato.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

/**
 * Criteria class for the {@link mta.ato.domain.ObjectAction} entity. This class is used
 * in {@link mta.ato.web.rest.ObjectActionResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /object-actions?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ObjectActionCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private ZonedDateTimeFilter updateTime;

    private LongFilter objectsId;

    private LongFilter actionsId;

    public ObjectActionCriteria() {
    }

    public ObjectActionCriteria(ObjectActionCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.updateTime = other.updateTime == null ? null : other.updateTime.copy();
        this.objectsId = other.objectsId == null ? null : other.objectsId.copy();
        this.actionsId = other.actionsId == null ? null : other.actionsId.copy();
    }

    @Override
    public ObjectActionCriteria copy() {
        return new ObjectActionCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public ZonedDateTimeFilter getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTimeFilter updateTime) {
        this.updateTime = updateTime;
    }

    public LongFilter getObjectsId() {
        return objectsId;
    }

    public void setObjectsId(LongFilter objectsId) {
        this.objectsId = objectsId;
    }

    public LongFilter getActionsId() {
        return actionsId;
    }

    public void setActionsId(LongFilter actionsId) {
        this.actionsId = actionsId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ObjectActionCriteria that = (ObjectActionCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(updateTime, that.updateTime) &&
            Objects.equals(objectsId, that.objectsId) &&
            Objects.equals(actionsId, that.actionsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        updateTime,
        objectsId,
        actionsId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ObjectActionCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (updateTime != null ? "updateTime=" + updateTime + ", " : "") +
                (objectsId != null ? "objectsId=" + objectsId + ", " : "") +
                (actionsId != null ? "actionsId=" + actionsId + ", " : "") +
            "}";
    }

}
