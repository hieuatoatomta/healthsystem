package mta.ato.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

/**
 * Criteria class for the {@link mta.ato.domain.News} entity. This class is used
 * in {@link mta.ato.web.rest.NewsResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /news?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NewsCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter code;

    private StringFilter title;

    private StringFilter bodyChild;

    private StringFilter bodyNews;

    private StringFilter description;

    private StringFilter reasonForRefusal;

    private LongFilter status;

    private LongFilter numberOfViewer;

    private ZonedDateTimeFilter updateTime;

    private LongFilter highlights;

    private StringFilter imageLink;

    private LongFilter dsImageLinkId;

    private LongFilter categoryId;

    private LongFilter usersId;

    public NewsCriteria() {
    }

    public NewsCriteria(NewsCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.title = other.title == null ? null : other.title.copy();
        this.bodyChild = other.bodyChild == null ? null : other.bodyChild.copy();
        this.bodyNews = other.bodyNews == null ? null : other.bodyNews.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.reasonForRefusal = other.reasonForRefusal == null ? null : other.reasonForRefusal.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.numberOfViewer = other.numberOfViewer == null ? null : other.numberOfViewer.copy();
        this.updateTime = other.updateTime == null ? null : other.updateTime.copy();
        this.highlights = other.highlights == null ? null : other.highlights.copy();
        this.imageLink = other.imageLink == null ? null : other.imageLink.copy();
        this.dsImageLinkId = other.dsImageLinkId == null ? null : other.dsImageLinkId.copy();
        this.categoryId = other.categoryId == null ? null : other.categoryId.copy();
        this.usersId = other.usersId == null ? null : other.usersId.copy();
    }

    @Override
    public NewsCriteria copy() {
        return new NewsCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getTitle() {
        return title;
    }

    public void setTitle(StringFilter title) {
        this.title = title;
    }

    public StringFilter getBodyChild() {
        return bodyChild;
    }

    public void setBodyChild(StringFilter bodyChild) {
        this.bodyChild = bodyChild;
    }

    public StringFilter getBodyNews() {
        return bodyNews;
    }

    public void setBodyNews(StringFilter bodyNews) {
        this.bodyNews = bodyNews;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getReasonForRefusal() {
        return reasonForRefusal;
    }

    public void setReasonForRefusal(StringFilter reasonForRefusal) {
        this.reasonForRefusal = reasonForRefusal;
    }

    public LongFilter getStatus() {
        return status;
    }

    public void setStatus(LongFilter status) {
        this.status = status;
    }

    public LongFilter getNumberOfViewer() {
        return numberOfViewer;
    }

    public void setNumberOfViewer(LongFilter numberOfViewer) {
        this.numberOfViewer = numberOfViewer;
    }

    public ZonedDateTimeFilter getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTimeFilter updateTime) {
        this.updateTime = updateTime;
    }

    public LongFilter getHighlights() {
        return highlights;
    }

    public void setHighlights(LongFilter highlights) {
        this.highlights = highlights;
    }

    public StringFilter getImageLink() {
        return imageLink;
    }

    public void setImageLink(StringFilter imageLink) {
        this.imageLink = imageLink;
    }

    public LongFilter getDsImageLinkId() {
        return dsImageLinkId;
    }

    public void setDsImageLinkId(LongFilter dsImageLinkId) {
        this.dsImageLinkId = dsImageLinkId;
    }

    public LongFilter getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(LongFilter categoryId) {
        this.categoryId = categoryId;
    }

    public LongFilter getUsersId() {
        return usersId;
    }

    public void setUsersId(LongFilter usersId) {
        this.usersId = usersId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final NewsCriteria that = (NewsCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(code, that.code) &&
            Objects.equals(title, that.title) &&
            Objects.equals(bodyChild, that.bodyChild) &&
            Objects.equals(bodyNews, that.bodyNews) &&
            Objects.equals(description, that.description) &&
            Objects.equals(reasonForRefusal, that.reasonForRefusal) &&
            Objects.equals(status, that.status) &&
            Objects.equals(numberOfViewer, that.numberOfViewer) &&
            Objects.equals(updateTime, that.updateTime) &&
            Objects.equals(highlights, that.highlights) &&
            Objects.equals(imageLink, that.imageLink) &&
            Objects.equals(dsImageLinkId, that.dsImageLinkId) &&
            Objects.equals(categoryId, that.categoryId) &&
            Objects.equals(usersId, that.usersId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        code,
        title,
        bodyChild,
        bodyNews,
        description,
        reasonForRefusal,
        status,
        numberOfViewer,
        updateTime,
        highlights,
        imageLink,
        dsImageLinkId,
        categoryId,
        usersId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NewsCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (title != null ? "title=" + title + ", " : "") +
                (bodyChild != null ? "bodyChild=" + bodyChild + ", " : "") +
                (bodyNews != null ? "bodyNews=" + bodyNews + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (reasonForRefusal != null ? "reasonForRefusal=" + reasonForRefusal + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (numberOfViewer != null ? "numberOfViewer=" + numberOfViewer + ", " : "") +
                (updateTime != null ? "updateTime=" + updateTime + ", " : "") +
                (highlights != null ? "highlights=" + highlights + ", " : "") +
                (imageLink != null ? "imageLink=" + imageLink + ", " : "") +
                (dsImageLinkId != null ? "dsImageLinkId=" + dsImageLinkId + ", " : "") +
                (categoryId != null ? "categoryId=" + categoryId + ", " : "") +
                (usersId != null ? "usersId=" + usersId + ", " : "") +
            "}";
    }

}
