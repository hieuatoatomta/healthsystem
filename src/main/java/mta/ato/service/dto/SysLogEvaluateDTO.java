package mta.ato.service.dto;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link mta.ato.domain.SysLogEvaluate} entity.
 */
public class SysLogEvaluateDTO implements Serializable {

    private Long id;

    private String fullName;

    @Size(max = 100)
    private String mail;

    @Size(max = 10)
    private String phone;

    private String nameType;

    private String nameEvaluate;

    private String content;

    private ZonedDateTime updateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    public String getNameEvaluate() {
        return nameEvaluate;
    }

    public void setNameEvaluate(String nameEvaluate) {
        this.nameEvaluate = nameEvaluate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SysLogEvaluateDTO)) {
            return false;
        }

        return id != null && id.equals(((SysLogEvaluateDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SysLogEvaluateDTO{" +
            "id=" + getId() +
            ", fullName='" + getFullName() + "'" +
            ", mail='" + getMail() + "'" +
            ", phone='" + getPhone() + "'" +
            ", nameType='" + getNameType() + "'" +
            ", nameEvaluate='" + getNameEvaluate() + "'" +
            ", content='" + getContent() + "'" +
            ", updateTime='" + getUpdateTime() + "'" +
            "}";
    }
}
