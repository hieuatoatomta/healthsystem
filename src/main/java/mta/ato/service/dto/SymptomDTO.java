package mta.ato.service.dto;

import mta.ato.domain.Frequency;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the {@link mta.ato.domain.Symptom} entity.
 */
public class SymptomDTO implements Serializable {

    private Long id;

    @Size(max = 100)
    private String name;

    private Long status;

    private Long type;

    private ZonedDateTime updateTime;

    private String nameXn;

    private Long typediseaseId;

    private Long testdiseaseId;

    private List<Long> listIdXn;

    private List<Frequency> frequencyList;

    public List<Frequency> getFrequencyList() {
        return frequencyList;
    }

    public void setFrequencyList(List<Frequency> frequencyList) {
        this.frequencyList = frequencyList;
    }

    public List<Long> getListIdXn() {
        return listIdXn;
    }

    public void setListIdXn(List<Long> listIdXn) {
        this.listIdXn = listIdXn;
    }

    public String getNameXn() {
        return nameXn;
    }

    public void setNameXn(String nameXn) {
        this.nameXn = nameXn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Long getTypediseaseId() {
        return typediseaseId;
    }

    public void setTypediseaseId(Long typeDiseaseId) {
        this.typediseaseId = typeDiseaseId;
    }

    public Long getTestdiseaseId() {
        return testdiseaseId;
    }

    public void setTestdiseaseId(Long testDiseaseId) {
        this.testdiseaseId = testDiseaseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SymptomDTO)) {
            return false;
        }

        return id != null && id.equals(((SymptomDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SymptomDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", status=" + getStatus() +
            ", type=" + getType() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", typediseaseId=" + getTypediseaseId() +
            ", testdiseaseId=" + getTestdiseaseId() +
            "}";
    }
}
