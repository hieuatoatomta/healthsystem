package mta.ato.service.dto;

import mta.ato.utils.annotations.ValidFile;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Set;

import static mta.ato.config.Constants.CONTENT_TYPE.*;

/**
 * A DTO for the {@link mta.ato.domain.News} entity.
 */
public class NewsDTO implements Serializable {

    private JSONObject model;

    private Long id;

    @Size(max = 200)
    private String code;

    @Size(max = 300)
    private String title;

    @Size(max = 1000)
    private String bodyChild;

    @Size(max = 10000)
    private String bodyNews;

    @Size(max = 1000)
    private String description;

    @Size(max = 1000)
    private String reasonForRefusal;

    private Long status;

    private Long numberOfViewer;

    private ZonedDateTime updateTime;

    private Long highlights;

    @Size(max = 1000)
    private String imageLink;

    private Long categoryId;
    private String categoryName;
    private String usersName;
    public JSONObject getModel() {
        return model;
    }

    public void setModel(JSONObject model) {
        this.model = model;
    }

    public String getUsersName() {
        return usersName;
    }

    public void setUsersName(String usersName) {
        this.usersName = usersName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    private Long usersId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBodyChild() {
        return bodyChild;
    }

    public void setBodyChild(String bodyChild) {
        this.bodyChild = bodyChild;
    }

    public String getBodyNews() {
        return bodyNews;
    }

    public void setBodyNews(String bodyNews) {
        this.bodyNews = bodyNews;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReasonForRefusal() {
        return reasonForRefusal;
    }

    public void setReasonForRefusal(String reasonForRefusal) {
        this.reasonForRefusal = reasonForRefusal;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getNumberOfViewer() {
        return numberOfViewer;
    }

    public void setNumberOfViewer(Long numberOfViewer) {
        this.numberOfViewer = numberOfViewer;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Long getHighlights() {
        return highlights;
    }

    public void setHighlights(Long highlights) {
        this.highlights = highlights;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getUsersId() {
        return usersId;
    }

    public void setUsersId(Long usersId) {
        this.usersId = usersId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NewsDTO)) {
            return false;
        }

        return id != null && id.equals(((NewsDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NewsDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", title='" + getTitle() + "'" +
            ", bodyChild='" + getBodyChild() + "'" +
            ", bodyNews='" + getBodyNews() + "'" +
            ", description='" + getDescription() + "'" +
            ", reasonForRefusal='" + getReasonForRefusal() + "'" +
            ", status=" + getStatus() +
            ", numberOfViewer=" + getNumberOfViewer() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", highlights=" + getHighlights() +
            ", imageLink='" + getImageLink() + "'" +
            ", categoryId=" + getCategoryId() +
            ", usersId=" + getUsersId() +
            "}";
    }
}
