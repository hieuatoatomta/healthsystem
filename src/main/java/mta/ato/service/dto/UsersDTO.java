package mta.ato.service.dto;

import mta.ato.utils.annotations.Phone;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the {@link mta.ato.domain.Users} entity.
 */
public class UsersDTO implements Serializable {

    private Long id;

    @NotNull(message = "Tên tài khoản không được trống")
    @Size(max = 60)
    private String name;

    @Size(max = 60)
    private String fullName;

    @Size(max = 60)
    private String pass;

    @Size(max = 60)
    private String pathUrl;

    private String dateOfBirth;

    @NotNull(message = "Email không được trống")
    @Size(max = 100)
    private String mail;

    @Size(max = 10)
    @Phone(message = "Số điện thoại không đúng định dạng")
    private String phone;

    private Long status;

    @Size(max = 8)
    private String resetKey;

    private ZonedDateTime resetDate;

    @Size(max = 60)
    private String creator;

    private ZonedDateTime creationTime;

    private String rePassword;

    private List<Long> listRole;

    public List<Long> getListRole() {
        return listRole;
    }

    public void setListRole(List<Long> listRole) {
        this.listRole = listRole;
    }

    public String getRePassword() {
        return rePassword;
    }

    public void setRePassword(String rePassword) {
        this.rePassword = rePassword;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPathUrl() {
        return pathUrl;
    }

    public void setPathUrl(String pathUrl) {
        this.pathUrl = pathUrl;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getResetKey() {
        return resetKey;
    }

    public void setResetKey(String resetKey) {
        this.resetKey = resetKey;
    }

    public ZonedDateTime getResetDate() {
        return resetDate;
    }

    public void setResetDate(ZonedDateTime resetDate) {
        this.resetDate = resetDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public ZonedDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(ZonedDateTime creationTime) {
        this.creationTime = creationTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UsersDTO)) {
            return false;
        }

        return id != null && id.equals(((UsersDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UsersDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", fullName='" + getFullName() + "'" +
            ", pass='" + getPass() + "'" +
            ", pathUrl='" + getPathUrl() + "'" +
            ", dateOfBirth='" + getDateOfBirth() + "'" +
            ", mail='" + getMail() + "'" +
            ", phone='" + getPhone() + "'" +
            ", status=" + getStatus() +
            ", resetKey='" + getResetKey() + "'" +
            ", resetDate='" + getResetDate() + "'" +
            ", creator='" + getCreator() + "'" +
            ", creationTime='" + getCreationTime() + "'" +
            "}";
    }
}
