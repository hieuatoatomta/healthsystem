package mta.ato.service.dto;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the {@link mta.ato.domain.StatusDisease} entity.
 */
public class StatusDiseaseDTO implements Serializable {

    private Long id;

    @Size(max = 100)
    private String name;

    private String determined;

    private Long status;

    private Long type;
    private Long type1;
    private List<Long> lsGd;
    private Long value;

    private String NameType;

    private ZonedDateTime updateTime;

    private Long isEx;
    private String isExLink;

    private Long likStatus;

    @Size(max = 1000)
    private String description;

    private Long typediseaseId;

    private String nameXn;
    private String nameTT;
    private Long totalMax;

    public List<Long> getLsGd() {
        return lsGd;
    }

    public void setLsGd(List<Long> lsGd) {
        this.lsGd = lsGd;
    }

    public Long getType1() {
        return type1;
    }

    public void setType1(Long type1) {
        this.type1 = type1;
    }

    public String getIsExLink() {
        return isExLink;
    }

    public void setIsExLink(String isExLink) {
        this.isExLink = isExLink;
    }

    public Long getTotalMax() {
        return totalMax;
    }

    public void setTotalMax(Long totalMax) {
        this.totalMax = totalMax;
    }

    public String getNameXn() {
        return nameXn;
    }

    public void setNameXn(String nameXn) {
        this.nameXn = nameXn;
    }

    public String getNameTT() {
        return nameTT;
    }

    public void setNameTT(String nameTT) {
        this.nameTT = nameTT;
    }

    public String getNameType() {
        return NameType;
    }

    public void setNameType(String nameType) {
        NameType = nameType;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetermined() {
        return determined;
    }

    public void setDetermined(String determined) {
        this.determined = determined;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Long getIsEx() {
        return isEx;
    }

    public void setIsEx(Long isEx) {
        this.isEx = isEx;
    }

    public Long getLikStatus() {
        return likStatus;
    }

    public void setLikStatus(Long likStatus) {
        this.likStatus = likStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getTypediseaseId() {
        return typediseaseId;
    }

    public void setTypediseaseId(Long typeDiseaseId) {
        this.typediseaseId = typeDiseaseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StatusDiseaseDTO)) {
            return false;
        }

        return id != null && id.equals(((StatusDiseaseDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "StatusDiseaseDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", determined='" + getDetermined() + "'" +
            ", status=" + getStatus() +
            ", type=" + getType() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", isEx=" + getIsEx() +
            ", likStatus=" + getLikStatus() +
            ", description='" + getDescription() + "'" +
            ", typediseaseId=" + getTypediseaseId() +
            "}";
    }
}
