package mta.ato.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

/**
 * Criteria class for the {@link mta.ato.domain.TypeTest} entity. This class is used
 * in {@link mta.ato.web.rest.TypeTestResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /type-tests?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TypeTestCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private ZonedDateTimeFilter updateTime;

    private LongFilter typediseaseId;

    private LongFilter testdiseaseId;

    public TypeTestCriteria() {
    }

    public TypeTestCriteria(TypeTestCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.updateTime = other.updateTime == null ? null : other.updateTime.copy();
        this.typediseaseId = other.typediseaseId == null ? null : other.typediseaseId.copy();
        this.testdiseaseId = other.testdiseaseId == null ? null : other.testdiseaseId.copy();
    }

    @Override
    public TypeTestCriteria copy() {
        return new TypeTestCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public ZonedDateTimeFilter getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTimeFilter updateTime) {
        this.updateTime = updateTime;
    }

    public LongFilter getTypediseaseId() {
        return typediseaseId;
    }

    public void setTypediseaseId(LongFilter typediseaseId) {
        this.typediseaseId = typediseaseId;
    }

    public LongFilter getTestdiseaseId() {
        return testdiseaseId;
    }

    public void setTestdiseaseId(LongFilter testdiseaseId) {
        this.testdiseaseId = testdiseaseId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TypeTestCriteria that = (TypeTestCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(updateTime, that.updateTime) &&
            Objects.equals(typediseaseId, that.typediseaseId) &&
            Objects.equals(testdiseaseId, that.testdiseaseId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        updateTime,
        typediseaseId,
        testdiseaseId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TypeTestCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (updateTime != null ? "updateTime=" + updateTime + ", " : "") +
                (typediseaseId != null ? "typediseaseId=" + typediseaseId + ", " : "") +
                (testdiseaseId != null ? "testdiseaseId=" + testdiseaseId + ", " : "") +
            "}";
    }

}
