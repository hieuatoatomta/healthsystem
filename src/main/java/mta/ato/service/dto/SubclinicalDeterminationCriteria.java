package mta.ato.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

/**
 * Criteria class for the {@link mta.ato.domain.SubclinicalDetermination} entity. This class is used
 * in {@link mta.ato.web.rest.SubclinicalDeterminationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /subclinical-determinations?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SubclinicalDeterminationCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter amount;

    private LongFilter idXn;

    private LongFilter idCheck;

    private LongFilter idTT;

    private LongFilter type;

    private ZonedDateTimeFilter updateTime;

    private LongFilter statusdiseaseId;

    public SubclinicalDeterminationCriteria() {
    }

    public SubclinicalDeterminationCriteria(SubclinicalDeterminationCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.amount = other.amount == null ? null : other.amount.copy();
        this.idXn = other.idXn == null ? null : other.idXn.copy();
        this.idCheck = other.idCheck == null ? null : other.idCheck.copy();
        this.idTT = other.idTT == null ? null : other.idTT.copy();
        this.type = other.type == null ? null : other.type.copy();
        this.updateTime = other.updateTime == null ? null : other.updateTime.copy();
        this.statusdiseaseId = other.statusdiseaseId == null ? null : other.statusdiseaseId.copy();
    }

    @Override
    public SubclinicalDeterminationCriteria copy() {
        return new SubclinicalDeterminationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getAmount() {
        return amount;
    }

    public void setAmount(LongFilter amount) {
        this.amount = amount;
    }

    public LongFilter getIdXn() {
        return idXn;
    }

    public void setIdXn(LongFilter idXn) {
        this.idXn = idXn;
    }

    public LongFilter getIdCheck() {
        return idCheck;
    }

    public void setIdCheck(LongFilter idCheck) {
        this.idCheck = idCheck;
    }

    public LongFilter getIdTT() {
        return idTT;
    }

    public void setIdTT(LongFilter idTT) {
        this.idTT = idTT;
    }

    public LongFilter getType() {
        return type;
    }

    public void setType(LongFilter type) {
        this.type = type;
    }

    public ZonedDateTimeFilter getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTimeFilter updateTime) {
        this.updateTime = updateTime;
    }

    public LongFilter getStatusdiseaseId() {
        return statusdiseaseId;
    }

    public void setStatusdiseaseId(LongFilter statusdiseaseId) {
        this.statusdiseaseId = statusdiseaseId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SubclinicalDeterminationCriteria that = (SubclinicalDeterminationCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(amount, that.amount) &&
            Objects.equals(idXn, that.idXn) &&
            Objects.equals(idCheck, that.idCheck) &&
            Objects.equals(idTT, that.idTT) &&
            Objects.equals(type, that.type) &&
            Objects.equals(updateTime, that.updateTime) &&
            Objects.equals(statusdiseaseId, that.statusdiseaseId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        amount,
        idXn,
        idCheck,
        idTT,
        type,
        updateTime,
        statusdiseaseId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SubclinicalDeterminationCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (amount != null ? "amount=" + amount + ", " : "") +
                (idXn != null ? "idXn=" + idXn + ", " : "") +
                (idCheck != null ? "idCheck=" + idCheck + ", " : "") +
                (idTT != null ? "idTT=" + idTT + ", " : "") +
                (type != null ? "type=" + type + ", " : "") +
                (updateTime != null ? "updateTime=" + updateTime + ", " : "") +
                (statusdiseaseId != null ? "statusdiseaseId=" + statusdiseaseId + ", " : "") +
            "}";
    }

}
