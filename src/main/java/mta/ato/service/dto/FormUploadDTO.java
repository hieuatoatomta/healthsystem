package mta.ato.service.dto;

import mta.ato.utils.annotations.ValidFile;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

import static mta.ato.config.Constants.CONTENT_TYPE.*;
import static mta.ato.config.Constants.CONTENT_TYPE.PDF;

public class FormUploadDTO {
    private JSONObject model;
//    @ValidFile(filesAllow = {JPG, JPEG, PNG, PDF}, message = "{attachFile.onlySupportMicrosoftAndPdfFile}")
    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public JSONObject getModel() {
        return model;
    }

    public void setModel(JSONObject model) {
        this.model = model;
    }
}
