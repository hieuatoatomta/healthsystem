package mta.ato.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the {@link mta.ato.domain.RoleObject} entity.
 */
public class RoleObjectDTO implements Serializable {

    private Long id;

    private ZonedDateTime updateTime;


    private Long rolesId;

    private Long actionsId;

    private Long objectsId;

    private List<RoleObjectDTO> list;

    public List<RoleObjectDTO> getList() {
        return list;
    }

    public void setList(List<RoleObjectDTO> list) {
        this.list = list;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Long getRolesId() {
        return rolesId;
    }

    public void setRolesId(Long rolesId) {
        this.rolesId = rolesId;
    }

    public Long getActionsId() {
        return actionsId;
    }

    public void setActionsId(Long actionsId) {
        this.actionsId = actionsId;
    }

    public Long getObjectsId() {
        return objectsId;
    }

    public void setObjectsId(Long objectsId) {
        this.objectsId = objectsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RoleObjectDTO)) {
            return false;
        }

        return id != null && id.equals(((RoleObjectDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RoleObjectDTO{" +
            "id=" + getId() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", rolesId=" + getRolesId() +
            ", actionsId=" + getActionsId() +
            ", objectsId=" + getObjectsId() +
            "}";
    }
}
