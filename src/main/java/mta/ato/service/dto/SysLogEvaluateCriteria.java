package mta.ato.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

/**
 * Criteria class for the {@link mta.ato.domain.SysLogEvaluate} entity. This class is used
 * in {@link mta.ato.web.rest.SysLogEvaluateResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /sys-log-evaluates?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SysLogEvaluateCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter fullName;

    private StringFilter mail;

    private StringFilter phone;

    private StringFilter nameType;

    private StringFilter nameEvaluate;

    private StringFilter content;

    private ZonedDateTimeFilter updateTime;

    public SysLogEvaluateCriteria() {
    }

    public SysLogEvaluateCriteria(SysLogEvaluateCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.fullName = other.fullName == null ? null : other.fullName.copy();
        this.mail = other.mail == null ? null : other.mail.copy();
        this.phone = other.phone == null ? null : other.phone.copy();
        this.nameType = other.nameType == null ? null : other.nameType.copy();
        this.nameEvaluate = other.nameEvaluate == null ? null : other.nameEvaluate.copy();
        this.content = other.content == null ? null : other.content.copy();
        this.updateTime = other.updateTime == null ? null : other.updateTime.copy();
    }

    @Override
    public SysLogEvaluateCriteria copy() {
        return new SysLogEvaluateCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getFullName() {
        return fullName;
    }

    public void setFullName(StringFilter fullName) {
        this.fullName = fullName;
    }

    public StringFilter getMail() {
        return mail;
    }

    public void setMail(StringFilter mail) {
        this.mail = mail;
    }

    public StringFilter getPhone() {
        return phone;
    }

    public void setPhone(StringFilter phone) {
        this.phone = phone;
    }

    public StringFilter getNameType() {
        return nameType;
    }

    public void setNameType(StringFilter nameType) {
        this.nameType = nameType;
    }

    public StringFilter getNameEvaluate() {
        return nameEvaluate;
    }

    public void setNameEvaluate(StringFilter nameEvaluate) {
        this.nameEvaluate = nameEvaluate;
    }

    public StringFilter getContent() {
        return content;
    }

    public void setContent(StringFilter content) {
        this.content = content;
    }

    public ZonedDateTimeFilter getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTimeFilter updateTime) {
        this.updateTime = updateTime;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SysLogEvaluateCriteria that = (SysLogEvaluateCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(fullName, that.fullName) &&
            Objects.equals(mail, that.mail) &&
            Objects.equals(phone, that.phone) &&
            Objects.equals(nameType, that.nameType) &&
            Objects.equals(nameEvaluate, that.nameEvaluate) &&
            Objects.equals(content, that.content) &&
            Objects.equals(updateTime, that.updateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        fullName,
        mail,
        phone,
        nameType,
        nameEvaluate,
        content,
        updateTime
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SysLogEvaluateCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (fullName != null ? "fullName=" + fullName + ", " : "") +
                (mail != null ? "mail=" + mail + ", " : "") +
                (phone != null ? "phone=" + phone + ", " : "") +
                (nameType != null ? "nameType=" + nameType + ", " : "") +
                (nameEvaluate != null ? "nameEvaluate=" + nameEvaluate + ", " : "") +
                (content != null ? "content=" + content + ", " : "") +
                (updateTime != null ? "updateTime=" + updateTime + ", " : "") +
            "}";
    }

}
