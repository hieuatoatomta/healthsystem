package mta.ato.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

/**
 * Criteria class for the {@link mta.ato.domain.Frequency} entity. This class is used
 * in {@link mta.ato.web.rest.FrequencyResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /frequencies?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class FrequencyCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private LongFilter status;

    private LongFilter type;

    private ZonedDateTimeFilter updateTime;

    private LongFilter symptomId;

    public FrequencyCriteria() {
    }

    public FrequencyCriteria(FrequencyCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.type = other.type == null ? null : other.type.copy();
        this.updateTime = other.updateTime == null ? null : other.updateTime.copy();
        this.symptomId = other.symptomId == null ? null : other.symptomId.copy();
    }

    @Override
    public FrequencyCriteria copy() {
        return new FrequencyCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getStatus() {
        return status;
    }

    public void setStatus(LongFilter status) {
        this.status = status;
    }

    public LongFilter getType() {
        return type;
    }

    public void setType(LongFilter type) {
        this.type = type;
    }

    public ZonedDateTimeFilter getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTimeFilter updateTime) {
        this.updateTime = updateTime;
    }

    public LongFilter getSymptomId() {
        return symptomId;
    }

    public void setSymptomId(LongFilter symptomId) {
        this.symptomId = symptomId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FrequencyCriteria that = (FrequencyCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(status, that.status) &&
            Objects.equals(type, that.type) &&
            Objects.equals(updateTime, that.updateTime) &&
            Objects.equals(symptomId, that.symptomId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        status,
        type,
        updateTime,
        symptomId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FrequencyCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (type != null ? "type=" + type + ", " : "") +
                (updateTime != null ? "updateTime=" + updateTime + ", " : "") +
                (symptomId != null ? "symptomId=" + symptomId + ", " : "") +
            "}";
    }

}
