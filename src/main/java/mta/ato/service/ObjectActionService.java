package mta.ato.service;

import mta.ato.domain.ObjectAction;
import mta.ato.repository.ObjectActionRepository;
import mta.ato.service.dto.ObjectActionDTO;
import mta.ato.service.mapper.ObjectActionMapper;
import mta.ato.utils.DateUtil;
import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Optional;
import java.util.stream.IntStream;

/**
 * Service Implementation for managing {@link ObjectAction}.
 */
@Service
@Transactional
public class ObjectActionService {

    private final Logger log = LoggerFactory.getLogger(ObjectActionService.class);

    private final ObjectActionRepository objectActionRepository;

    private final ObjectActionMapper objectActionMapper;

    public ObjectActionService(ObjectActionRepository objectActionRepository, ObjectActionMapper objectActionMapper) {
        this.objectActionRepository = objectActionRepository;
        this.objectActionMapper = objectActionMapper;
    }

    /**
     * Save a objectAction.
     *
     * @param objectActionDTO the entity to save.
     * @return the persisted entity.
     */
    public ObjectActionDTO save(ObjectActionDTO objectActionDTO) {
        log.debug("Request to save ObjectAction : {}", objectActionDTO);
        ObjectAction objectAction = objectActionMapper.toEntity(objectActionDTO);
        objectAction = objectActionRepository.save(objectAction);
        return objectActionMapper.toDto(objectAction);
    }

    /**
     * Get all the objectActions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ObjectActionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ObjectActions");
        return objectActionRepository.findAll(pageable)
            .map(objectActionMapper::toDto);
    }


    /**
     * Get one objectAction by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ObjectActionDTO> findOne(Long id) {
        log.debug("Request to get ObjectAction : {}", id);
        return objectActionRepository.findById(id)
            .map(objectActionMapper::toDto);
    }

    /**
     * Delete the objectAction by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ObjectAction : {}", id);
        objectActionRepository.deleteById(id);
    }

    public Long deleteByCode(ObjectActionDTO obj) {
        try {
            IntStream.range(0, obj.getListUncheck().size()).forEach(i -> {
                ObjectActionDTO objectActionDTO = new ObjectActionDTO();
                objectActionDTO.setObjectsId(obj.getObjectsId());
                objectActionDTO.setActionsId(obj.getListUncheck().get(i));
                objectActionRepository.deleteByObjectsAndAction(objectActionDTO.getActionsId(), objectActionDTO.getObjectsId());
            });
//            obj.setListUncheckToString(obj.getListUncheck().toString().substring(1, obj.getListUncheck().toString().length()-1));
//            objectActionDAO.deleleRow(obj);
            return obj.getObjectsId();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ServiceException("Thêm mới quyền không thành công");
        }
    }

    public Long insert(ObjectActionDTO obj) {
        try {
            for (int i = 0; i < obj.getListAdd().size(); i++){
                ObjectActionDTO objectActionDTO = new ObjectActionDTO();
                objectActionDTO.setObjectsId(obj.getObjectsId());
                objectActionDTO.setActionsId(obj.getListAdd().get(i));
                objectActionDTO.setUpdateTime(DateUtil.getDateC());
                ObjectAction objectAction = objectActionMapper.toEntity(objectActionDTO);
                objectActionRepository.save(objectAction);
            }
            return obj.getObjectsId();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ServiceException("Thêm mới quyền không thành công");
        }
    }
}
