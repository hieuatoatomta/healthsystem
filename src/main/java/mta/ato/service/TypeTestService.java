package mta.ato.service;

import mta.ato.domain.ObjectAction;
import mta.ato.domain.TypeTest;
import mta.ato.repository.TypeTestRepository;
import mta.ato.service.dto.ObjectActionDTO;
import mta.ato.service.dto.TypeTestDTO;
import mta.ato.service.mapper.TypeTestMapper;
import mta.ato.utils.DateUtil;
import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.stream.IntStream;

/**
 * Service Implementation for managing {@link TypeTest}.
 */
@Service
@Transactional
public class TypeTestService {

    private final Logger log = LoggerFactory.getLogger(TypeTestService.class);

    private final TypeTestRepository typeTestRepository;

    private final TypeTestMapper typeTestMapper;

    public TypeTestService(TypeTestRepository typeTestRepository, TypeTestMapper typeTestMapper) {
        this.typeTestRepository = typeTestRepository;
        this.typeTestMapper = typeTestMapper;
    }

    /**
     * Save a typeTest.
     *
     * @param typeTestDTO the entity to save.
     * @return the persisted entity.
     */
    public TypeTestDTO save(TypeTestDTO typeTestDTO) {
        log.debug("Request to save TypeTest : {}", typeTestDTO);
        TypeTest typeTest = typeTestMapper.toEntity(typeTestDTO);
        typeTest = typeTestRepository.save(typeTest);
        return typeTestMapper.toDto(typeTest);
    }

    /**
     * Get all the typeTests.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TypeTestDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TypeTests");
        return typeTestRepository.findAll(pageable)
            .map(typeTestMapper::toDto);
    }


    /**
     * Get one typeTest by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TypeTestDTO> findOne(Long id) {
        log.debug("Request to get TypeTest : {}", id);
        return typeTestRepository.findById(id)
            .map(typeTestMapper::toDto);
    }

    /**
     * Delete the typeTest by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TypeTest : {}", id);
        typeTestRepository.deleteById(id);
    }

    public Long deleteByCode(TypeTestDTO obj) {
        try {
            IntStream.range(0, obj.getListUncheck().size()).forEach(i -> {
                TypeTestDTO objectActionDTO = new TypeTestDTO();
                objectActionDTO.setTypediseaseId(obj.getTypediseaseId());
                objectActionDTO.setTestdiseaseId(obj.getListUncheck().get(i));
                typeTestRepository.deleteByObjectsAndAction(objectActionDTO.getTestdiseaseId(), objectActionDTO.getTypediseaseId());
            });
//            obj.setListUncheckToString(obj.getListUncheck().toString().substring(1, obj.getListUncheck().toString().length()-1));
//            objectActionDAO.deleleRow(obj);
            return obj.getTypediseaseId();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ServiceException("Thêm mới quyền không thành công");
        }
    }

    public Long insert(TypeTestDTO obj) {
        try {
            for (int i = 0; i < obj.getListAdd().size(); i++){
                TypeTestDTO objectActionDTO = new TypeTestDTO();
                objectActionDTO.setTypediseaseId(obj.getTypediseaseId());
                objectActionDTO.setTestdiseaseId(obj.getListAdd().get(i));
                objectActionDTO.setUpdateTime(DateUtil.getDateC());
                TypeTest typeTest = typeTestMapper.toEntity(objectActionDTO);
                typeTestRepository.save(typeTest);
            }
            return obj.getTypediseaseId();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ServiceException("Thêm mới bệnh không thành công");
        }
    }
}
