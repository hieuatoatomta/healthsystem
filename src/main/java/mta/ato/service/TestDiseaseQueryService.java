package mta.ato.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mta.ato.domain.TestDisease;
import mta.ato.domain.*; // for static metamodels
import mta.ato.repository.TestDiseaseRepository;
import mta.ato.service.dto.TestDiseaseCriteria;
import mta.ato.service.dto.TestDiseaseDTO;
import mta.ato.service.mapper.TestDiseaseMapper;

/**
 * Service for executing complex queries for {@link TestDisease} entities in the database.
 * The main input is a {@link TestDiseaseCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TestDiseaseDTO} or a {@link Page} of {@link TestDiseaseDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TestDiseaseQueryService extends QueryService<TestDisease> {

    private final Logger log = LoggerFactory.getLogger(TestDiseaseQueryService.class);

    private final TestDiseaseRepository testDiseaseRepository;

    private final TestDiseaseMapper testDiseaseMapper;

    public TestDiseaseQueryService(TestDiseaseRepository testDiseaseRepository, TestDiseaseMapper testDiseaseMapper) {
        this.testDiseaseRepository = testDiseaseRepository;
        this.testDiseaseMapper = testDiseaseMapper;
    }

    /**
     * Return a {@link List} of {@link TestDiseaseDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TestDiseaseDTO> findByCriteria(TestDiseaseCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TestDisease> specification = createSpecification(criteria);
        return testDiseaseMapper.toDto(testDiseaseRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TestDiseaseDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TestDisease> findByCriteria(TestDiseaseCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TestDisease> specification = createSpecification(criteria);
        return testDiseaseRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TestDiseaseCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TestDisease> specification = createSpecification(criteria);
        return testDiseaseRepository.count(specification);
    }

    /**
     * Function to convert {@link TestDiseaseCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TestDisease> createSpecification(TestDiseaseCriteria criteria) {
        Specification<TestDisease> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TestDisease_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), TestDisease_.code));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), TestDisease_.name));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatus(), TestDisease_.status));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), TestDisease_.updateTime));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), TestDisease_.description));
            }
            if (criteria.getDsTypeTestId() != null) {
                specification = specification.and(buildSpecification(criteria.getDsTypeTestId(),
                    root -> root.join(TestDisease_.dsTypeTests, JoinType.LEFT).get(TypeTest_.id)));
            }
            if (criteria.getDsSymptomId() != null) {
                specification = specification.and(buildSpecification(criteria.getDsSymptomId(),
                    root -> root.join(TestDisease_.dsSymptoms, JoinType.LEFT).get(Symptom_.id)));
            }
        }
        return specification;
    }
}
