package mta.ato.service;

import io.jsonwebtoken.lang.Assert;
import mta.ato.config.Constants;
import mta.ato.config.CustomUserDetails;
import mta.ato.domain.Users;
import mta.ato.service.dto.ChangePassDTO;
import mta.ato.service.dto.LoginDTO;
import mta.ato.service.dto.UsersDTO;
import mta.ato.service.mapper.UsersMapper;
import mta.ato.utils.DateUtil;
import mta.ato.utils.Translator;
import mta.ato.utils.Utils;
import mta.ato.utils.ValidateUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@Transactional
public class UserJWTService {

    private static final Logger logger = LoggerFactory.getLogger(UserJWTService.class);

    @Autowired
    mta.ato.repository.UsersRepository usersRepository;

    @Autowired
    AuthenticationProvider authenticationProvider;

    @Autowired
    CaptchaService captchaService;

    private final UsersMapper usersMapper;

    public UserJWTService(UsersMapper usersMapper) {
        this.usersMapper = usersMapper;
    }

    public LoginDTO login(UsersDTO obj, boolean checkCaptcha) {
        //checkLogin(obj);
//        if (checkCaptcha) {
//            boolean captchaVerified = captchaService.verify(obj.getRecaptchaReactive());
//            if (!captchaVerified) {
//                logger.error(Constants.GOOGLE_RECAPTCHA_FALSE);
//                throw new ServiceException(Translator.toLocale(Constants.GOOGLE_RECAPTCHA_FALSE));
//            }
//        }
        Users rs = usersRepository.searchUsersId(obj.getName(), null, null);
        UsersDTO usersDTO = usersMapper.toDto(rs);

        if (usersDTO != null) {
            if (ValidateUtils.changePassword(usersDTO.getPass(), obj.getPass())) {

                Authentication authentication = authenticationProvider
                    .authenticate(new UsernamePasswordAuthenticationToken(obj, obj.getPass()));
                Assert.notNull(authentication, Constants.LOGIN_FALSE);

                LoginDTO login = new LoginDTO();
                CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
                String jwt = customUserDetails.getJwt();
                HttpHeaders httpHeaders = new HttpHeaders();
                httpHeaders.add(Constants.AUTHORIZATION, Constants.BEARER + jwt);
                login.setHttpHeaders(httpHeaders);
                login.setListObjects(customUserDetails.getList());
                login.setCustomUserDetails(customUserDetails.getUser());
                login.setPath(Translator.toLocale(Constants.URL_PART));
                return login;

            }
            logger.error(Constants.LOGIN_FALSE);
            throw new ServiceException(Translator.toLocale(Constants.LOGIN_FALSE));

        } else {
            logger.error(Constants.LOGIN_FALSE);
            throw new ServiceException(Translator.toLocale(Constants.LOGIN_FALSE));

        }

    }

    public String resetPassword(ChangePassDTO changePassDTO) {
        UsersDTO userDTO = null;
        if (StringUtils.isNotEmpty(changePassDTO.getKey())) {
            try {
                Users rs = usersRepository.searchUsersId(null, null, changePassDTO.getKey());
                userDTO = usersMapper.toDto(rs);
                Date date = Date.from(userDTO.getResetDate().toInstant());
                System.out.println(date);
                if (DateUtil.compareDate(DateUtil.addMinutesToDate(Date.from(userDTO.getResetDate().toInstant()), 15), new Date()) == 1) {
                    logger.error(Constants.TOKEN_ERROR);
                    throw new ServiceException(Translator.toLocale(Constants.TOKEN_ERROR));
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                throw new ServiceException(Translator.toLocale(Constants.TOKEN_ERROR));
            }
        }
        if (StringUtils.isNotEmpty(changePassDTO.getUserName())) {
            Users rs = usersRepository.searchUsersId(changePassDTO.getUserName(), null, null);
            userDTO = usersMapper.toDto(rs);
        }
        if (userDTO != null) {
            Utils.checkRestPass(changePassDTO);
            if (StringUtils.isNotEmpty(changePassDTO.getUserName())) {
                if (!ValidateUtils.changePassword(userDTO.getPass(), changePassDTO.getOldPass())) {
                    logger.error(Constants.LOGIN_PASS_CHANGE_FALSE);
                    throw new ServiceException(Translator.toLocale(Constants.LOGIN_PASS_CHANGE_FALSE));
                }
                if (ValidateUtils.checkResetPass(changePassDTO.getOldPass(), changePassDTO.getNewPass())) {
                    logger.error(Constants.LOGIN_PASS_OLD_SS_FALSE);
                    throw new ServiceException(Translator.toLocale(Constants.LOGIN_PASS_OLD_SS_FALSE));
                }
            }
            if (!ValidateUtils.checkResetPass(changePassDTO.getComPass(), changePassDTO.getNewPass())) {
                logger.error(Constants.LOGIN_PASS_NEW_SS_FALSE);
                throw new ServiceException(Translator.toLocale(Constants.LOGIN_PASS_NEW_SS_FALSE));
            }
            userDTO.setPass(new BCryptPasswordEncoder().encode(changePassDTO.getNewPass()));
            try {
                Users users = usersMapper.toEntity(userDTO);
                usersRepository.save(users);
                if (StringUtils.isNotEmpty(changePassDTO.getKey())) {
                    userDTO.setResetKey(null);
                    userDTO.setResetDate(null);
                    Users users1 = usersMapper.toEntity(userDTO);
                    usersRepository.save(users1);
                }
                logger.info(Constants.CHANGE_PASS_SUS);
                return Translator.toLocale(Constants.CHANGE_PASS_SUS);
            } catch (ServiceException e) {
                logger.error(Constants.CHANGE_PASS_FALSE, e);
                throw new ServiceException(Translator.toLocale(Constants.CHANGE_PASS_FALSE));
            }
        } else {
            logger.error(Constants.CHANGE_NAME_FALSE);
            throw new ServiceException(Translator.toLocale(Constants.CHANGE_NAME_FALSE));
        }
    }

    public UsersDTO forgotPassword(ChangePassDTO obj) {
        if (null != obj.getEmail()) {
            try {
                Users rs = usersRepository.searchUsersId(null, obj.getEmail(), null);
                UsersDTO userDTO = usersMapper.toDto(rs);
                if (userDTO != null) {
                    return userDTO;
                }
            } catch (Exception e) {
                logger.error(Translator.toLocale(Constants.LOGIN_EMAIL_FALSE), e);
                throw new ServiceException(Translator.toLocale(Constants.LOGIN_EMAIL_FALSE));
            }
        }
        logger.error(Constants.LOGIN_EMAIL_FALSE);
        throw new ServiceException(Translator.toLocale(Constants.LOGIN_EMAIL_FALSE));
    }

    public void updateKey(UsersDTO sysUserDTO) {
        sysUserDTO.setResetDate(DateUtil.getDateC());
        Users sysUser = usersMapper.toEntity(sysUserDTO);
        usersRepository.save(sysUser);
    }
}
