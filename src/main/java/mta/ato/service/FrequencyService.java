package mta.ato.service;

import mta.ato.domain.Frequency;
import mta.ato.repository.FrequencyRepository;
import mta.ato.service.dto.FrequencyDTO;
import mta.ato.service.mapper.FrequencyMapper;
import mta.ato.utils.DataUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Frequency}.
 */
@Service
@Transactional
public class FrequencyService {

    private final Logger log = LoggerFactory.getLogger(FrequencyService.class);

    private final FrequencyRepository frequencyRepository;

    private final FrequencyMapper frequencyMapper;

    public FrequencyService(FrequencyRepository frequencyRepository, FrequencyMapper frequencyMapper) {
        this.frequencyRepository = frequencyRepository;
        this.frequencyMapper = frequencyMapper;
    }

    /**
     * Save a frequency.
     *
     * @param frequencyDTO the entity to save.
     * @return the persisted entity.
     */
    public FrequencyDTO save(FrequencyDTO frequencyDTO) {
        log.debug("Request to save Frequency : {}", frequencyDTO);
        Frequency frequency = frequencyMapper.toEntity(frequencyDTO);
        frequency = frequencyRepository.save(frequency);
        return frequencyMapper.toDto(frequency);
    }

    /**
     * Get all the frequencies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<FrequencyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Frequencies");
        return frequencyRepository.findAll(pageable)
            .map(frequencyMapper::toDto);
    }


    /**
     * Get one frequency by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<FrequencyDTO> findOne(Long id) {
        log.debug("Request to get Frequency : {}", id);
        return frequencyRepository.findById(id)
            .map(frequencyMapper::toDto);
    }

    /**
     * Delete the frequency by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Frequency : {}", id);
        frequencyRepository.deleteById(id);
    }

    public List<FrequencyDTO> getMax(List<Long> lsId) {
        List<FrequencyDTO> lstObject = frequencyRepository.getMax(lsId)
            .stream().map(e -> {
                FrequencyDTO dto = new FrequencyDTO();
                dto.setMaxCd(DataUtil.safeToLong(e[0]));
                return dto;
            }).collect(Collectors.toList());
        return lstObject;
    }
}
