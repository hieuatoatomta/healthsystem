package mta.ato.service;

import mta.ato.domain.Symptom;
import mta.ato.domain.TestDisease;
import mta.ato.repository.SymptomRepository;
import mta.ato.service.dto.SymptomDTO;
import mta.ato.service.dto.TestDiseaseDTO;
import mta.ato.service.mapper.SymptomMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Symptom}.
 */
@Service
@Transactional
public class SymptomService {

    private final Logger log = LoggerFactory.getLogger(SymptomService.class);

    private final SymptomRepository symptomRepository;

    private final SymptomMapper symptomMapper;

    public SymptomService(SymptomRepository symptomRepository, SymptomMapper symptomMapper) {
        this.symptomRepository = symptomRepository;
        this.symptomMapper = symptomMapper;
    }

    /**
     * Save a symptom.
     *
     * @param symptomDTO the entity to save.
     * @return the persisted entity.
     */
    public SymptomDTO save(SymptomDTO symptomDTO) {
        log.debug("Request to save Symptom : {}", symptomDTO);
        Symptom symptom = symptomMapper.toEntity(symptomDTO);
        symptom = symptomRepository.save(symptom);
        return symptomMapper.toDto(symptom);
    }

    /**
     * Get all the symptoms.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SymptomDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Symptoms");
        return symptomRepository.findAll(pageable)
            .map(symptomMapper::toDto);
    }


    /**
     * Get one symptom by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SymptomDTO> findOne(Long id) {
        log.debug("Request to get Symptom : {}", id);
        return symptomRepository.findById(id)
            .map(symptomMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Optional<Symptom> findOne1(Long id) {
        log.debug("Request to get Symptom : {}", id);
        return symptomRepository.findById(id);
    }

    /**
     * Delete the symptom by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Symptom : {}", id);
        symptomRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public List<SymptomDTO> findByTypediseaseId(Long typediseaseId, Long testdiseaseId) {
        log.debug("Request to get StatusDisease : {}", typediseaseId);
        List<Symptom> statusDiseaseList = symptomRepository.findByDetermined(typediseaseId, testdiseaseId);
        List<SymptomDTO> statusDiseaseDTOList = new ArrayList<>();
        for (int i = 0; i < statusDiseaseList.size(); i++) {
            statusDiseaseDTOList.add(symptomMapper.toDto(statusDiseaseList.get(i)));
        }
        return statusDiseaseDTOList;
    }

    @Transactional(readOnly = true)
    public List<SymptomDTO> findByGroup(Long typediseaseId, List<Long> listIdXn) {
        log.debug("Request to get StatusDisease : {}", typediseaseId);
        List<Symptom> statusDiseaseList = symptomRepository.findByGroup(typediseaseId, listIdXn);
        List<SymptomDTO> statusDiseaseDTOList = new ArrayList<>();
        for (int i = 0; i < statusDiseaseList.size(); i++) {
            SymptomDTO symptomDTO = symptomMapper.toDto(statusDiseaseList.get(i));
            symptomDTO.setNameXn(statusDiseaseList.get(i).getTestdisease().getName());
            statusDiseaseDTOList.add(symptomDTO);
        }
        return statusDiseaseDTOList;
    }
}
