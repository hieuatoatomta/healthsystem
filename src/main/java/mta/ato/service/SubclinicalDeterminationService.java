package mta.ato.service;

import mta.ato.domain.SubclinicalDetermination;
import mta.ato.repository.SubclinicalDeterminationRepository;
import mta.ato.service.dto.DsRolesDTO;
import mta.ato.service.dto.SubclinicalDeterminationDTO;
import mta.ato.service.dto.UsersDTO;
import mta.ato.service.mapper.SubclinicalDeterminationMapper;
import mta.ato.utils.DataUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link SubclinicalDetermination}.
 */
@Service
@Transactional
public class SubclinicalDeterminationService {

    private final Logger log = LoggerFactory.getLogger(SubclinicalDeterminationService.class);

    private final SubclinicalDeterminationRepository subclinicalDeterminationRepository;

    private final SubclinicalDeterminationMapper subclinicalDeterminationMapper;

    public SubclinicalDeterminationService(SubclinicalDeterminationRepository subclinicalDeterminationRepository, SubclinicalDeterminationMapper subclinicalDeterminationMapper) {
        this.subclinicalDeterminationRepository = subclinicalDeterminationRepository;
        this.subclinicalDeterminationMapper = subclinicalDeterminationMapper;
    }

    /**
     * Save a subclinicalDetermination.
     *
     * @param subclinicalDeterminationDTO the entity to save.
     * @return the persisted entity.
     */
    public SubclinicalDeterminationDTO save(SubclinicalDeterminationDTO subclinicalDeterminationDTO) {
        log.debug("Request to save SubclinicalDetermination : {}", subclinicalDeterminationDTO);
        SubclinicalDetermination subclinicalDetermination = subclinicalDeterminationMapper.toEntity(subclinicalDeterminationDTO);
        subclinicalDetermination = subclinicalDeterminationRepository.save(subclinicalDetermination);
        return subclinicalDeterminationMapper.toDto(subclinicalDetermination);
    }

    /**
     * Get all the subclinicalDeterminations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SubclinicalDeterminationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SubclinicalDeterminations");
        return subclinicalDeterminationRepository.findAll(pageable)
            .map(subclinicalDeterminationMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<SubclinicalDeterminationDTO> getRoles(Long id) {
        List<SubclinicalDeterminationDTO> lstObject = subclinicalDeterminationRepository.getRoleAction( id )
            .stream().map( e -> {
                SubclinicalDeterminationDTO dto = new SubclinicalDeterminationDTO();
                dto.setAmount( DataUtil.safeToLong( e[0] ) );
                dto.setStatusdiseaseId( DataUtil.safeToLong( e[1] ) );
                dto.setType( DataUtil.safeToLong( e[2] ) );
                dto.setNameXn( DataUtil.safeToString( e[3] ) );
                dto.setNameTT( DataUtil.safeToString( e[4] ) );
                return dto;
            } ).collect( Collectors.toList() );
        return lstObject;
    }

    @Transactional(readOnly = true)
    public List<SubclinicalDeterminationDTO> findAllByAmountAndIdXn(Long typediseaseId, Long id, List<Long> idXn) {
        log.debug("Request to get SubclinicalDetermination : {}", id);
        List<SubclinicalDeterminationDTO> lstObject = subclinicalDeterminationRepository.findAllByAmountInIdXn(typediseaseId, id, idXn)
            .stream().map( e -> {
                if(null == e){
                    SubclinicalDeterminationDTO dto = new SubclinicalDeterminationDTO();
                    dto.setTotalMax(0L);
                    return dto;
                } else {
                    SubclinicalDeterminationDTO dto = new SubclinicalDeterminationDTO();
                    dto.setTotalMax( DataUtil.safeToLong( e[0] ) );
                    return dto;
                }
            } ).collect( Collectors.toList() );
        return lstObject;
    }


    /**
     * Get one subclinicalDetermination by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SubclinicalDeterminationDTO> findOne(Long id) {
        log.debug("Request to get SubclinicalDetermination : {}", id);
        return subclinicalDeterminationRepository.findById(id)
            .map(subclinicalDeterminationMapper::toDto);
    }

    @Transactional(readOnly = true)
    public boolean findByType(Long id) {
        log.debug("Request to get SubclinicalDetermination : {}", id);
        if (subclinicalDeterminationRepository.findAllByType(id).size() > 0){
            return true;
        } return false;
    }

    /**
     * Delete the subclinicalDetermination by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete SubclinicalDetermination : {}", id);
        subclinicalDeterminationRepository.deleteById(id);
    }

    public void deleteByType(Long id) {
        log.debug("Request to delete SubclinicalDetermination : {}", id);
        subclinicalDeterminationRepository.deleteByType(id);
    }
}
