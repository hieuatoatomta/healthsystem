package mta.ato.service;

import mta.ato.domain.TestDisease;
import mta.ato.domain.TypeDisease;
import mta.ato.domain.TypeTest;
import mta.ato.repository.TestDiseaseRepository;
import mta.ato.service.dto.TestDiseaseDTO;
import mta.ato.service.dto.TypeTestDTO;
import mta.ato.service.mapper.TestDiseaseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TestDisease}.
 */
@Service
@Transactional
public class TestDiseaseService {

    private final Logger log = LoggerFactory.getLogger(TestDiseaseService.class);

    private final TestDiseaseRepository testDiseaseRepository;

    private final TestDiseaseMapper testDiseaseMapper;

    public TestDiseaseService(TestDiseaseRepository testDiseaseRepository, TestDiseaseMapper testDiseaseMapper) {
        this.testDiseaseRepository = testDiseaseRepository;
        this.testDiseaseMapper = testDiseaseMapper;
    }

    /**
     * Save a testDisease.
     *
     * @param testDiseaseDTO the entity to save.
     * @return the persisted entity.
     */
    public TestDiseaseDTO save(TestDiseaseDTO testDiseaseDTO) {
        log.debug("Request to save TestDisease : {}", testDiseaseDTO);
        TestDisease testDisease = testDiseaseMapper.toEntity(testDiseaseDTO);
        testDisease = testDiseaseRepository.save(testDisease);
        return testDiseaseMapper.toDto(testDisease);
    }

    /**
     * Get all the testDiseases.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TestDiseaseDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TestDiseases");
        return testDiseaseRepository.findAll(pageable)
            .map(testDiseaseMapper::toDto);
    }


    /**
     * Get one testDisease by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TestDiseaseDTO> findOne(Long id) {
        log.debug("Request to get TestDisease : {}", id);
        return testDiseaseRepository.findById(id)
            .map(testDiseaseMapper::toDto);
    }

    /**
     * Delete the testDisease by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TestDisease : {}", id);
        testDiseaseRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public TestDisease findByCode(String code) {
        log.debug("Request to get all Roles");
        return testDiseaseRepository.findByCode(code);
    }

    @Transactional(readOnly = true)
    public List<TestDiseaseDTO> findByTypediseaseId(Long typediseaseId) {
        log.debug("Request to get StatusDisease : {}", typediseaseId);
        List<TestDisease> statusDiseaseList = testDiseaseRepository.findByDetermined(typediseaseId);
        List<TestDiseaseDTO> statusDiseaseDTOList = new ArrayList<>();
        for (int i = 0; i < statusDiseaseList.size(); i++) {
            statusDiseaseDTOList.add(testDiseaseMapper.toDto(statusDiseaseList.get(i)));
        }
        return statusDiseaseDTOList;
    }

}
