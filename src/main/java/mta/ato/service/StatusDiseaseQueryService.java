package mta.ato.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mta.ato.domain.StatusDisease;
import mta.ato.domain.*; // for static metamodels
import mta.ato.repository.StatusDiseaseRepository;
import mta.ato.service.dto.StatusDiseaseCriteria;
import mta.ato.service.dto.StatusDiseaseDTO;
import mta.ato.service.mapper.StatusDiseaseMapper;

/**
 * Service for executing complex queries for {@link StatusDisease} entities in the database.
 * The main input is a {@link StatusDiseaseCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link StatusDiseaseDTO} or a {@link Page} of {@link StatusDiseaseDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class StatusDiseaseQueryService extends QueryService<StatusDisease> {

    private final Logger log = LoggerFactory.getLogger(StatusDiseaseQueryService.class);

    private final StatusDiseaseRepository statusDiseaseRepository;

    private final StatusDiseaseMapper statusDiseaseMapper;

    public StatusDiseaseQueryService(StatusDiseaseRepository statusDiseaseRepository, StatusDiseaseMapper statusDiseaseMapper) {
        this.statusDiseaseRepository = statusDiseaseRepository;
        this.statusDiseaseMapper = statusDiseaseMapper;
    }

    /**
     * Return a {@link List} of {@link StatusDiseaseDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<StatusDiseaseDTO> findByCriteria(StatusDiseaseCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<StatusDisease> specification = createSpecification(criteria);
        return statusDiseaseMapper.toDto(statusDiseaseRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link StatusDiseaseDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<StatusDisease> findByCriteria(StatusDiseaseCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<StatusDisease> specification = createSpecification(criteria);
        return statusDiseaseRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(StatusDiseaseCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<StatusDisease> specification = createSpecification(criteria);
        return statusDiseaseRepository.count(specification);
    }

    /**
     * Function to convert {@link StatusDiseaseCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<StatusDisease> createSpecification(StatusDiseaseCriteria criteria) {
        Specification<StatusDisease> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), StatusDisease_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), StatusDisease_.name));
            }
            if (criteria.getDetermined() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDetermined(), StatusDisease_.determined));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatus(), StatusDisease_.status));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getType(), StatusDisease_.type));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), StatusDisease_.updateTime));
            }
            if (criteria.getIsEx() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIsEx(), StatusDisease_.isEx));
            }
            if (criteria.getLikStatus() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLikStatus(), StatusDisease_.likStatus));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), StatusDisease_.description));
            }
            if (criteria.getDsSubclinicalDeterminationId() != null) {
                specification = specification.and(buildSpecification(criteria.getDsSubclinicalDeterminationId(),
                    root -> root.join(StatusDisease_.dsSubclinicalDeterminations, JoinType.LEFT).get(SubclinicalDetermination_.id)));
            }
            if (criteria.getTypediseaseId() != null) {
                specification = specification.and(buildSpecification(criteria.getTypediseaseId(),
                    root -> root.join(StatusDisease_.typedisease, JoinType.LEFT).get(TypeDisease_.id)));
            }
        }
        return specification;
    }
}
